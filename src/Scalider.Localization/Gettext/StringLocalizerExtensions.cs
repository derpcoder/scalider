﻿#region # using statements #

using System;
using JetBrains.Annotations;
using Microsoft.Extensions.Localization;

#endregion

namespace Scalider.Localization.Gettext
{

    /// <summary>
    /// Extension methods for the <see cref="GettextStringLocalizer"/>.
    /// </summary>
    public static class StringLocalizerExtensions
    {

        /// <summary>
        /// Returns the plural form for <paramref name="n"/> of the translation
        /// of <paramref name="text" />.
        /// 
        /// Similar to <c>ngettext</c> function.
        /// </summary>
        /// <param name="localizer"></param>
        /// <param name="text">Singular form of message to translate.</param>
        /// <param name="pluralText">Plural form of message to translate.</param>
        /// <param name="n">Value that determines the plural form.</param>
        /// <returns>
        /// Translated text.
        /// </returns>
        public static string GetPluralString(this IStringLocalizer localizer,
            [NotNull] string text, [NotNull] string pluralText, long n)
        {
            var gettext = localizer as GettextStringLocalizer;
            if (gettext == null)
            {
                throw new InvalidOperationException(
                    $"{nameof(GetPluralString)} is supported only for " +
                    $"{nameof(GettextStringLocalizer)}");
            }

            return gettext.GetPluralString(text, pluralText, n);
        }

        /// <summary>
        /// Returns the plural form for <paramref name="n"/> of the translation
        /// of <paramref name="text" />and formatted with the supplied
        /// arguments.
        /// 
        /// Similar to <c>ngettext</c> function.
        /// </summary>
        /// <param name="localizer"></param>
        /// <param name="text">Singular form of message to translate.</param>
        /// <param name="pluralText">Plural form of message to translate.</param>
        /// <param name="n">Value that determines the plural form.</param>
        /// <param name="args">The values to format the string with.</param>
        /// <returns>
        /// Translated text.
        /// </returns>
        public static string GetPluralString(this IStringLocalizer localizer,
            [NotNull] string text, [NotNull] string pluralText, long n,
            params object[] args)
        {
            var gettext = localizer as GettextStringLocalizer;
            if (gettext == null)
            {
                throw new InvalidOperationException(
                    $"{nameof(GetPluralString)} is supported only for " +
                    $"{nameof(GettextStringLocalizer)}");
            }

            return gettext.GetPluralString(text, pluralText, n, args);
        }

        /// <summary>
        /// Returns <paramref name="text" /> translated into the selected
        /// language using given <paramref name="context" />.
        /// 
        /// Similar to <c>pgettext</c> function.
        /// </summary>
        /// <param name="localizer"></param>
        /// <param name="context">Context.</param>
        /// <param name="text">Text to translate.</param>
        /// <returns>
        /// Translated text.
        /// </returns>
        public static string GetParticularString(this IStringLocalizer localizer,
            [NotNull] string context, [NotNull] string text)
        {
            var gettext = localizer as GettextStringLocalizer;
            if (gettext == null)
            {
                throw new InvalidOperationException(
                    $"{nameof(GetParticularString)} is supported only for " +
                    $"{nameof(GettextStringLocalizer)}");
            }

            return gettext.GetParticularString(context, text);
        }

        /// <summary>
        /// Returns <paramref name="text" /> translated into the selected
        /// language using given <paramref name="context" /> and formatted with
        /// the supplied arguments.
        /// 
        /// Similar to <c>pgettext</c> function.
        /// </summary>
        /// <param name="localizer"></param>
        /// <param name="context">Context.</param>
        /// <param name="text">Text to translate.</param>
        /// <param name="args">The values to format the string with.</param>
        /// <returns>
        /// Translated text.
        /// </returns>
        public static string GetParticularString(this IStringLocalizer localizer,
            [NotNull] string context, [NotNull] string text, params object[] args)
        {
            var gettext = localizer as GettextStringLocalizer;
            if (gettext == null)
            {
                throw new InvalidOperationException(
                    $"{nameof(GetParticularString)} is supported only for " +
                    $"{nameof(GettextStringLocalizer)}");
            }

            return gettext.GetParticularString(context, text, args);
        }

        /// <summary>
        /// Returns the plural form for <paramref name="n" /> of the
        /// translation of <paramref name="text" /> using given
        /// <paramref name="context" />.
        /// 
        /// Similar to <c>npgettext</c> function.
        /// </summary>
        /// <param name="localizer"></param>
        /// <param name="context">Context.</param>
        /// <param name="text">Singular form of message to translate.</param>
        /// <param name="pluralText">Plural form of message to translate.</param>
        /// <param name="n">Value that determines the plural form.</param>
        /// <returns>
        /// Translated text.
        /// </returns>
        public static string GetParticularPluralString(
            this IStringLocalizer localizer, [NotNull] string context,
            [NotNull] string text, [NotNull] string pluralText, long n)
        {
            var gettext = localizer as GettextStringLocalizer;
            if (gettext == null)
            {
                throw new InvalidOperationException(
                    $"{nameof(GetParticularPluralString)} is supported only for " +
                    $"{nameof(GettextStringLocalizer)}");
            }

            return gettext.GetParticularPluralString(context, text, pluralText, n);
        }

        /// <summary>
        /// Returns the plural form for <paramref name="n" /> of the
        /// translation of <paramref name="text" /> using given
        /// <paramref name="context" /> and formatted with the supplied
        /// arguments.
        /// 
        /// Similar to <c>npgettext</c> function.
        /// </summary>
        /// <param name="localizer"></param>
        /// <param name="context">Context.</param>
        /// <param name="text">Singular form of message to translate.</param>
        /// <param name="pluralText">Plural form of message to translate.</param>
        /// <param name="n">Value that determines the plural form.</param>
        /// <param name="args">The values to format the string with.</param>
        /// <returns>
        /// Translated text.
        /// </returns>
        public static string GetParticularPluralString(
            this IStringLocalizer localizer, [NotNull] string context,
            [NotNull] string text, [NotNull] string pluralText, long n,
            params object[] args)
        {
            var gettext = localizer as GettextStringLocalizer;
            if (gettext == null)
            {
                throw new InvalidOperationException(
                    $"{nameof(GetParticularPluralString)} is supported only for " +
                    $"{nameof(GettextStringLocalizer)}");
            }

            return gettext.GetParticularPluralString(context, text, pluralText, n,
                args);
        }

    }
}