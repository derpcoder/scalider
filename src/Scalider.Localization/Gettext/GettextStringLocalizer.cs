﻿#region # using statements #

using System.Collections.Generic;
using System.Globalization;
using JetBrains.Annotations;
using Microsoft.Extensions.Localization;

#endregion

namespace Scalider.Localization.Gettext
{

    /// <summary>
    /// An <see cref="IStringLocalizer"/> that uses the
    /// <see cref="IGettextProcessor"/> to provide localized strings.
    /// </summary>
    public class GettextStringLocalizer : IStringLocalizer
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="GettextStringLocalizer"/> class.
        /// </summary>
        /// <param name="processor"></param>
        public GettextStringLocalizer([NotNull] IGettextProcessor processor)
        {
            Check.NotNull(processor, nameof(processor));
            Processor = processor;
        }

        #region # Properties #

        #region == Protected ==

        /// <summary>
        /// Gets the <see cref="IGettextProcessor"/> to use.
        /// </summary>
        protected IGettextProcessor Processor { get; }

        /// <summary>
        /// Gets the <see cref="CultureInfo"/> for this
        /// <see cref="IStringLocalizer"/>.
        /// </summary>
        protected virtual CultureInfo Culture => CultureInfo.CurrentUICulture;

        #endregion

        #endregion

        #region # Methods #

        #region == Public ==

        /// <summary>
        /// Returns <paramref name="text" /> translated into the selected
        /// language.
        /// 
        /// Similar to <c>gettext</c> function.
        /// </summary>
        /// <param name="text">Text to translate.</param>
        /// <returns>
        /// Translated text.
        /// </returns>
        public string GetString([NotNull] string text)
        {
            Check.NotNull(text, nameof(text));
            return Processor.GetString(Culture, text) ?? text;
        }

        /// <summary>
        /// Returns <paramref name="text" /> translated into the selected
        /// language and formatted with the supplied arguments.
        /// 
        /// Similar to <c>gettext</c> function.
        /// </summary>
        /// <param name="text">Text to translate.</param>
        /// <param name="args">The values to format the string with.</param>
        /// <returns>
        /// Translated text.
        /// </returns>
        public string GetString([NotNull] string text, params object[] args)
        {
            Check.NotNull(text, nameof(text));
            return string.Format(GetString(text), args);
        }

        /// <summary>
        /// Returns the plural form for <paramref name="n"/> of the translation
        /// of <paramref name="text" />.
        /// 
        /// Similar to <c>ngettext</c> function.
        /// </summary>
        /// <param name="text">Singular form of message to translate.</param>
        /// <param name="pluralText">Plural form of message to translate.</param>
        /// <param name="n">Value that determines the plural form.</param>
        /// <returns>
        /// Translated text.
        /// </returns>
        public string GetPluralString([NotNull] string text,
            [NotNull] string pluralText, long n)
        {
            Check.NotNull(text, nameof(text));
            Check.NotNull(pluralText, nameof(pluralText));

            return Processor.GetPluralString(Culture, text, pluralText, n);
        }

        /// <summary>
        /// Returns the plural form for <paramref name="n"/> of the translation
        /// of <paramref name="text" /> and formatted with the supplied
        /// arguments.
        /// 
        /// Similar to <c>ngettext</c> function.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="pluralText"></param>
        /// <param name="n"></param>
        /// <param name="args">The values to format the string with.</param>
        /// <returns></returns>
        public string GetPluralString([NotNull] string text,
            [NotNull] string pluralText, long n, params object[] args)
        {
            Check.NotNull(text, nameof(text));
            Check.NotNull(pluralText, nameof(pluralText));

            return string.Format(GetPluralString(text, pluralText, n), args);
        }

        /// <summary>
        /// Returns <paramref name="text" /> translated into the selected
        /// language using given <paramref name="context" />.
        /// 
        /// Similar to <c>pgettext</c> function.
        /// </summary>
        /// <param name="context">Context.</param>
        /// <param name="text">Text to translate.</param>
        /// <returns>
        /// Translated text.
        /// </returns>
        public string GetParticularString([NotNull] string context,
            [NotNull] string text)
        {
            Check.NotNull(context, nameof(context));
            Check.NotNull(text, nameof(text));

            return Processor.GetParticularString(Culture, context, text);
        }

        /// <summary>
        /// Returns <paramref name="text" /> translated into the selected
        /// language using given <paramref name="context" /> and formatted with
        /// the supplied arguments.
        /// 
        /// Similar to <c>pgettext</c> function.
        /// </summary>
        /// <param name="context">Context.</param>
        /// <param name="text">Text to translate.</param>
        /// <param name="args">The values to format the string with.</param>
        /// <returns>
        /// Translated text.
        /// </returns>
        public string GetParticularString([NotNull] string context,
            [NotNull] string text, params object[] args)
        {
            Check.NotNull(context, nameof(context));
            Check.NotNull(text, nameof(text));

            return string.Format(GetParticularString(context, text), args);
        }

        /// <summary>
        /// Returns the plural form for <paramref name="n" /> of the
        /// translation of <paramref name="text" /> using given
        /// <paramref name="context" />.
        /// 
        /// Similar to <c>npgettext</c> function.
        /// </summary>
        /// <param name="context">Context.</param>
        /// <param name="text">Singular form of message to translate.</param>
        /// <param name="pluralText">Plural form of message to translate.</param>
        /// <param name="n">Value that determines the plural form.</param>
        /// <returns>
        /// Translated text.
        /// </returns>
        public string GetParticularPluralString([NotNull] string context,
            [NotNull] string text, [NotNull] string pluralText, long n)
        {
            Check.NotNull(context, nameof(context));
            Check.NotNull(text, nameof(text));
            Check.NotNull(pluralText, nameof(pluralText));

            return Processor.GetParticularPluralString(Culture, context, text,
                pluralText, n);
        }

        /// <summary>
        /// Returns the plural form for <paramref name="n" /> of the
        /// translation of <paramref name="text" /> using given
        /// <paramref name="context" /> and formatted with the supplied
        /// arguments.
        /// 
        /// Similar to <c>npgettext</c> function.
        /// </summary>
        /// <param name="context">Context.</param>
        /// <param name="text">Singular form of message to translate.</param>
        /// <param name="pluralText">Plural form of message to translate.</param>
        /// <param name="n">Value that determines the plural form.</param>
        /// <param name="args">The values to format the string with.</param>
        /// <returns>
        /// Translated text.
        /// </returns>
        public string GetParticularPluralString([NotNull] string context,
            [NotNull] string text, [NotNull] string pluralText, long n,
            params object[] args)
        {
            Check.NotNull(context, nameof(context));
            Check.NotNull(text, nameof(text));
            Check.NotNull(pluralText, nameof(pluralText));

            return string.Format(
                GetParticularPluralString(context, text, pluralText, n), args);
        }

        #endregion

        #endregion

        #region # IStringLocalizer #

        /// <summary>
        /// Gets all string resources.
        /// </summary>
        /// <param name="includeParentCultures">A <see cref="bool" />
        /// indicating whether to include strings from parent cultures.</param>
        /// <returns>
        /// The strings.
        /// </returns>
        public IEnumerable<LocalizedString> GetAllStrings(bool includeParentCultures)
            => Processor.GetAllStrings(Culture, includeParentCultures);

        /// <summary>
        /// Creates a new <see cref="IStringLocalizer" /> for a specific
        /// <see cref="CultureInfo" />.
        /// </summary>
        /// <param name="culture">The <see cref="CultureInfo" /> to use.</param>
        /// <returns>
        /// A culture-specific <see cref="IStringLocalizer" />.
        /// </returns>
        public IStringLocalizer WithCulture(CultureInfo culture) => culture == null
            ? new GettextStringLocalizer(Processor)
            : new GettextWithCultureStringLocalizer(Processor, culture);

        /// <summary>
        /// Gets the string resource with the given name.
        /// </summary>
        /// <param name="name">The name of the string resource.</param>
        /// <returns>
        /// The string resource as a <see cref="LocalizedString" />.
        /// </returns>
        public LocalizedString this[string name]
        {
            get
            {
                Check.NotNull(name, nameof(name));

                var value = Processor.GetString(Culture, name);
                return new LocalizedString(name, value ?? name, value == null);
            }
        }

        /// <summary>
        /// Gets the string resource with the given name and formatted with
        /// the supplied arguments.
        /// </summary>
        /// <param name="name">The name of the string resource.</param>
        /// <param name="arguments">The values to format the string with.</param>
        /// <returns>
        /// The formatted string resource as a <see cref="LocalizedString" />.
        /// </returns>
        public LocalizedString this[string name, params object[] arguments]
        {
            get
            {
                Check.NotNull(name, nameof(name));

                var format = Processor.GetString(Culture, name);
                var value = string.Format(format ?? name, arguments);
                return new LocalizedString(name, value, format == null);
            }
        }

        #endregion

    }
}