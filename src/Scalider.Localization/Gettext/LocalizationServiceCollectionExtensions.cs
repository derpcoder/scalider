﻿#region # using statements #

using JetBrains.Annotations;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Localization;

#endregion

namespace Scalider.Localization.Gettext
{

    /// <summary>
    /// Extension methods for setting up localization services in an
    /// <see cref="IServiceCollection" />.
    /// </summary>
    public static class LocalizationServiceCollectionExtensions
    {

        /// <summary>
        /// Adds services required for application localization.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add
        /// the services to.</param>
        /// <param name="path">The path to the gettext resources.</param>
        /// <returns>
        /// The <see cref="IServiceCollection"/> so that additional calls can
        /// be chained.
        /// </returns>
        public static IServiceCollection AddGettextLocalization(
            this IServiceCollection services, [NotNull] string path)
        {
            Check.NotNull(services, nameof(services));
            Check.NotNullOrEmpty(path, nameof(path));

            services.TryAddSingleton(typeof(IGettextProcessor),
                p => new DefaultGettextProcessor(path));

            services.TryAddSingleton(typeof(IStringLocalizerFactory),
                typeof(GettextStringLocalizerFactory));

            services.TryAddTransient(typeof(IStringLocalizer),
                typeof(GettextStringLocalizer));

            services.TryAddTransient(typeof(IStringLocalizer<>),
                typeof(StringLocalizer<>));

            return services;
        }

    }
}