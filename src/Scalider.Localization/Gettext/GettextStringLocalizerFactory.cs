﻿#region # using statements #

using System;
using System.Reflection;
using Microsoft.Extensions.Localization;

#endregion

namespace Scalider.Localization.Gettext
{

    /// <summary>
    /// An <see cref="IStringLocalizerFactory"/> that creates instances of
    /// <see cref="GettextStringLocalizer"/>.
    /// </summary>
    public class GettextStringLocalizerFactory : IStringLocalizerFactory
    {

        #region # Variables #

        private readonly IGettextProcessor _processor;

        #endregion

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="GettextStringLocalizerFactory"/> class.
        /// </summary>
        /// <param name="processor"></param>
        public GettextStringLocalizerFactory(IGettextProcessor processor)
        {
            _processor = processor;
        }

        #region # IStringLocalizerFactory #

        /// <summary>
        /// Creates an <see cref="IStringLocalizer" /> using the
        /// <see cref="Assembly" /> and <see cref="Type.FullName" /> of the
        /// specified <see cref="Type" />.
        /// </summary>
        /// <param name="resourceSource">The <see cref="Type" />.</param>
        /// <returns>
        /// The <see cref="IStringLocalizer" />.
        /// </returns>
        public IStringLocalizer Create(Type resourceSource) =>
            new GettextStringLocalizer(_processor);

        /// <summary>
        /// Creates an <see cref="IStringLocalizer" />.
        /// </summary>
        /// <param name="baseName">The base name of the resource to load strings from.</param>
        /// <param name="location">The location to load resources from.</param>
        /// <returns>
        /// The <see cref="IStringLocalizer" />.
        /// </returns>
        public IStringLocalizer Create(string baseName, string location) =>
            new GettextStringLocalizer(_processor);

        #endregion

    }

}