﻿#region # using statements #

using System.Collections.Generic;
using System.Globalization;
using JetBrains.Annotations;
using Microsoft.Extensions.Localization;

#endregion

namespace Scalider.Localization.Gettext
{

    /// <summary>
    /// Defines the basic functionality of a Gettext processor.
    /// </summary>
    public interface IGettextProcessor
    {

        /// <summary>
        /// Returns <paramref name="text" /> translated into the selected
        /// language.
        /// 
        /// Similar to <c>gettext</c> function.
        /// </summary>
        /// <param name="culture">The selected language.</param>
        /// <param name="text">Text to translate.</param>
        /// <returns>
        /// Translated text.
        /// </returns>
        string GetString([NotNull] CultureInfo culture, [NotNull] string text);

        /// <summary>
        /// Returns the plural form for <paramref name="n"/> of the translation
        /// of <paramref name="text" />.
        /// 
        /// Similar to <c>ngettext</c> function.
        /// </summary>
        /// <param name="culture">The selected language.</param>
        /// <param name="text">Singular form of message to translate.</param>
        /// <param name="pluralText">Plural form of message to translate.</param>
        /// <param name="n">Value that determines the plural form.</param>
        /// <returns>
        /// Translated text.
        /// </returns>
        string GetPluralString([NotNull] CultureInfo culture, [NotNull] string text,
            [NotNull] string pluralText, long n);

        /// <summary>
        /// Returns <paramref name="text" /> translated into the selected
        /// language using given <paramref name="context" />.
        /// 
        /// Similar to <c>pgettext</c> function.
        /// </summary>
        /// <param name="culture">The selected language.</param>
        /// <param name="context">Context.</param>
        /// <param name="text">Text to translate.</param>
        /// <returns>
        /// Translated text.
        /// </returns>
        string GetParticularString([NotNull] CultureInfo culture,
            [NotNull] string context, [NotNull] string text);

        /// <summary>
        /// Returns the plural form for <paramref name="n" /> of the
        /// translation of <paramref name="text" /> using given
        /// <paramref name="context" />.
        /// 
        /// Similar to <c>npgettext</c> function.
        /// </summary>
        /// <param name="culture">The selected language.</param>
        /// <param name="context">Context.</param>
        /// <param name="text">Singular form of message to translate.</param>
        /// <param name="pluralText">Plural form of message to translate.</param>
        /// <param name="n">Value that determines the plural form.</param>
        /// <returns>
        /// Translated text.
        /// </returns>
        string GetParticularPluralString([NotNull] CultureInfo culture,
            [NotNull] string context, [NotNull] string text,
            [NotNull] string pluralText, long n);

        /// <summary>
        /// Returns all the localized strings for the selected language.
        /// </summary>
        /// <param name="culture">The selected language</param>
        /// <param name="includeAncestorCultures">Whether to include the parent
        /// language localized strings.</param>
        /// <returns>
        /// The localizable strings.
        /// </returns>
        IEnumerable<LocalizedString> GetAllStrings([NotNull] CultureInfo culture,
            bool includeAncestorCultures);

    }

}