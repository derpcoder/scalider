﻿#region # using statements #

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using JetBrains.Annotations;
using Microsoft.Extensions.Localization;
using NGettext;
using NGettext.Loaders;
using NGettext.PluralCompile;

#endregion

namespace Scalider.Localization.Gettext
{

    /// <summary>
    /// Represents the default <see cref="IGettextProcessor"/>.
    /// </summary>
    public class DefaultGettextProcessor : IGettextProcessor
    {

        #region # Variables #

        private readonly string _path;
        private readonly ConcurrentDictionary<string, Catalog> _catalogs;

        #endregion

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="DefaultGettextProcessor"/> class.
        /// </summary>
        /// <param name="path"></param>
        public DefaultGettextProcessor([NotNull] string path)
        {
            Check.NotNullOrEmpty(path, nameof(path));

            _path = path;
            _catalogs = new ConcurrentDictionary<string, Catalog>(
                StringComparer.InvariantCultureIgnoreCase
            );
        }

        #region # Methods #

        #region == Private ==

        private static Catalog CreateCatalog(string path) => new Catalog(
            new MoCompilingPluralLoader(path, new MoFileParser()));

        private Catalog GetCatalog(CultureInfo culture)
        {
            if (_catalogs.TryGetValue(culture.Name, out Catalog catalog))
                return catalog;

            // The catalog has not ben loaded yet, try to load it
            var path = Path.Combine(_path, culture.Name);
            if (Directory.Exists(path))
            {
                var lcMessagesPath = Path.Combine(path, "LC_MESSAGES.mo");
                if (File.Exists(lcMessagesPath))
                    catalog = CreateCatalog(lcMessagesPath);
                else
                {
                    path += ".mo";
                    if (File.Exists(path))
                        catalog = CreateCatalog(path);
                }
            }
            else
            {
                path += ".mo";
                if (File.Exists(path))
                    catalog = CreateCatalog(path);
            }

            // Done
            _catalogs[culture.Name] = catalog;
            return catalog;
        }

        private string Get(CultureInfo culture, Func<Catalog, string> getter)
        {
            var currentCulture = culture;
            while (true)
            {
                var catalog = GetCatalog(currentCulture);
                if (catalog != null)
                {
                    var result = getter(catalog);
                    if (result != null)
                        return result;
                }

                // Move to parent culture
                if (Equals(currentCulture.Parent, currentCulture))
                    break;

                currentCulture = currentCulture.Parent;
            }

            // Not found
            return null;
        }

        #endregion

        #endregion

        #region # IGettextProcessor #

        /// <summary>
        /// Returns <paramref name="text" /> translated into the selected
        /// language.
        /// 
        /// Similar to <c>gettext</c> function.
        /// </summary>
        /// <param name="culture">The selected language.</param>
        /// <param name="text">Text to translate.</param>
        /// <returns>
        /// Translated text.
        /// </returns>
        public string GetString(CultureInfo culture, string text)
        {
            Check.NotNull(culture, nameof(culture));
            Check.NotNull(text, nameof(text));

            return Get(culture, c => c.GetStringDefault(text, null));
        }

        /// <summary>
        /// Returns the plural form for <paramref name="n"/> of the translation
        /// of <paramref name="text" />.
        /// 
        /// Similar to <c>ngettext</c> function.
        /// </summary>
        /// <param name="culture">The selected language.</param>
        /// <param name="text">Singular form of message to translate.</param>
        /// <param name="pluralText">Plural form of message to translate.</param>
        /// <param name="n">Value that determines the plural form.</param>
        /// <returns>
        /// Translated text.
        /// </returns>
        public string GetPluralString(CultureInfo culture, string text,
            string pluralText, long n)
        {
            Check.NotNull(culture, nameof(culture));
            Check.NotNull(text, nameof(text));
            Check.NotNull(pluralText, nameof(pluralText));

            return Get(culture, c => c.GetPluralString(text, pluralText, n));
        }

        /// <summary>
        /// Returns <paramref name="text" /> translated into the selected
        /// language using given <paramref name="context" />.
        /// 
        /// Similar to <c>pgettext</c> function.
        /// </summary>
        /// <param name="culture">The selected language.</param>
        /// <param name="context">Context.</param>
        /// <param name="text">Text to translate.</param>
        /// <returns>
        /// Translated text.
        /// </returns>
        public string GetParticularString(CultureInfo culture, string context,
            string text)
        {
            Check.NotNull(culture, nameof(culture));
            Check.NotNull(context, nameof(context));
            Check.NotNull(text, nameof(text));

            return Get(culture, c => c.GetParticularString(context, text));
        }

        /// <summary>
        /// Returns the plural form for <paramref name="n" /> of the
        /// translation of <paramref name="text" /> using given
        /// <paramref name="context" />.
        /// 
        /// Similar to <c>npgettext</c> function.
        /// </summary>
        /// <param name="culture">The selected language.</param>
        /// <param name="context">Context.</param>
        /// <param name="text">Singular form of message to translate.</param>
        /// <param name="pluralText">Plural form of message to translate.</param>
        /// <param name="n">Value that determines the plural form.</param>
        /// <returns>
        /// Translated text.
        /// </returns>
        public string GetParticularPluralString(CultureInfo culture, string context,
            string text, string pluralText, long n)
        {
            Check.NotNull(culture, nameof(culture));
            Check.NotNull(context, nameof(context));
            Check.NotNull(text, nameof(text));
            Check.NotNull(pluralText, nameof(pluralText));

            return Get(culture,
                c => c.GetParticularPluralString(context, text, pluralText, n));
        }

        /// <summary>
        /// Returns all the localized strings for the selected language.
        /// </summary>
        /// <param name="culture">The selected language</param>
        /// <param name="includeAncestorCultures">Whether to include the parent
        /// language localized strings.</param>
        /// <returns>
        /// The localizable strings.
        /// </returns>
        public IEnumerable<LocalizedString> GetAllStrings(CultureInfo culture,
            bool includeAncestorCultures)
        {
            throw new NotSupportedException();
        }

        #endregion

    }

}