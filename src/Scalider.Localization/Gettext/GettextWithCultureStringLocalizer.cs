﻿#region # using statements #

using System.Globalization;
using JetBrains.Annotations;
using Microsoft.Extensions.Localization;

#endregion

namespace Scalider.Localization.Gettext
{

    /// <summary>
    /// Represents a <see cref="GettextStringLocalizer"/> with a custom
    /// <see cref="CultureInfo"/>.
    /// </summary>
    public class GettextWithCultureStringLocalizer : GettextStringLocalizer
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="GettextWithCultureStringLocalizer"/> class.
        /// </summary>
        /// <param name="processor"></param>
        /// <param name="cultureInfo"></param>
        public GettextWithCultureStringLocalizer(
            [NotNull] IGettextProcessor processor, [NotNull] CultureInfo cultureInfo)
            : base(processor)
        {
            Check.NotNull(processor, nameof(processor));
            Check.NotNull(cultureInfo, nameof(cultureInfo));

            Culture = cultureInfo;
        }

        #region # Properties #

        #region == Overrides ==

        /// <summary>
        /// Gets the <see cref="CultureInfo"/> for this
        /// <see cref="IStringLocalizer"/>.
        /// </summary>
        protected override CultureInfo Culture { get; }

        #endregion

        #endregion

    }
}