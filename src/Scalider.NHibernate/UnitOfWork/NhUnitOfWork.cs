﻿#region # using statements #

using System;
using System.Data;
using JetBrains.Annotations;
using Scalider.Domain.UnitOfWork;
using NHibernate;

#endregion

namespace Scalider.NHibernate.UnitOfWork
{

    /// <summary>
    /// Represents a <see cref="IUnitOfWork"/> that uses NHibernate as backing
    /// layer.
    /// </summary>
    public class NhUnitOfWork : Disposable, IUnitOfWork
    {
        #region # Variables #

        private readonly IDbContextFactory _dbContextFactory;

        //
        private IDbContext _dbContext;
        private ITransaction _transaction;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="NhUnitOfWork"/> class.
        /// </summary>
        /// <param name="dbContextFactory"></param>
        public NhUnitOfWork(IDbContextFactory dbContextFactory)
        {
            Check.NotNull(dbContextFactory, nameof(dbContextFactory));

            _dbContextFactory = dbContextFactory;
            Id = Guid.NewGuid().ToString("N");
        }

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">true to release both managed and unmanaged
        /// resources; false to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (!disposing)
                return;

            _transaction?.Dispose();
            _dbContext?.Dispose();
        }

        #endregion

        #region == Public ==

        /// <summary>
        /// Gets the current <see cref="IDbContext"/> for the unit of work.
        /// </summary>
        [NotNull, MustUseReturnValue]
        public virtual IDbContext GetDbContext()
        {
            ThrowIfDisposed();
            return _dbContext ?? (_dbContext = _dbContextFactory.Create());
        }

        #endregion

        #endregion

        #region # IUnitOfWork #

        /// <summary>
        /// Gets the unique identifier of the unit of work.
        /// </summary>
        public string Id { get; }

        /// <summary>
        /// Begins a transaction on the underlying store connection using the
        /// default <see cref="IsolationLevel"/>.
        /// </summary>
        /// <returns>
        /// A transaction object wrapping access to the underlying store's
        /// transaction object.
        /// </returns>
        public virtual void BeginTransaction()
        {
            ThrowIfDisposed();
            if (_transaction == null)
                _transaction = GetDbContext().BeginTransaction();
        }

        public virtual void BeginTransaction(IsolationLevel isolationLevel)
        {
            ThrowIfDisposed();
            if (_transaction == null)
                _transaction = GetDbContext().BeginTransaction(isolationLevel);
        }

        /// <summary>
        /// Flush the associated database connection and end the unit of work.
        /// </summary>
        public virtual void Commit()
        {
            ThrowIfDisposed();
            if (_transaction == null)
                GetDbContext().Flush();
            else
            {
                try
                {
                    if (_transaction == null || !_transaction.IsActive)
                        return;

                    _transaction.Commit();
                    _transaction = null;
                }
                catch (Exception)
                {
                    if (_transaction == null || !_transaction.IsActive)
                        return;

                    _transaction.Rollback();
                    _transaction = null;
                }
                finally
                {
                    _dbContext.Dispose();
                    _dbContext = null;
                }
            }
        }

        /// <summary>
        /// Force the underlying transaction to roll back.
        /// </summary>
        public virtual void Rollback()
        {
            if (_transaction == null)
                GetDbContext().Clear();
            else
            {
                try
                {
                    if (_transaction == null || !_transaction.IsActive)
                        return;

                    _transaction.Rollback();
                    _transaction = null;
                }
                finally
                {
                    _dbContext.Dispose();
                    _dbContext = null;
                }
            }
        }

        #endregion
    }
}