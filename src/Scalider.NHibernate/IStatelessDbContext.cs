﻿namespace Scalider.NHibernate
{

    /// <summary>
    /// Defines the basic functionality of a stateless database context.
    /// </summary>
    /// <remarks>
    /// More about stateless sessions on
    /// A stateless session is, well, stateless. Unlike a normal session, it 
    /// doesn’t maintain a reference to the entities that it loads. As such,
    /// it’s perfectly suited to loading entities for display-only purposes.
    /// 
    /// For that type of task, you generally load the entities from the
    /// database, throw them on the form and forget about them. A stateless
    /// session is just what you need in this case. But stateless sessions come
    /// with a set of limitations. Chief among them in this case is that
    /// stateless sessions do not support lazy loading, do not involve
    /// themselves in the usual NHibernate event mode and do not make use of
    /// NHibernate’s caching features.
    /// </remarks>
    public interface IStatelessDbContext : IDbContext
    {
    }
}