﻿#region # using statements #

using System;
using FluentNHibernate.Mapping;
using JetBrains.Annotations;
using Scalider.Domain.Entities;

#endregion

namespace Scalider.NHibernate.Mapping
{

    /// <summary>
    /// Defines the mapping for an entity.
    /// </summary>
    /// <typeparam name="TEntity">The type encapsulating the type of the
    /// entity.</typeparam>
    /// <typeparam name="TKey">The type encapsulating the primary key of the
    /// entity.</typeparam>
    public abstract class EntityMap<TEntity, TKey> : ClassMap<TEntity>
        where TEntity : class, IEntity<TKey>
        where TKey : IEquatable<TKey>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="EntityMap{TEntity,TKey}"/> class.
        /// </summary>
        /// <param name="tableName">The name of the table to map.</param>
        protected EntityMap([NotNull] string tableName)
        {
            Check.NotNullOrEmpty(tableName, nameof(tableName));

            Table(tableName);
            Id(x => x.Id).Not.Nullable();

            // Soft delete
            if (typeof(TEntity).Is<ISoftDelete>())
                Where("IsDeleted = 0");
        }
    }
}