﻿#region # using statements #

using FluentNHibernate.Mapping;
using JetBrains.Annotations;
using Scalider.Domain.Entities;

#endregion

namespace Scalider.NHibernate.Mapping
{

    /// <summary>
    /// Provides extension methods for the entity mapping class.
    /// </summary>
    public static class MappingExtensions
    {

        /// <summary>
        /// Adds the "IsDeleted" column map to the given mapping class with the
        /// default column name.
        /// </summary>
        /// <typeparam name="TEntity">The type encapsulating the type of the
        /// entity.</typeparam>
        /// <param name="mapping">The mapping class.</param>
        public static void MapIsDeleted<TEntity>(
            [NotNull] this ClassMap<TEntity> mapping)
            where TEntity : class, ISoftDelete
        {
            Check.NotNull(mapping, nameof(mapping));
            MapIsDeleted(mapping, "IsDeleted");
        }

        /// <summary>
        /// Adds the "IsDeleted" column map to the given mapping class.
        /// </summary>
        /// <typeparam name="TEntity">The type encapsulating the type of the
        /// entity.</typeparam>
        /// <param name="mapping">The mapping class.</param>
        /// <param name="columnName">The name of the "IsDeleted" column.</param>
        public static void MapIsDeleted<TEntity>(
            [NotNull] this ClassMap<TEntity> mapping, [NotNull] string columnName)
            where TEntity : class, ISoftDelete
        {
            Check.NotNull(mapping, nameof(mapping));
            Check.NotNullOrEmpty(columnName, nameof(columnName));

            mapping.Map(t => t.IsDeleted)
                   .Column(columnName)
                   .Not.Nullable()
                   .Default("0");
        }

    }
}