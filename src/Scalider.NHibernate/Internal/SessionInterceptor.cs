﻿#region # using statements #

using Scalider.Domain.Entities;
using NHibernate;

#endregion

namespace Scalider.NHibernate.Internal
{

    /// <summary>
    /// Represents an <see cref="IInterceptor"/> that automatically handles the
    /// <see cref="ISoftDelete"/> entities and others.
    /// </summary>
    internal sealed class SessionInterceptor : EmptyInterceptor
    {
        #region # Variables #

        private ISession _session;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SessionInterceptor"/>
        /// class.
        /// </summary>
        public SessionInterceptor()
        {
        }

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Called when a session-scoped (and <b>only</b> session scoped)
        /// interceptor is attached to a session
        /// </summary>
        /// <remarks>
        /// session-scoped-interceptor is an instance of the interceptor used
        /// only for one session. The use of singleton-interceptor may cause
        /// problems in multi-thread scenario.
        /// </remarks>
        public override void SetSession(ISession session)
        {
            _session = session;
        }

        #endregion

        #endregion
    }
}