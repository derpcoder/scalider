﻿#region # using statements #

using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;
using System.Reflection;
using NHibernate.Hql.Ast;
using NHibernate.Linq;
using NHibernate.Linq.Functions;
using NHibernate.Linq.Visitors;

#endregion

namespace Scalider.NHibernate.Linq.Functions
{

    /// <summary>
    /// Represents an <see cref="IHqlGeneratorForMethod"/> that provides a
    /// Linq-to-Hql generator for <see cref="IEquatable{T}.Equals(T)"/>.
    /// </summary>
    /// <typeparam name="T">The type encapsulating the type of objects to
    /// compare.</typeparam>
    public class EqualityGenerator<T> : BaseHqlGeneratorForMethod
        where T : IEquatable<T>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="EqualityGenerator{T}"/> class.
        /// </summary>
        [SuppressMessage("ReSharper", "ReturnValueOfPureMethodIsNotUsed")]
        public EqualityGenerator()
        {
            SupportedMethods = new[]
            {
                ReflectionHelper.GetMethodDefinition<T>(x => x.Equals(default(T)))
            };
        }

        #region # Methods #

        #region == Overrides ==

        public override HqlTreeNode BuildHql(MethodInfo method,
            Expression targetObject, ReadOnlyCollection<Expression> arguments,
            HqlTreeBuilder treeBuilder, IHqlExpressionVisitor visitor)
        {
            return treeBuilder.Equality(
                visitor.Visit(targetObject).AsExpression(),
                visitor.Visit(arguments[0]).AsExpression()
            );
        }

        #endregion

        #endregion
    }
}