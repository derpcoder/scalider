﻿#region # using statements #

using System;
using NHibernate.Linq.Functions;

#endregion

namespace Scalider.NHibernate.Linq.Functions
{

    /// <summary>
    /// Represents a <see cref="ILinqToHqlGeneratorsRegistry"/> that adds some
    /// extra Linq-to-Hql generators.
    /// </summary>
    public class LinqToHqlGeneratorsRegistry :
        DefaultLinqToHqlGeneratorsRegistry
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="LinqToHqlGeneratorsRegistry"/> class.
        /// </summary>
        public LinqToHqlGeneratorsRegistry()
        {
            this.Merge(new StringEqualsGenerator());

            // IEquitable<T>
            this.Merge(new EqualityGenerator<Guid>());
            this.Merge(new EqualityGenerator<char>());
            this.Merge(new EqualityGenerator<byte>());
            this.Merge(new EqualityGenerator<short>());
            this.Merge(new EqualityGenerator<int>());
            this.Merge(new EqualityGenerator<long>());
            this.Merge(new EqualityGenerator<ushort>());
            this.Merge(new EqualityGenerator<uint>());
            this.Merge(new EqualityGenerator<ulong>());
            this.Merge(new EqualityGenerator<decimal>());
            this.Merge(new EqualityGenerator<double>());
            this.Merge(new EqualityGenerator<float>());
        }

    }
}