﻿#region # using statements #

using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;
using System.Reflection;
using NHibernate.Hql.Ast;
using NHibernate.Impl;
using NHibernate.Linq;
using NHibernate.Linq.Functions;
using NHibernate.Linq.Visitors;

#endregion

namespace Scalider.NHibernate.Linq.Functions
{

    /// <summary>
    /// Represents an <see cref="IHqlGeneratorForMethod"/> that provides a
    /// Linq-to-Hql generator for
    /// <see cref="string.Equals(string, StringComparison)"/> and
    /// <see cref="string.Equals(string, string, StringComparison)"/>.
    /// </summary>
    public class StringEqualsGenerator : BaseHqlGeneratorForMethod
    {
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="StringEqualsGenerator"/> class.
        /// </summary>
        [SuppressMessage("ReSharper", "ReturnValueOfPureMethodIsNotUsed")]
        public StringEqualsGenerator()
        {
            SupportedMethods = new[]
            {
                ReflectionHelper.GetMethodDefinition<string>(
                    x => x.Equals("", StringComparison.CurrentCulture)),
                ReflectionHelper.GetMethodDefinition<string>(
                    x => string.Equals("", "", StringComparison.CurrentCulture))
            };
        }

        #region # Methods #

        #region == Overrides ==

        public override HqlTreeNode BuildHql(MethodInfo method,
            Expression targetObject,
            ReadOnlyCollection<Expression> arguments, HqlTreeBuilder treeBuilder,
            IHqlExpressionVisitor visitor)
        {
            HqlTreeNode leftNode;
            HqlTreeNode rightNode;
            StringComparison? comparison;
            switch (arguments.Count)
            {
                case 2:
                    leftNode = visitor.Visit(targetObject);
                    rightNode = visitor.Visit(arguments[0]);
                    comparison =
                        (StringComparison?)
                        ExpressionProcessor.FindValue(arguments[1]);
                    break;
                case 3:
                    leftNode = visitor.Visit(arguments[0]);
                    rightNode = visitor.Visit(arguments[1]);
                    comparison =
                        (StringComparison?)
                        ExpressionProcessor.FindValue(arguments[2]);
                    break;
                default:
                    throw new ArgumentException("Invalid number of arguments",
                        nameof(arguments));
            }

            // Done
            if (Helper.IsCaseInvariant(comparison))
            {
                return treeBuilder.Equality(
                    treeBuilder.MethodCall("LOWER", leftNode.AsExpression()),
                    treeBuilder.MethodCall("LOWER", rightNode.AsExpression())
                );
            }

            return treeBuilder.Equality(leftNode.AsExpression(),
                rightNode.AsExpression());
        }

        #endregion

        #endregion
    }
}