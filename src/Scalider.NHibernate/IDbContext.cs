﻿#region # using statements #

using System;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using JetBrains.Annotations;
using NHibernate;

#endregion

namespace Scalider.NHibernate
{

    /// <summary>
    /// Defines the basic functionality of a database context.
    /// </summary>
    public interface IDbContext : IDisposable
    {

        /// <summary>
        /// Gets the active database connection for this database context.
        /// </summary>
        IDbConnection Connection { get; }

        /// <summary>
        /// Return the persistent instance of the given entity class with the
        /// given identifier, assuming that the instance exists.
        /// </summary>
        /// <typeparam name="T">The type encapsulating the entity to be loaded.</typeparam>
        /// <param name="id">A valid identifier of an existing persistent
        /// instance of the class.</param>
        /// <returns>
        /// The persistent instance or proxy.
        /// </returns>
        T Load<T>(object id) where T : class;

        /// <summary>
        /// Creates an <see cref="IQueryable{T}"/> object for the given entity
        /// type.
        /// </summary>
        /// <typeparam name="T">The type encapsulating the entity.</typeparam>
        /// <returns>
        /// An <see cref="IQueryable"/> object.
        /// </returns>
        IQueryable<T> Query<T>() where T : class;

        /// <summary>
        /// Creates a new <see cref="IQueryOver{TRoot}"/> for the entity class.
        /// </summary>
        /// <typeparam name="T">The type encapsulating the entity.</typeparam>
        /// <returns>
        /// An <see cref="IQueryOver{TRoot}"/> object.
        /// </returns>
        IQueryOver<T, T> QueryOver<T>() where T : class;

        /// <summary>
        /// Creates a new <see cref="IQueryOver{TRoot}"/> for the entity class.
        /// </summary>
        /// <typeparam name="T">The type encapsulating the entity.</typeparam>
        /// <param name="alias">The alias of the entity.</param>
        /// <returns>
        /// An <see cref="IQueryOver{TRoot}"/> object.
        /// </returns>
        IQueryOver<T, T> QueryOver<T>([NotNull] Expression<Func<T>> alias)
            where T : class;

        /// <summary>
        /// Persist the given transient instance, first assigning a generated
        /// identifier.
        /// </summary>
        /// <typeparam name="T">The type encapsulating the entity.</typeparam>
        /// <param name="entity">A transient instance of a persistent class.</param>
        void Insert<T>([NotNull] T entity) where T : class;

        /// <summary>
        /// Update the persistent instance with the identifier of the given
        /// transient instance.
        /// </summary>
        /// <typeparam name="T">The type encapsulating the entity.</typeparam>
        /// <param name="entity">A transient instance containing updated state.</param>
        void Update<T>([NotNull] T entity) where T : class;

        /// <summary>
        /// Remove a persistent instance from the datastore.
        /// </summary>
        /// <typeparam name="T">The type encapsulating the entity.</typeparam>
        /// <param name="entity">The instance to be removed.</param>
        void Delete<T>([NotNull] T entity) where T : class;

        /// <summary>
        /// Begins a transaction on the underlying store connection using the
        /// default <see cref="IsolationLevel"/>.
        /// </summary>
        /// <returns>
        /// A transaction object wrapping access to the underlying store's
        /// transaction object.
        /// </returns>
        ITransaction BeginTransaction();

        /// <summary>
        /// Begins a transaction on the underlying store connection using the
        /// specified isolation level.
        /// </summary>
        /// <param name="isolationLevel">The database isolation level with
        /// which the underlying store transaction will be created.</param>
        /// <returns>
        /// A transaction object wrapping access to the underlying store's
        /// transaction object.
        /// </returns>
        ITransaction BeginTransaction(IsolationLevel isolationLevel);

        /// <summary>
        /// Force flushes the changes made to the database context.
        /// </summary>
        void Flush();

        /// <summary>
        /// Clears the database context, evicts all the loaded entities and
        /// cancel any pending insert, update or delete operation.
        /// </summary>
        void Clear();

    }
}