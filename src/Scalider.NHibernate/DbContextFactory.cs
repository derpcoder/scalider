﻿#region # using statements #

using System;
using System.Diagnostics.CodeAnalysis;
using JetBrains.Annotations;
using Scalider.NHibernate.Internal;
using Scalider.NHibernate.Linq.Functions;
using NHibernate;
using NHibernate.Cfg;

#endregion

namespace Scalider.NHibernate
{

    /// <summary>
    /// Represents the default implementation of the
    /// <see cref="IDbContextFactory"/>.
    /// </summary>
    public class DbContextFactory : Disposable, IDbContextFactory
    {
        #region # Variables #

        private readonly Action<Configuration> _configurationCallback;
        private ISessionFactory _sessionFactory;
        private bool _ownsSessionFactory;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="DbContextFactory"/>
        /// class.
        /// </summary>
        /// <param name="callback"></param>
        public DbContextFactory([NotNull] Action<Configuration> callback)
        {
            Check.NotNull(callback, nameof(callback));
            _configurationCallback = callback;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DbContextFactory"/>
        /// class.
        /// </summary>
        /// <param name="sessionFactory"></param>
        /// <param name="ownsSessionFactory">A value indicating whether the
        /// <paramref name="sessionFactory"/> should be disposed when this
        /// database context is disposed.</param>
        public DbContextFactory([NotNull] ISessionFactory sessionFactory,
            bool ownsSessionFactory = true)
        {
            Check.NotNull(sessionFactory, nameof(sessionFactory));

            _sessionFactory = sessionFactory;
            _ownsSessionFactory = ownsSessionFactory;
        }

        #region # Properties #

        #region == Protected ==

        /// <summary>
        /// Gets the <see cref="ISessionFactory"/> to be used when creating
        /// database contexts.
        /// </summary>
        [SuppressMessage("ReSharper", "InvertIf")]
        protected virtual ISessionFactory SessionFactory
        {
            get
            {
                ThrowIfDisposed();
                if (_sessionFactory == null)
                {
                    _ownsSessionFactory = true;

                    var config = new Configuration();
                    config.LinqToHqlGeneratorsRegistry<LinqToHqlGeneratorsRegistry>();

                    _configurationCallback.Invoke(config);
                    _sessionFactory = config.BuildSessionFactory();
                }

                // Done
                return _sessionFactory;
            }
        }

        #endregion

        #endregion

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">true to release both managed and unmanaged
        /// resources; false to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (!disposing || !_ownsSessionFactory)
                return;

            _sessionFactory?.Dispose();
        }

        #endregion

        #endregion

        #region # IDbContextFactory #

        /// <summary>
        /// Creates a new <see cref="IDbContext"/> instance.
        /// </summary>
        /// <returns>
        /// The created <see cref="IDbContext"/> object.
        /// </returns>
        public virtual IDbContext Create()
        {
            ThrowIfDisposed();
            return new DbContext(SessionFactory.OpenSession(new SessionInterceptor()));
        }

        #endregion
    }
}