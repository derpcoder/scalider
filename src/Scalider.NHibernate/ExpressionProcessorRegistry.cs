﻿#region # using statements #

using System;
using System.Linq.Expressions;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Impl;

#endregion

namespace Scalider.NHibernate
{

    /// <summary>
    /// Represents a centralized registry for extra lambda expressions to
    /// criterion/order converters.
    /// </summary>
    public static class ExpressionProcessorRegistry
    {
        #region # Variables #

        private static bool _registered;
        private static IInternalLogger _logger;

        #endregion

        /// <summary>
        /// Register all the extra custom lambda expressions to criterion/order
        /// converters only once.
        /// </summary>
        public static void RegisterProcessorsIfNotRegistered()
        {
            if (_registered)
                return;

            // Try to retrieve logger
            try
            {
                _logger =
                    LoggerProvider.LoggerFor(typeof(ExpressionProcessorRegistry));
            }
            catch
            {
                // ignore
            }

            // Register functions
            RegisterCustomMethods();

            // Done
            _registered = true;
        }

        private static IProjection FindProjection(
            System.Linq.Expressions.Expression expression)
        {
            if (expression is MemberExpression)
            {
                try
                {
                    var memberProjection =
                        ExpressionProcessor.FindMemberProjection(expression);
                    return memberProjection.AsProjection();
                }
                catch
                {
                    // ignore
                }
            }
            else
            {
                try
                {
                    var value = ExpressionProcessor.FindValue(expression);
                    return Projections.Constant(value);
                }
                catch
                {
                    // ignore
                }
            }

            // Failed to retrieve projection
            return null;
        }

        private static void TryRegisterCustomMethodCall(Expression<Func<bool>> function,
            Func<MethodCallExpression, ICriterion> functionProcessor)
        {
            try
            {
                ExpressionProcessor.RegisterCustomMethodCall(function,
                    functionProcessor);
            }
            catch (Exception e)
            {
                _logger?.Error(
                    "There was an unexpected error while trying to register the " +
                    $"custom method call '{function.Body}'", e);
            }
        }

        #region # RegisterCustomMethods #

        private static void RegisterCustomMethods()
        {
            // String
            TryRegisterCustomMethodCall(() => "".Equals(""),
                ProcessEquitableEqualsCriterion);
            TryRegisterCustomMethodCall(
                () => "".Equals("", StringComparison.CurrentCulture),
                ProcessStringEqualsCriterion);

            TryRegisterCustomMethodCall(
                () => string.Equals("", ""), ProcessStringEqualsCriterion);
            TryRegisterCustomMethodCall(
                () => string.Equals("", "", StringComparison.CurrentCulture),
                ProcessStringEqualsCriterion);

            // IEquitable<T>
            TryRegisterCustomMethodCall(
                () => default(Guid).Equals(Guid.Empty),
                ProcessEquitableEqualsCriterion);
            TryRegisterCustomMethodCall(
                () => ((IEquatable<Guid>)null).Equals(Guid.Empty),
                ProcessEquitableEqualsCriterion);

            TryRegisterCustomMethodCall(
                () => default(char).Equals('\0'), ProcessEquitableEqualsCriterion);
            TryRegisterCustomMethodCall(
                () => ((IEquatable<char>)null).Equals('\0'),
                ProcessEquitableEqualsCriterion);

            TryRegisterCustomMethodCall(
                () => default(byte).Equals(0), ProcessEquitableEqualsCriterion);
            TryRegisterCustomMethodCall(
                () => ((IEquatable<byte>)null).Equals(0),
                ProcessEquitableEqualsCriterion);

            TryRegisterCustomMethodCall(
                () => default(short).Equals(0), ProcessEquitableEqualsCriterion);
            TryRegisterCustomMethodCall(
                () => ((IEquatable<short>)null).Equals(0),
                ProcessEquitableEqualsCriterion);

            TryRegisterCustomMethodCall(
                () => default(int).Equals(0), ProcessEquitableEqualsCriterion);
            TryRegisterCustomMethodCall(
                () => ((IEquatable<int>)null).Equals(0),
                ProcessEquitableEqualsCriterion);

            TryRegisterCustomMethodCall(
                () => default(long).Equals(0), ProcessEquitableEqualsCriterion);
            TryRegisterCustomMethodCall(
                () => ((IEquatable<long>)null).Equals(0),
                ProcessEquitableEqualsCriterion);

            TryRegisterCustomMethodCall(
                () => default(ushort).Equals(0), ProcessEquitableEqualsCriterion);
            TryRegisterCustomMethodCall(
                () => ((IEquatable<ushort>)null).Equals(0),
                ProcessEquitableEqualsCriterion);

            TryRegisterCustomMethodCall(
                () => default(uint).Equals(0), ProcessEquitableEqualsCriterion);
            TryRegisterCustomMethodCall(
                () => ((IEquatable<uint>)null).Equals(0),
                ProcessEquitableEqualsCriterion);

            TryRegisterCustomMethodCall(
                () => default(ulong).Equals(0), ProcessEquitableEqualsCriterion);
            TryRegisterCustomMethodCall(
                () => ((IEquatable<ulong>)null).Equals(0),
                ProcessEquitableEqualsCriterion);

            TryRegisterCustomMethodCall(
                () => default(decimal).Equals(0), ProcessEquitableEqualsCriterion);
            TryRegisterCustomMethodCall(
                () => ((IEquatable<decimal>)null).Equals(0),
                ProcessEquitableEqualsCriterion);

            TryRegisterCustomMethodCall(
                () => default(double).Equals(0), ProcessEquitableEqualsCriterion);
            TryRegisterCustomMethodCall(
                () => ((IEquatable<double>)null).Equals(0),
                ProcessEquitableEqualsCriterion);

            TryRegisterCustomMethodCall(
                () => default(float).Equals(0), ProcessEquitableEqualsCriterion);
            TryRegisterCustomMethodCall(
                () => ((IEquatable<float>)null).Equals(0),
                ProcessEquitableEqualsCriterion);
        }

        #endregion

        #region # ProcessEquitableEqualsCriterion #

        private static ICriterion ProcessEquitableEqualsCriterion(
                MethodCallExpression expression)
            =>
            Restrictions.EqProperty(FindProjection(expression.Object),
                FindProjection(expression.Arguments[0]));

        #endregion

        #region # ProcessStringEqualsCriterion #

        private static ICriterion ProcessStringEqualsCriterion(
            MethodCallExpression expression)
        {
            IProjection leftProjection;
            IProjection rightProjection;
            StringComparison? comparison = null;
            if (expression.Arguments.Count == 2)
            {
                // Probably "".Equals("", comparison)
                leftProjection = FindProjection(expression.Object);
                if (leftProjection != null)
                {
                    rightProjection = FindProjection(expression.Arguments[0]);
                    comparison =
                        (StringComparison?)
                        ExpressionProcessor.FindValue(expression.Arguments[1]);

                    // Done
                    return Helper.IsCaseInvariant(comparison)
                        ? Restrictions.EqProperty(
                            Projections.SqlFunction("LOWER", NHibernateUtil.String,
                                leftProjection),
                            Projections.SqlFunction("LOWER", NHibernateUtil.String,
                                rightProjection))
                        : Restrictions.EqProperty(leftProjection, rightProjection);
                }
            }

            // Probably string.Equals("", "", [comparison])
            leftProjection = FindProjection(expression.Arguments[0]);
            rightProjection = FindProjection(expression.Arguments[1]);

            if (expression.Arguments.Count > 2)
            {
                comparison =
                    (StringComparison?)
                    ExpressionProcessor.FindValue(expression.Arguments[2]);
            }

            // Done
            return Helper.IsCaseInvariant(comparison)
                ? Restrictions.EqProperty(
                    Projections.SqlFunction("LOWER", NHibernateUtil.String,
                        leftProjection),
                    Projections.SqlFunction("LOWER", NHibernateUtil.String,
                        rightProjection))
                : Restrictions.EqProperty(leftProjection, rightProjection);
        }

        #endregion

    }
}