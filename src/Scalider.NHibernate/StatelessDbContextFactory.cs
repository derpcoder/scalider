﻿#region # using statements #

using System;
using JetBrains.Annotations;
using NHibernate;
using NHibernate.Cfg;

#endregion

namespace Scalider.NHibernate
{

    /// <summary>
    /// Represents an <see cref="IDbContextFactory"/> that provides
    /// <see cref="IStatelessDbContext"/>.
    /// </summary>
    public class StatelessDbContextFactory : DbContextFactory
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="StatelessDbContextFactory"/> class.
        /// </summary>
        /// <param name="callback"></param>
        public StatelessDbContextFactory([NotNull] Action<Configuration> callback)
            : base(callback)
        {
        }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="StatelessDbContextFactory"/> class.
        /// </summary>
        /// <param name="sessionFactory"></param>
        /// <param name="ownsSessionFactory">A value indicating whether the
        /// <paramref name="sessionFactory"/> should be disposed when this
        /// database context is disposed.</param>
        public StatelessDbContextFactory([NotNull] ISessionFactory sessionFactory,
            bool ownsSessionFactory = true)
            : base(sessionFactory, ownsSessionFactory)
        {
        }

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Creates a new <see cref="IDbContext"/> instance.
        /// </summary>
        /// <returns>
        /// The created <see cref="IDbContext"/> object.
        /// </returns>
        public override IDbContext Create()
        {
            ThrowIfDisposed();
            return new StatelessDbContext(SessionFactory.OpenStatelessSession());
        }

        #endregion

        #endregion
    }
}