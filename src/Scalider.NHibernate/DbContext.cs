﻿#region # using statements #

using System;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using JetBrains.Annotations;
using NHibernate;
using NHibernate.Linq;

#endregion

namespace Scalider.NHibernate
{
    
    /// <summary>
    /// Represents the default implementation of the <see cref="IDbContext"/>.
    /// </summary>
    public class DbContext : Disposable, IDbContext
    {
        #region # Variables #

        private readonly ISession _session;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="DbContext"/> class.
        /// </summary>
        /// <param name="session"></param>
        public DbContext([NotNull] ISession session)
        {
            Check.NotNull(session, nameof(session));
            _session = session;
        }

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">true to release both managed and unmanaged
        /// resources; false to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (!disposing)
                return;

            _session?.Dispose();
        }

        #endregion

        #endregion

        #region # IDbContext #

        /// <summary>
        /// Gets the active database connection for this database context.
        /// </summary>
        public virtual IDbConnection Connection
        {
            get
            {
                ThrowIfDisposed();
                return _session.Connection;
            }
        }

        /// <summary>
        /// Return the persistent instance of the given entity class with the
        /// given identifier, assuming that the instance exists.
        /// </summary>
        /// <typeparam name="T">The type encapsulating the entity to be loaded.</typeparam>
        /// <param name="id">A valid identifier of an existing persistent
        /// instance of the class.</param>
        /// <returns>
        /// The persistent instance or proxy.
        /// </returns>
        public virtual T Load<T>(object id)
            where T : class
        {
            ThrowIfDisposed();
            return _session.Load<T>(id);
        }

        /// <summary>
        /// Creates an <see cref="IQueryable{T}"/> object for the given entity
        /// type.
        /// </summary>
        /// <typeparam name="T">The type encapsulating the entity.</typeparam>
        /// <returns>
        /// An <see cref="IQueryable"/> object.
        /// </returns>
        public virtual IQueryable<T> Query<T>()
            where T : class
        {
            ThrowIfDisposed();
            return _session.Query<T>();
        }

        /// <summary>
        /// Creates a new <see cref="IQueryOver{TRoot}"/> for the entity class.
        /// </summary>
        /// <typeparam name="T">The type encapsulating the entity.</typeparam>
        /// <returns>
        /// An <see cref="IQueryOver{TRoot}"/> object.
        /// </returns>
        public virtual IQueryOver<T, T> QueryOver<T>()
            where T : class
        {
            ThrowIfDisposed();
            return _session.QueryOver<T>();
        }

        /// <summary>
        /// Creates a new <see cref="IQueryOver{TRoot}"/> for the entity class.
        /// </summary>
        /// <typeparam name="T">The type encapsulating the entity.</typeparam>
        /// <param name="alias">The alias of the entity.</param>
        /// <returns>
        /// An <see cref="IQueryOver{TRoot}"/> object.
        /// </returns>
        public virtual IQueryOver<T, T> QueryOver<T>(Expression<Func<T>> alias)
            where T : class
        {
            ThrowIfDisposed();
            Check.NotNull(alias, nameof(alias));

            return _session.QueryOver(alias);
        }

        /// <summary>
        /// Persist the given transient instance, first assigning a generated
        /// identifier.
        /// </summary>
        /// <typeparam name="T">The type encapsulating the entity.</typeparam>
        /// <param name="entity">A transient instance of a persistent class.</param>
        public virtual void Insert<T>(T entity)
            where T : class
        {
            ThrowIfDisposed();
            Check.NotNull(entity, nameof(entity));

            _session.Save(entity);
        }

        /// <summary>
        /// Update the persistent instance with the identifier of the given
        /// transient instance.
        /// </summary>
        /// <typeparam name="T">The type encapsulating the entity.</typeparam>
        /// <param name="entity">A transient instance containing updated state.</param>
        public virtual void Update<T>(T entity)
            where T : class
        {
            ThrowIfDisposed();
            Check.NotNull(entity, nameof(entity));

            _session.Update(entity);
        }

        /// <summary>
        /// Remove a persistent instance from the datastore.
        /// </summary>
        /// <typeparam name="T">The type encapsulating the entity.</typeparam>
        /// <param name="entity">The instance to be removed.</param>
        public virtual void Delete<T>(T entity)
            where T : class
        {
            ThrowIfDisposed();
            Check.NotNull(entity, nameof(entity));

            _session.Delete(entity);
        }

        /// <summary>
        /// Begins a transaction on the underlying store connection using the
        /// default <see cref="IsolationLevel"/>.
        /// </summary>
        /// <returns>
        /// A transaction object wrapping access to the underlying store's
        /// transaction object.
        /// </returns>
        public virtual ITransaction BeginTransaction()
        {
            ThrowIfDisposed();
            return _session.BeginTransaction();
        }

        /// <summary>
        /// Begins a transaction on the underlying store connection using the
        /// specified isolation level.
        /// </summary>
        /// <param name="isolationLevel">The database isolation level with
        /// which the underlying store transaction will be created.</param>
        /// <returns>
        /// A transaction object wrapping access to the underlying store's
        /// transaction object.
        /// </returns>
        public virtual ITransaction BeginTransaction(IsolationLevel isolationLevel)
        {
            ThrowIfDisposed();
            return _session.BeginTransaction(isolationLevel);
        }

        /// <summary>
        /// Force flushes the changes made to the database context.
        /// </summary>
        public virtual void Flush()
        {
            ThrowIfDisposed();
            _session.Flush();
        }

        /// <summary>
        /// Clears the database context, evicts all the loaded entities and
        /// cancel any pending insert, update or delete operation.
        /// </summary>
        public void Clear()
        {
            ThrowIfDisposed();
            _session.Clear();
        }

        #endregion
    }
}