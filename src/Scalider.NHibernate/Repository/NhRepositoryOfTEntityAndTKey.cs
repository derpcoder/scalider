﻿#region # using statements #

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using JetBrains.Annotations;
using Scalider.Domain.Entities;
using Scalider.Domain.Repository;
using Scalider.NHibernate.UnitOfWork;

#endregion

namespace Scalider.NHibernate.Repository
{

    /// <summary>
    /// Represents an <see cref="IRepository{TEntity,TKey}"/> that uses
    /// NHibernate as underlaying layer for reading and writing the backing
    /// storage.
    /// </summary>
    /// <typeparam name="TEntity">The type encapsulating the type of the
    /// entity.</typeparam>
    /// <typeparam name="TKey">The type encapsulating the primary key of the
    /// entity.</typeparam>
    public class NhRepository<TEntity, TKey> : RepositoryBase<TEntity, TKey>
        where TEntity : class, IEntity<TKey>
        where TKey : IEquatable<TKey>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="NhRepository{TEntity,TKey}"/> class.
        /// </summary>
        /// <param name="unitOfWork"></param>
        public NhRepository([NotNull] NhUnitOfWork unitOfWork)
        {
            Check.NotNull(unitOfWork, nameof(unitOfWork));
            UnitOfWork = unitOfWork;
        }

        #region # Properties #

        #region == Public ==

        public NhUnitOfWork UnitOfWork { get; }

        #endregion

        #endregion

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Gets the count of all entities in this repository.
        /// </summary>
        /// <returns>
        /// The number of entities in this repository.
        /// </returns>
        public override int Count() => GetDbContext().QueryOver<TEntity>().RowCount();

        /// <summary>
        /// Gets the count of all entities in this repository.
        /// </summary>
        /// <returns>
        /// The number of entities in this repository.
        /// </returns>
        /// <remarks>
        /// Use this method when the return value is expected to be greater
        /// than <see cref="int.MaxValue"/>.
        /// </remarks>
        public override long LongCount()
            => GetDbContext().QueryOver<TEntity>().RowCountInt64();

        /// <summary>
        /// Gets the count of all entities in this repository.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The number of entities in this repository.
        /// </returns>
        public override int Count(Expression<Func<TEntity, bool>> predicate)
        {
            Check.NotNull(predicate, nameof(predicate));
            return GetDbContext().QueryOver<TEntity>().Where(predicate).RowCount();
        }

        /// <summary>
        /// Gets the count of all entities in this repository.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The number of entities in this repository.
        /// </returns>
        /// <remarks>
        /// Use this method when the return value is expected to be greater
        /// than <see cref="int.MaxValue"/>.
        /// </remarks>
        public override long LongCount(Expression<Func<TEntity, bool>> predicate)
        {
            Check.NotNull(predicate, nameof(predicate));
            return
                GetDbContext().QueryOver<TEntity>().Where(predicate).RowCountInt64();
        }

        /// <summary>
        /// Returns all the entities for this repository.
        /// </summary>
        /// <returns>
        /// A collection containing all the entities for this repository.
        /// </returns>
        public override IEnumerable<TEntity> GetAll()
            => GetDbContext().QueryOver<TEntity>().List();

        /// <summary>
        /// Returns collection containing all the entities that satisfies a
        /// condition.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The collection containing all the entities that satisfies the
        /// condition.
        /// </returns>
        public override IEnumerable<TEntity> Find(
            Expression<Func<TEntity, bool>> predicate)
        {
            Check.NotNull(predicate, nameof(predicate));
            return GetDbContext().QueryOver<TEntity>().Where(predicate).List();
        }

        /// <summary>
        /// Returns the only entity that satisfies a condition or throws an
        /// exception if there is more than exactly one entity that satisfies
        /// the condition.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The single entity that satisfies the condition.
        /// </returns>
        public override TEntity Single(Expression<Func<TEntity, bool>> predicate)
        {
            Check.NotNull(predicate, nameof(predicate));
            return
                GetDbContext().QueryOver<TEntity>().Where(predicate).List().Single();
        }

        /// <summary>
        /// Returns the first entity that satisfies a condition or <c>null</c>
        /// if no such entity is found.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The entity that satisfies the condition or <c>null</c> if no such
        /// entity is found.
        /// </returns>
        public override TEntity FirstOrDefault(
            Expression<Func<TEntity, bool>> predicate)
        {
            Check.NotNull(predicate, nameof(predicate));
            return
                GetDbContext()
                    .QueryOver<TEntity>()
                    .Where(predicate)
                    .SingleOrDefault();
        }

        /// <summary>
        /// Inserts a new entity.
        /// </summary>
        /// <param name="entity">The entity to insert.</param>
        public override void Insert(TEntity entity)
        {
            Check.NotNull(entity, nameof(entity));
            GetDbContext().Insert(entity);
        }

        /// <summary>
        /// Updates an existing identity.
        /// </summary>
        /// <param name="entity">The entity to update.</param>
        public override void Update(TEntity entity)
        {
            Check.NotNull(entity, nameof(entity));
            GetDbContext().Update(entity);
        }

        /// <summary>
        /// Removes an entity from the datastore.
        /// </summary>
        /// <param name="entity">The entity to remove.</param>
        public override void Delete(TEntity entity)
        {
            Check.NotNull(entity, nameof(entity));
            if (typeof(TEntity).Is<ISoftDelete>())
            {
                // ReSharper disable once SuspiciousTypeConversion.Global
                ((ISoftDelete)entity).IsDeleted = true;
                GetDbContext().Update(entity);
            }
            else
                GetDbContext().Delete(entity);
        }

        /// <summary>
        /// Removes the entity with the given primary key.
        /// </summary>
        /// <param name="id">The primary key of the entity to remove.</param>
        public override void Delete(TKey id)
        {
            var entity = GetDbContext().Load<TEntity>(id);
            if (entity != null)
                Delete(entity);
        }

        #endregion

        #region == Protected ==

        /// <summary>
        /// Gets the <see cref="IDbContext"/> to operate against.
        /// </summary>
        protected virtual IDbContext GetDbContext() => UnitOfWork.GetDbContext();

        #endregion

        #endregion

        static NhRepository()
        {
            ExpressionProcessorRegistry.RegisterProcessorsIfNotRegistered();
        }

    }
}