﻿#region # using statements #

using System;

#endregion

namespace Scalider.NHibernate
{

    /// <summary>
    /// Represents a type used to configure the database system and create
    /// instances of <see cref="IDbContext"/>.
    /// </summary>
    public interface IDbContextFactory : IDisposable
    {

        /// <summary>
        /// Creates a new <see cref="IDbContext"/> instance.
        /// </summary>
        /// <returns>
        /// The created <see cref="IDbContext"/> object.
        /// </returns>
        IDbContext Create();

    }
}