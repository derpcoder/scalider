﻿#region # using statements #

using System;

#endregion

namespace Scalider.NHibernate
{

    /// <summary>
    /// Provides some internal helpers.
    /// </summary>
    internal static class Helper
    {

        /// <summary>
        /// Determines whether the given <see cref="StringComparison"/>
        /// represents a case invariant comparison.
        /// </summary>
        /// <param name="comparison">The <see cref="StringComparison"/>.</param>
        /// <returns>
        /// <c>true</c> if the given <paramref name="comparison"/> represents a
        /// case invariant comparison; otherwise, false.
        /// </returns>
        public static bool IsCaseInvariant(StringComparison? comparison)
        {
            if (!comparison.HasValue)
                return false;

            var value = comparison.Value;
            return value == StringComparison.CurrentCultureIgnoreCase ||
                   value == StringComparison.InvariantCultureIgnoreCase ||
                   value == StringComparison.OrdinalIgnoreCase;
        }

    }
}