﻿#region # using statements #

using System.Collections.Generic;

#endregion

namespace Scalider.Collections
{

    /// <summary>
    /// Extension methods for <see cref="ICollection{T}"/>.
    /// </summary>
    public static class CollectionExtensions
    {

        /// <summary>
        /// Determines whether the given <paramref name="source"/> is
        /// <c>null</c> or doesn't have any element.
        /// </summary>
        /// <typeparam name="T">The type encapsulating the collection elements.</typeparam>
        /// <param name="source">The collection to validate.</param>
        /// <returns>
        /// true if the given <paramref name="source"/> is <c>null</c> or
        /// doesn't have any element; otherwise, false.
        /// </returns>
        public static bool IsNullOrEmpty<T>(this ICollection<T> source)
            => source == null || source.Count == 0;

        /// <summary>
        /// Adds an item to the collection if it's not already in the
        /// collection.
        /// </summary>
        /// <typeparam name="T">The type encapsulating the collection elements.</typeparam>
        /// <param name="source">The collection to add item to.</param>
        /// <param name="item">The item to add to the collection.</param>
        /// <returns>
        /// true if the given <paramref name="item"/> is added to
        /// <paramref name="source"/>; otherwise, false.
        /// </returns>
        public static bool AddIfNotCotains<T>(this ICollection<T> source, T item)
        {
            Check.NotNull(source, nameof(source));

            if (source.Contains(item))
                return false;

            source.Add(item);
            return true;
        }

    }
}