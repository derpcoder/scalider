﻿#region # using statements #

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;

#endregion

namespace Scalider.Collections.Concurrent
{

    /// <summary>
    /// Represents a strongly typed thread-safe random-access list of objects
    /// that can be accessed by index.
    /// </summary>
    /// <typeparam name="T">The type of elements in the list.</typeparam>
    [DebuggerTypeProxy(typeof(CollectionDebugView<>)),
     DebuggerDisplay("Count = {Count}")]
    public class CopyOnWriteCollection<T> : IList, IList<T>
    {
        #region # Variables #

        private readonly object _sync = new object();
        private T[] _elements;

        #endregion

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="CopyOnWriteCollection{T}"/> class.
        /// </summary>
        public CopyOnWriteCollection()
        {
            _elements = Array.Empty<T>();
        }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="CopyOnWriteCollection{T}"/> class.
        /// </summary>
        /// <param name="collection">The collection whose elements are copied
        /// to the new list.</param>
        public CopyOnWriteCollection(IEnumerable<T> collection)
        {
            Check.NotNull(collection, nameof(collection));

            var arr = collection as T[] ?? collection.ToArray();
            var size = arr.Length;
            _elements = new T[size];
            ArrayCopy(arr, 0, _elements, 0, size);
        }

        #region # Methods #

        #region == Public ==

        /// <summary>
        /// Adds the elements of the specified collection to the end of the
        /// <see cref="CopyOnWriteCollection{T}" />.</summary>
        /// <param name="collection">The collection whose elements should be
        /// added to the end of the <see cref="CopyOnWriteCollection{T}" />.
        /// The collection itself cannot be null, but it can contain elements
        /// that are null, if type T is a reference type.</param>
        public void AddRange(IEnumerable<T> collection)
        {
            Check.NotNull(collection, nameof(collection));

            var locked = false;
            try
            {
                Monitor.Enter(_sync, ref locked);
                var toAdd = collection.ToArray();
                var size = _elements.Length;
                var sizeToAdd = toAdd.Length;

                var newElements = new T[size + sizeToAdd];
                ArrayCopy(_elements, 0, newElements, 0, size);
                ArrayCopy(toAdd, 0, newElements, size, sizeToAdd);

                _elements = newElements;
            }
            finally
            {
                if (locked)
                    Monitor.Exit(_sync);
            }
        }

        /// <summary>
        /// Inserts the elements of a collection into the
        /// <see cref="CopyOnWriteCollection{T}" /> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which the new elements
        /// should be inserted.</param>
        /// <param name="collection">The collection whose elements should be
        /// inserted into the <see cref="CopyOnWriteCollection{T}" />. The
        /// collection itself cannot be null, but it can contain elements that
        /// are null, if type T is a reference type.</param>
        public void InserRange(int index, IEnumerable<T> collection)
        {
            Check.NotNull(collection, nameof(collection));

            var locked = false;
            try
            {
                Monitor.Enter(_sync, ref locked);
                var toAdd = collection as T[] ?? collection.ToArray();

                var size = _elements.Length;
                var toAddSize = toAdd.Length;
                var newElements = new T[size + toAddSize];
                ArrayCopy(_elements, 0, newElements, 0, index);
                ArrayCopy(toAdd, 0, newElements, index, toAddSize);
                ArrayCopy(_elements, index, newElements, index + toAddSize,
                    size - index);
                _elements = newElements;
            }
            finally
            {
                if (locked)
                    Monitor.Exit(_sync);
            }
        }

        /// <summary>
        /// Removes all the elements that match the conditions defined by the
        /// specified predicate.
        /// </summary>
        /// <param name="match">The <see cref="Predicate{T}" /> delegate that
        /// defines the conditions of the elements to remove.</param>
        /// <returns>
        /// The number of elements removed from the
        /// <see cref="CopyOnWriteCollection{T}" />.
        /// </returns>
        public int RemoveAll(Predicate<T> match)
        {
            var locked = false;
            try
            {
                Monitor.Enter(_sync, ref locked);
                var matches = Array.FindAll(_elements, match);
                var matchesSize = matches.Length;
                if (matchesSize == 0)
                    return 0;
                
                var newElements = Array.FindAll(_elements, i => !match(i));
                _elements = newElements;

                return matchesSize;
            }
            finally
            {
                if (locked)
                    Monitor.Exit(_sync);
            }
        }

        /// <summary>
        /// Copies the elements of the <see cref="CopyOnWriteCollection{T}" />
        /// to a new array.
        /// </summary>
        /// <returns>
        /// An array containing copies of the elements of the
        /// <see cref="CopyOnWriteCollection{T}" />.
        /// </returns>
        public T[] ToArray()
        {
            var size = _elements.Length;
            var elements = new T[size];
            ArrayCopy(_elements, 0, elements, 0, size);

            return elements;
        }

        /// <summary>
        /// Searches the entire sorted <see cref="CopyOnWriteCollection{T}" />
        /// for an element using the default comparer and returns the
        /// zero-based index of the element.
        /// </summary>
        /// <param name="item">The object to locate. The value can be null for
        /// reference types.</param>
        /// <returns>
        /// The zero-based index of <paramref name="item" /> in the sorted
        /// <see cref="CopyOnWriteCollection{T}" />, if
        /// <paramref name="item" /> is found; otherwise, a negative number
        /// that is the bitwise complement of the index of the next element
        /// that is larger than <paramref name="item" /> or, if there is no
        /// larger element, the bitwise complement of <see cref="Count" />.
        /// </returns>
        public int BinarySearch(T item)
        {
            return BinarySearch(0, Count, item, null);
        }

        /// <summary>
        /// Searches the entire sorted <see cref="CopyOnWriteCollection{T}" />
        /// for an element using the specified comparer and returns the
        /// zero-based index of the element.
        /// </summary>
        /// <param name="item">The object to locate. The value can be null for
        /// reference types.</param>
        /// <param name="comparer">The <see cref="Comparer{T}" />
        /// implementation to use when comparing elements.-or-null to use the
        /// default comparer <see cref="Comparer{T}.Default" />.</param>
        /// <returns>
        /// The zero-based index of <paramref name="item" /> in the sorted
        /// <see cref="CopyOnWriteCollection{T}" />, if
        /// <paramref name="item" /> is found; otherwise, a negative number
        /// that is the bitwise complement of the index of the next element
        /// that is larger than <paramref name="item" /> or, if there is no
        /// larger element, the bitwise complement of <see cref="Count" />.
        /// </returns>
        public int BinarySearch(T item, IComparer<T> comparer)
        {
            return BinarySearch(0, Count, item, comparer);
        }

        /// <summary>
        /// Searches a range of elements in the sorted
        /// <see cref="CopyOnWriteCollection{T}" /> for an element using the
        /// specified comparer and returns the zero-based index of the element.
        /// </summary>
        /// <param name="index">The zero-based starting index of the range to
        /// search.</param>
        /// <param name="count">The length of the range to search.</param>
        /// <param name="item">The object to locate. The value can be null for
        /// reference types.</param>
        /// <param name="comparer">The <see cref="IComparer{T}" />
        /// implementation to use when comparing elements, or null to use the
        /// default comparer <see cref="Comparer{T}.Default" />.</param>
        /// <returns>
        /// The zero-based index of <paramref name="item" /> in the sorted
        /// <see cref="CopyOnWriteCollection{T}" />, if
        /// <paramref name="item" /> is found; otherwise, a negative number
        /// that is the bitwise complement of the index of the next element
        /// that is larger than <paramref name="item" /> or, if there is no
        /// larger element, the bitwise complement of <see cref="Count" />.
        /// </returns>
        public int BinarySearch(int index, int count, T item, IComparer<T> comparer)
        {
            if (index < 0)
                throw new ArgumentOutOfRangeException(nameof(index));
            if (count < 0)
                throw new ArgumentOutOfRangeException(nameof(count));
            if (Count - index < count)
                throw new ArgumentException("The length of the offset is invalid");

            return Array.BinarySearch(_elements, index, count, item, comparer);
        }

        /// <summary>
        /// Searches for the specified object and returns the zero-based index
        /// of the first occurrence within the range of elements in the
        /// <see cref="CopyOnWriteCollection{T}" /> that extends from the
        /// specified index to the last element.
        /// </summary>
        /// <param name="item">The object to locate in the
        /// <see cref="CopyOnWriteCollection{T}" />. The value can be null for
        /// reference types.</param>
        /// <param name="index">The zero-based starting index of the search. 0
        /// (zero) is valid in an empty list.</param>
        /// <returns>
        /// The zero-based index of the first occurrence of
        /// <paramref name="item" /> within the range of elements in the
        /// <see cref="CopyOnWriteCollection{T}" /> that extends from
        /// <paramref name="index" /> to the last element, if found; otherwise,
        /// –1.
        /// </returns>
        public int IndexOf(T item, int index)
        {
            if (index > Count)
                throw new ArgumentOutOfRangeException(nameof(index));

            return IndexOf(item, index, Count - index);
        }

        /// <summary>
        /// Searches for the specified object and returns the zero-based index
        /// of the first occurrence within the range of elements in the
        /// <see cref="CopyOnWriteCollection{T}" /> that starts at the
        /// specified index and contains the specified number of elements.
        /// </summary>
        /// <param name="item">The object to locate in the
        /// <see cref="CopyOnWriteCollection{T}" />. The value can be null for
        /// reference types.</param>
        /// <param name="index">The zero-based starting index of the search. 0
        /// (zero) is valid in an empty list.</param>
        /// <param name="count">The number of elements in the section to
        /// search.</param>
        /// <returns>
        /// The zero-based index of the first occurrence of
        /// <paramref name="item" /> within the range of elements in the
        /// <see cref="CopyOnWriteCollection{T}" /> that starts at
        /// <paramref name="index" /> and contains <paramref name="count" />
        /// number of elements, if found; otherwise, –1.
        /// </returns>
        public int IndexOf(T item, int index, int count)
        {
            if (index > Count)
                throw new ArgumentOutOfRangeException(nameof(index));
            if (count < 0 || index > Count - count)
                throw new ArgumentOutOfRangeException(nameof(count));

            return Array.IndexOf(_elements, item, index, count);
        }

        /// <summary>
        /// Searches for the specified object and returns the zero-based index
        /// of the last occurrence within the entire
        /// <see cref="CopyOnWriteCollection{T}" />.
        /// </summary>
        /// <param name="item">The object to locate in the
        /// <see cref="CopyOnWriteCollection{T}" />. The value can be null for
        /// reference types.</param>
        /// <returns>
        /// The zero-based index of the last occurrence of
        /// <paramref name="item" /> within the entire the
        /// <see cref="CopyOnWriteCollection{T}" />, if found; otherwise, –1.
        /// </returns>
        public int LastIndexOf(T item)
        {
            return Count == 0 ? -1 : LastIndexOf(item, Count - 1, Count);
        }

        /// <summary>
        /// Searches for the specified object and returns the zero-based index
        /// of the last occurrence within the range of elements in the
        /// <see cref="CopyOnWriteCollection{T}" /> that extends from the first
        /// element to the specified index.
        /// </summary>
        /// <param name="item">The object to locate in the
        /// <see cref="CopyOnWriteCollection{T}" />. The value can be null for
        /// reference types.</param>
        /// <param name="index">The zero-based starting index of the backward search.</param>
        /// <returns>
        /// The zero-based index of the last occurrence of
        /// <paramref name="item" /> within the range of elements in the
        /// <see cref="CopyOnWriteCollection{T}" /> that extends from the first
        /// element to <paramref name="index" />, if found; otherwise, –1.
        /// </returns>
        public int LastIndexOf(T item, int index)
        {
            if (index > Count)
                throw new ArgumentOutOfRangeException(nameof(index));

            return LastIndexOf(item, index, index + 1);
        }

        /// <summary>
        /// Searches for the specified object and returns the zero-based index
        /// of the last occurrence within the range of elements in the
        /// <see cref="CopyOnWriteCollection{T}" /> that contains the specified
        /// number of elements and ends at the specified index.
        /// </summary>
        /// <param name="item">The object to locate in the
        /// <see cref="CopyOnWriteCollection{T}" />. The value can be null for
        /// reference types.</param>
        /// <param name="index">The zero-based starting index of the backward
        /// search.</param>
        /// <param name="count">The number of elements in the section to
        /// search.</param>
        /// <returns>
        /// The zero-based index of the last occurrence of
        /// <paramref name="item" /> within the range of elements in the
        /// <see cref="CopyOnWriteCollection{T}" /> that contains
        /// <paramref name="count" /> number of elements and ends at
        /// <paramref name="index" />, if found; otherwise, –1.
        /// </returns>
        public int LastIndexOf(T item, int index, int count)
        {
            if (Count != 0 && index < 0)
                throw new ArgumentOutOfRangeException(nameof(index));
            if (Count != 0 && count < 0)
                throw new ArgumentOutOfRangeException(nameof(count));
            if (Count == 0)
                return -1;
            if (index >= Count)
                throw new ArgumentOutOfRangeException(nameof(index));
            if (count > index + 1)
                throw new ArgumentOutOfRangeException(nameof(count));

            return Array.LastIndexOf(_elements, item, index, count);
        }

        #endregion

        #region == Private ==

        private static bool IsCompatibleObject(object value)
        {
            if (value is T)
                return true;

            return value == null && default(T) == null;
        }

        private static void ArrayCopy(Array sourceArray, int sourceIndex,
            Array destinationArray, int destinationIndex, int length)
        {
            var type = typeof(T).GetTypeInfo();
            if (type.IsPrimitive)
            {
                Buffer.BlockCopy(sourceArray, sourceIndex, destinationArray,
                    destinationIndex, length);
            }
            else
            {
                Array.Copy(sourceArray, sourceIndex, destinationArray,
                    destinationIndex, length);
            }
        }

        private void RemoveAtInternal(int index)
        {
            var size = _elements.Length;
            var newElements = new T[size - 1];
            if (index == 0 && newElements.Length > 0)
                ArrayCopy(_elements, 1, newElements, 0, size - 1);
            else if (index > 0)
            {
                ArrayCopy(_elements, 0, newElements, 0, index);
                ArrayCopy(_elements, index + 1, newElements, index, size - index - 1);
            }

            _elements = newElements;
        }

        #endregion

        #endregion

        #region # ICollection #

        /// <summary>
        /// Gets the number of elements contained in the
        /// <see cref="CopyOnWriteCollection{T}" />.
        /// </summary>
        /// <returns>
        /// The number of elements contained in the
        /// <see cref="CopyOnWriteCollection{T}" />.
        /// </returns>
        public int Count => _elements.Length;

        object ICollection.SyncRoot => _sync;

        bool ICollection.IsSynchronized => true;

        void ICollection.CopyTo(Array array, int arrayIndex)
        {
            try
            {
                ArrayCopy(_elements, 0, array, arrayIndex, _elements.Length);
            }
            catch (ArrayTypeMismatchException)
            {
            }
        }

        #endregion

        #region # ICollection<T> #

        bool ICollection<T>.IsReadOnly => false;

        /// <summary>
        /// Determines whether the <see cref="CopyOnWriteCollection{T}" />
        /// contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the
        /// <see cref="CopyOnWriteCollection{T}" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> is found in the
        /// <see cref="CopyOnWriteCollection{T}" />; otherwise, false.</returns>
        public bool Contains(T item)
        {
            return Array.IndexOf(_elements, item) != -1;
        }

        /// <summary>
        /// Copies the elements of the <see cref="CopyOnWriteCollection{T}" />
        /// to an <see cref="Array" />, starting at a particular
        /// <see cref="Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="Array" /> that
        /// is the destination of the elements copied from
        /// <see cref="CopyOnWriteCollection{T}" />. The <see cref="Array" /> 
        /// must have zero-based indexing.</param>
        /// <param name="arrayIndex">The zero-based index in
        /// <paramref name="array" /> at which copying begins.</param>
        public void CopyTo(T[] array, int arrayIndex)
        {
            ArrayCopy(_elements, 0, array, arrayIndex, _elements.Length);
        }

        /// <summary>
        /// Adds an item to the <see cref="CopyOnWriteCollection{T}" />.
        /// </summary>
        /// <param name="item">The object to add to the
        /// <see cref="CopyOnWriteCollection{T}" />.</param>
        public void Add(T item)
        {
            var locked = false;
            try
            {
                Monitor.Enter(_sync, ref locked);
                var size = _elements.Length;
                var newElements = new T[size + 1];
                ArrayCopy(_elements, 0, newElements, 0, size);
                newElements[size] = item;
                _elements = newElements;
            }
            finally
            {
                if (locked)
                    Monitor.Exit(_sync);
            }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the
        /// <see cref="CopyOnWriteCollection{T}" />.
        /// </summary>
        /// <param name="item">The object to remove from the
        /// <see cref="CopyOnWriteCollection{T}" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the
        /// <see cref="CopyOnWriteCollection{T}" />; otherwise, false. This
        /// method also returns false if <paramref name="item" /> is not found
        /// in the original <see cref="CopyOnWriteCollection{T}" />.
        /// </returns>
        public bool Remove(T item)
        {
            var locked = false;
            try
            {
                Monitor.Enter(_sync, ref locked);
                var size = _elements.Length;
                var oldElements = new T[size];
                ArrayCopy(_elements, 0, oldElements, 0, size);

                var index = Array.IndexOf(oldElements, item);
                if (index == -1)
                    return false;

                RemoveAtInternal(index);
                return true;
            }
            finally
            {
                if (locked)
                    Monitor.Exit(_sync);
            }
        }

        #endregion

        #region # IEnumerable #

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region # IEnumerable<T> #

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// An enumerator that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<T> GetEnumerator()
        {
            var size = _elements.Length;
            var newElements = new T[size];
            ArrayCopy(_elements, 0, newElements, 0, size);

            return newElements.Cast<T>().GetEnumerator();
        }

        #endregion

        #region # IList #

        bool IList.IsReadOnly => false;

        bool IList.IsFixedSize => false;

        object IList.this[int index]
        {
            get
            {
                return this[index];
            }
            set
            {
                Check.NullNotAllowed<T>(value, nameof(value));
                try
                {
                    this[index] = (T)value;
                }
                catch (InvalidCastException)
                {
                }
            }
        }

        /// <summary>
        /// Removes all items from the <see cref="CopyOnWriteCollection{T}" />.
        /// </summary>
        public void Clear()
        {
            var locked = false;
            try
            {
                Monitor.Enter(_sync, ref locked);
                _elements = new T[0];
            }
            finally
            {
                if (locked)
                    Monitor.Exit(_sync);
            }
        }

        /// <summary>
        /// Removes the <see cref="CopyOnWriteCollection{T}" /> item at the
        /// specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        public void RemoveAt(int index)
        {
            if (index < 0 || index > _elements.Length)
                throw new IndexOutOfRangeException();

            var locked = false;
            try
            {
                Monitor.Enter(_sync, ref locked);
                RemoveAtInternal(index);
            }
            finally
            {
                if (locked)
                    Monitor.Exit(_sync);
            }
        }

        int IList.Add(object value)
        {
            Check.NullNotAllowed<T>(value, nameof(value));
            try
            {
                Add((T)value);
            }
            catch (InvalidCastException)
            {
            }

            return _elements.Length - 1;
        }

        bool IList.Contains(object value)
        {
            return IsCompatibleObject(value) && Contains((T)value);
        }

        int IList.IndexOf(object value)
        {
            if (IsCompatibleObject(value))
                return IndexOf((T)value);

            return -1;
        }

        void IList.Insert(int index, object value)
        {
            Check.NullNotAllowed<T>(value, nameof(value));
            try
            {
                Insert(index, (T)value);
            }
            catch (InvalidCastException)
            {
            }
        }

        void IList.Remove(object value)
        {
            if (IsCompatibleObject(value))
                Remove((T)value);
        }

        #endregion

        #region # IList<T> #

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to get or
        /// set.</param>
        public T this[int index]
        {
            get
            {
                return _elements[index];
            }
            set
            {
                var size = _elements.Length;
                var newElements = new T[size];
                ArrayCopy(_elements, 0, newElements, 0, size);
                newElements[index] = value;
                _elements = newElements;
            }
        }

        /// <summary>
        /// Determines the index of a specific item in the
        /// <see cref="CopyOnWriteCollection{T}" />.
        /// </summary>
        /// <param name="item">The object to locate in the
        /// <see cref="CopyOnWriteCollection{T}" />.</param>
        /// <returns>
        /// The index of <paramref name="item" /> if found in the list;
        /// otherwise, -1.
        /// </returns>
        public int IndexOf(T item)
        {
            return IndexOf(item, 0, Count);
        }

        /// <summary>
        /// Inserts an item to the <see cref="CopyOnWriteCollection{T}" /> at
        /// the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which
        /// <paramref name="item" /> should be inserted.</param>
        /// <param name="item">The object to insert into the
        /// <see cref="CopyOnWriteCollection{T}" />.</param>
        public void Insert(int index, T item)
        {
            if (index < 0 || index > _elements.Length)
                throw new IndexOutOfRangeException();

            var locked = false;
            try
            {
                Monitor.Enter(_sync, ref locked);
                var size = _elements.Length;
                var newElements = new T[size + 1];
                ArrayCopy(_elements, 0, newElements, 0, index);
                newElements[index] = item;
                ArrayCopy(_elements, index, newElements, index + 1, size - index);
                _elements = newElements;
            }
            finally
            {
                if (locked)
                    Monitor.Exit(_sync);
            }
        }

        #endregion
    }
}