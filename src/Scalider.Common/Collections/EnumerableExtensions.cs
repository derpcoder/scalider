﻿#region # using statements #

using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace Scalider.Collections
{

    /// <summary>
    /// Extension methods for <see cref="IEnumerable{T}"/>.
    /// </summary>
    public static class EnumerableExtensions
    {

        /// <summary>
        /// Applies the given <paramref name="callback"/> to each element in
        /// <paramref name="collection"/>.
        /// </summary>
        /// <typeparam name="T">The type of objects to enumerate.</typeparam>
        /// <param name="collection">The enumeration.</param>
        /// <param name="callback">The action to apply to each item in the
        /// enumeration.</param>
        public static void Apply<T>(this IEnumerable<T> collection,
            Action<T> callback)
        {
            Check.NotNull(collection, nameof(collection));
            Check.NotNull(callback, nameof(callback));

            foreach (var item in collection)
                callback.Invoke(item);
        }

        /// <summary>
        /// Determines whether the specified element is contained on the given
        /// sequence by using the default equality comparer.
        /// </summary>
        /// <typeparam name="T">The type of the element.</typeparam>
        /// <param name="value">The value to locate in the sequence.</param>
        /// <param name="source">A sequence in which to locate a value.</param>
        /// <returns>
        /// true if the source sequence contains an element that has the
        /// specified value; otherwise, false.
        /// </returns>
        public static bool In<T>(this T value, params T[] source)
            => !Equals(value, null) && value.In(source, EqualityComparer<T>.Default);

        /// <summary>
        /// Determines whether the specified element is contained on the given
        /// sequence by using the default equality comparer.
        /// </summary>
        /// <typeparam name="T">The type of the element.</typeparam>
        /// <param name="value">The value to locate in the sequence.</param>
        /// <param name="source">A sequence in which to locate a value.</param>
        /// <returns>
        /// true if the source sequence contains an element that has the
        /// specified value; otherwise, false.
        /// </returns>
        public static bool In<T>(T value, IEnumerable<T> source)
            => !Equals(value, null) && value.In(source, EqualityComparer<T>.Default);

        /// <summary>
        /// Determines whether the specified element is contained on the given
        /// sequence by using a specified <see cref="IEqualityComparer{T}"/>.
        /// </summary>
        /// <typeparam name="T">The type of the element.</typeparam>
        /// <param name="value">The value to locate in the sequence.</param>
        /// <param name="source">A sequence in which to locate a value.</param>
        /// <param name="comparer">An equality comparer to compare values.</param>
        /// <returns>
        /// true if the source sequence contains an element that has the
        /// specified value; otherwise, false.
        /// </returns>
        public static bool In<T>(this T value, IEnumerable<T> source,
            IEqualityComparer<T> comparer)
        {
            Check.NotNull(source, nameof(source));
            return !Equals(value, null) &&
                   source.Contains(value, comparer ?? EqualityComparer<T>.Default);
        }

    }
}