﻿#region # using statements #

using System.Collections.Generic;
using System.Diagnostics;

#endregion

namespace Scalider.Collections
{
    internal class CollectionDebugView<T>
    {
        #region # Variables #

        private readonly ICollection<T> _collection;

        #endregion
        
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="CollectionDebugView{T}"/> class.
        /// </summary>
        /// <param name="collection">The collection to show debug information
        /// for.</param>
        public CollectionDebugView(ICollection<T> collection)
        {
            Check.NotNull(collection, nameof(collection));
            _collection = collection;
        }

        #region # Properties #

        #region == Public ==

        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public T[] Items
        {
            get
            {
                var array = new T[_collection.Count];
                _collection.CopyTo(array, 0);
                return array;
            }
        }

        #endregion

        #endregion
    }
}