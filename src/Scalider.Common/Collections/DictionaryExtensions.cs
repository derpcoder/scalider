﻿#region # using statements #

using System.Collections.Generic;
using System.Linq;

#endregion

namespace Scalider.Collections
{

    /// <summary>
    /// Extension methods for dictionaries.
    /// </summary>
    public static class DictionaryExtensions
    {

        /// <summary>
        /// Adds the elements of the specified <paramref name="collection"/> to
        /// the end of the <see cref="IDictionary{TKey,TValue}"/>.
        /// </summary>
        /// <typeparam name="TKey">The type of keys in the dictionary.</typeparam>
        /// <typeparam name="TValue">The type of values in the dictionary.</typeparam>
        /// <param name="source">The source dictionary.</param>
        /// <param name="collection">The collection whose elements should be
        /// added to the end of the <see cref="IDictionary{TKey,TValue}"/>.</param>
        public static void AddRange<TKey, TValue>(
            this IDictionary<TKey, TValue> source,
            IEnumerable<KeyValuePair<TKey, TValue>> collection)
        {
            Check.NotNull(source, nameof(source));
            foreach (
                var kv in collection.Where(kv => !source.ContainsKey(kv.Key)))
                source.Add(kv);
        }

    }
}