﻿#region # using statements #

using JetBrains.Annotations;

#endregion

namespace Scalider.Security
{

    /// <summary>
    /// Provides a base implementation for the
    /// <see cref="IEncryptionService"/>.
    /// </summary>
    public abstract class BaseEncryptionService : IEncryptionService
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="BaseEncryptionService"/> class.
        /// </summary>
        protected BaseEncryptionService()
        {
        }

        #region # Methods #

        #region == Protected ==

        /// <summary>
        /// Process the decryption of the given byte array.
        /// </summary>
        /// <param name="cipherData">The byte array to decrypt.</param>
        /// <returns>
        /// The decrypted byte array.
        /// </returns>
        protected abstract byte[] DecryptCore([NotNull] byte[] cipherData);

        /// <summary>
        /// Process the encryption of the given byte array.
        /// </summary>
        /// <param name="clearData">The byte array to encrypt.</param>
        /// <returns>
        /// The encrypted byte array.
        /// </returns>
        protected abstract byte[] EncryptCore([NotNull] byte[] clearData);

        #endregion

        #endregion

        #region # IEncryptionService #

        /// <summary>
        /// Decrypts the given byte array.
        /// </summary>
        /// <param name="cipherData">The byte array to decrypt.</param>
        /// <returns>
        /// The decrypted byte array.
        /// </returns>
        public byte[] Decrypt(byte[] cipherData)
        {
            Check.NotNull(cipherData, nameof(cipherData));
            return DecryptCore(cipherData);
        }

        /// <summary>
        /// Encrypts the given byte array.
        /// </summary>
        /// <param name="clearData">The byte array to encrypt.</param>
        /// <returns>
        /// The encrypted byte array.
        /// </returns>
        public byte[] Encrypt(byte[] clearData)
        {
            Check.NotNull(clearData, nameof(clearData));
            return EncryptCore(clearData);
        }

        #endregion
    }
}