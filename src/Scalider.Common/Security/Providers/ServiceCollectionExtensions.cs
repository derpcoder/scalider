﻿#region # using statements #

using System;
using JetBrains.Annotations;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;

#endregion

namespace Scalider.Security.Providers
{

    /// <summary>
    /// Extension methods for setting up encryption services in an
    /// <see cref="IServiceCollection"/>.
    /// </summary>
    public static class ServiceCollectionExtensions
    {

        /// <summary>
        /// Adds the AES encryption service to the specified
        /// <see cref="IServiceCollection"/>.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add
        /// services to.</param>
        /// <returns>
        /// The <see cref="IServiceCollection"/> so that additional calls can
        /// be chained.
        /// </returns>
        public static IServiceCollection AddAesEncryption(
            [NotNull] this IServiceCollection services)
        {
            Check.NotNull(services, nameof(services));

            // Add services
            services.TryAdd(ServiceDescriptor.Singleton(typeof(IEncryptionService),
                typeof(AesEncryptionService)));

            // Done
            return services;
        }

        /// <summary>
        /// Adds the AES encryption service to the specified
        /// <see cref="IServiceCollection"/>.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add
        /// services to.</param>
        /// <param name="callback">A callback used to configure the AES
        /// options.</param>
        /// <returns>
        /// The <see cref="IServiceCollection"/> so that additional calls can
        /// be chained.
        /// </returns>
        public static IServiceCollection AddAesEncryption(
            [NotNull] this IServiceCollection services,
            [NotNull] Action<AesEncryptionOptions> callback)
        {
            Check.NotNull(services, nameof(services));
            Check.NotNull(callback, nameof(callback));

            var options = new AesEncryptionOptions();
            callback.Invoke(options);

            return AddAesEncryption(services, options);
        }

        /// <summary>
        /// Adds the AES encryption service to the specified
        /// <see cref="IServiceCollection"/>.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add
        /// services to.</param>
        /// <param name="options">The AES options to use for encrypting and
        /// decrypting.</param>
        /// <returns>
        /// The <see cref="IServiceCollection"/> so that additional calls can
        /// be chained.
        /// </returns>
        public static IServiceCollection AddAesEncryption(
            [NotNull] this IServiceCollection services,
            [NotNull] AesEncryptionOptions options)
        {
            Check.NotNull(services, nameof(services));
            Check.NotNull(options, nameof(options));
            AesEncryptionService.ValidateOptions(options);

            // Add services
            services.TryAdd(
                ServiceDescriptor.Singleton(typeof(IOptions<AesEncryptionOptions>),
                    new OptionsWrapper<AesEncryptionOptions>(options)));
            services.TryAdd(ServiceDescriptor.Singleton(typeof(IEncryptionService),
                typeof(AesEncryptionService)));

            // Done
            return services;
        }

    }
}