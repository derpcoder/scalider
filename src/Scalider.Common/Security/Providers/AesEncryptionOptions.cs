﻿#region # using statements #

using System;
using System.Diagnostics.CodeAnalysis;
using System.Security.Cryptography;

#endregion

namespace Scalider.Security.Providers
{

    /// <summary>
    /// Represents the configuration options used by the AES encryption
    /// service.
    /// </summary>
    public class AesEncryptionOptions
    {
        #region # Variables #

        private PaddingMode? _padding;// = PaddingMode.PKCS7;
        private CipherMode? _mode;// = CipherMode.CBC;
        private byte[] _salt;
        private byte[] _iv;

        #endregion

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="AesEncryptionOptions"/> class.
        /// </summary>
        public AesEncryptionOptions()
        {
        }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="AesEncryptionOptions"/> class.
        /// </summary>
        /// <param name="base64Salt">The base64 representation of the Salt.</param>
        /// <param name="base64Iv">The base64 representation of the IV.</param>
        public AesEncryptionOptions(string base64Salt, string base64Iv)
        {
            Check.NotNullOrEmpty(base64Salt, nameof(base64Salt));
            Check.NotNullOrEmpty(base64Iv, nameof(base64Iv));

            Salt = Convert.FromBase64String(base64Salt);
            IV = Convert.FromBase64String(base64Iv);
        }

        #region # Properties #

        #region == Public ==

        /// <summary>
        /// Gets or sets the padding mode used by the AES algorithm.
        /// </summary>
        public PaddingMode? Padding
        {
            get => _padding;
            set
            {
                if (value != null)
                {
                    Check.Enum<PaddingMode>(value, nameof(value));
                    _padding = value;
                }
                else
                    _padding = null;
            }
        }

        /// <summary>
        /// Gets or sets the mode of operation of the AES algorithm.
        /// </summary>
        public CipherMode? Mode
        {
            get => _mode;
            set
            {
                if (value != null)
                {
                    Check.Enum<CipherMode>(value, nameof(value));
                    _mode = value;
                }
                else
                    _mode = null;
            }
        }

        /// <summary>
        /// Gets or sets the salt to use for generating the derivated key for
        /// encryption/decryption.
        /// </summary>
        public byte[] Salt
        {
            get => _salt;
            set
            {
                Check.NotNull(value, nameof(value));
                _salt = value;
            }
        }

        /// <summary>
        /// Gets or sets the initialization vector used as password for
        /// generating the derived key for encryption/decryption.
        /// </summary>
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public byte[] IV
        {
            get => _iv;
            set
            {
                Check.NotNull(value, nameof(value));
                _iv = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of iterations for the password derivation
        /// opration.
        /// </summary>
        public int Iterations { get; set; } = 1000;

        #endregion

        #endregion

    }
}