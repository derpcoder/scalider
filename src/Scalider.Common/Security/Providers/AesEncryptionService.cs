﻿#region # using statements #

using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using JetBrains.Annotations;
using Microsoft.Extensions.Options;

#endregion

namespace Scalider.Security.Providers
{

    /// <summary>
    /// Represents a <see cref="IEncryptionService"/> that uses AES for
    /// encryption and decryption.
    /// </summary>
    [DebuggerStepThrough]
    public class AesEncryptionService : BaseEncryptionService
    {
        #region # Variables #

        private readonly AesEncryptionOptions _options;

        #endregion

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="AesEncryptionService"/> class.
        /// </summary>
        /// <param name="options"></param>
        public AesEncryptionService([NotNull] AesEncryptionOptions options)
        {
            Check.NotNull(options, nameof(options));
            ValidateOptions(options);

            _options = options;
        }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="AesEncryptionService"/> class.
        /// </summary>
        /// <param name="optionsAccessor"></param>
        public AesEncryptionService(
            [NotNull] IOptions<AesEncryptionOptions> optionsAccessor)
        {
            Check.NotNull(optionsAccessor, nameof(optionsAccessor));
            Check.PropertyNotNull(optionsAccessor.Value,
                nameof(IOptions<AesEncryptionOptions>.Value),
                nameof(IOptions<AesEncryptionOptions>));
            ValidateOptions(optionsAccessor.Value);

            _options = optionsAccessor.Value;
        }

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Process the decryption of the given byte array.
        /// </summary>
        /// <param name="cipherData">The byte array to decrypt.</param>
        /// <returns>
        /// The decrypted byte array.
        /// </returns>
        protected override byte[] DecryptCore(byte[] cipherData)
        {
            Check.NotNull(cipherData, nameof(cipherData));

            // Decrypt
            byte[] result;
            using (var cipher = CreateProvider())
            {
                var transform = cipher.CreateDecryptor(cipher.Key, cipher.IV);
                using (var memoryStream = new MemoryStream())
                {
                    using (
                        var cryptoStream = new CryptoStream(memoryStream, transform,
                            CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(cipherData, 0, cipherData.Length);
                        cryptoStream.Flush();
                    }

                    result = memoryStream.ToArray();
                }
            }

            // Done
            return result;
        }

        /// <summary>
        /// Process the encryption of the given byte array.
        /// </summary>
        /// <param name="clearData">The byte array to encrypt.</param>
        /// <returns>
        /// The encrypted byte array.
        /// </returns>
        protected override byte[] EncryptCore(byte[] clearData)
        {
            Check.NotNull(clearData, nameof(clearData));

            // Encrypt
            byte[] result;
            using (var cipher = CreateProvider())
            {
                var transform = cipher.CreateEncryptor(cipher.Key, cipher.IV);
                using (var memoryStream = new MemoryStream())
                {
                    using (
                        var cryptoStream = new CryptoStream(memoryStream, transform,
                            CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(clearData, 0, clearData.Length);
                        cryptoStream.Flush();
                    }

                    result = memoryStream.ToArray();
                }
            }

            // Done
            return result;
        }

        #endregion

        #region == Internal ==

        /// <summary>
        /// Determines whether the given <paramref name="options"/> are valid.
        /// </summary>
        /// <param name="options">The options to validate.</param>
        internal static void ValidateOptions(AesEncryptionOptions options)
        {
            // Validate salt
            // 128/192/256
            Check.PropertyNotNull(options.Salt, nameof(AesEncryptionOptions.Salt),
                nameof(AesEncryptionOptions));
            if (options.Salt.Length < 8)
                throw new ArgumentException("Salt is not at least eight bytes.",
                    nameof(options.Salt));

            // Validate IV
            Check.PropertyNotNull(options.IV, nameof(AesEncryptionOptions.IV),
                nameof(AesEncryptionOptions));
            if (options.IV.Length < 8)
            {
                throw new ArgumentException("IV is not at least eight bytes",
                    nameof(options.IV));
            }
        }

        #endregion

        #region == Private ==

        private Aes CreateProvider()
        {
            var key = new Rfc2898DeriveBytes(_options.IV, _options.Salt,
                _options.Iterations);

            // Create the algorithm instance
            var algo = Aes.Create();
            if (algo == null)
            {
                // The AES provider is not available
                throw new CryptographicException(
                    "The AES provider is not available on the current " +
                    "environment.");
            }

            if (_options.Padding.HasValue)
                algo.Padding = _options.Padding.Value;
            if (_options.Mode.HasValue)
                algo.Mode = _options.Mode.Value;
            algo.Key = key.GetBytes(algo.KeySize / 8);
            algo.IV = key.GetBytes(algo.BlockSize / 8);

            return algo;
        }

        #endregion

        #endregion
    }
}