﻿#region # using statements #

using System;
using JetBrains.Annotations;
using Scalider.Internal;

#endregion

namespace Scalider.Security
{

    /// <summary>
    /// Extension methods for <see cref="IEncryptionService"/>.
    /// </summary>
    public static class EncryptionServiceExtensions
    {

        /// <summary>
        /// Decrypts the given string.
        /// </summary>
        /// <param name="service">The <see cref="IEncryptionService"/>.</param>
        /// <param name="encryptedData">The data to decrypt.</param>
        /// <returns>
        /// The plaintext form of the <paramref name="encryptedData"/>.
        /// </returns>
        public static string Decrypt([NotNull] this IEncryptionService service,
            [NotNull] string encryptedData)
        {
            Check.NotNull(service, nameof(service));
            Check.NotNull(encryptedData, nameof(encryptedData));

            var encryptedDataAsBytes = Convert.FromBase64String(encryptedData);
            var plaintextAsBytes = service.Decrypt(encryptedDataAsBytes);
            return EncodingUtil.SecretUtf8Encoding.GetString(plaintextAsBytes);
        }

        /// <summary>
        /// Encrypts the given string.
        /// </summary>
        /// <param name="service">The <see cref="IEncryptionService"/>.</param>
        /// <param name="plaintext">The data to encrypt.</param>
        /// <returns>
        /// The encrypted form of the <paramref name="plaintext"/> data.
        /// </returns>
        public static string Encrypt([NotNull] this IEncryptionService service,
            [NotNull] string plaintext)
        {
            Check.NotNull(service, nameof(service));
            Check.NotNull(plaintext, nameof(plaintext));

            var plaintextAsBytes = EncodingUtil.SecretUtf8Encoding.GetBytes(plaintext);
            var encryptedDataAsBytes = service.Encrypt(plaintextAsBytes);
            return Convert.ToBase64String(encryptedDataAsBytes);
        }

        /// <summary>
        /// Decrypts the given byte array. A return value indicates whether the
        /// decryption succeeded or failed.
        /// </summary>
        /// <param name="service">The <see cref="IEncryptionService"/>.</param>
        /// <param name="cipherData">The byte array to decrypt.</param>
        /// <param name="result">When this method returns, contains the
        /// decrypted representation of <paramref name="cipherData"/>, or null
        /// if the encryption failed.</param>
        /// <returns>
        /// true if the <paramref name="cipherData"/> was decrypted
        /// successfully; otherwise, false.
        /// </returns>
        public static bool TryDecrypt([NotNull] this IEncryptionService service,
            byte[] cipherData, out byte[] result)
        {
            result = null;
            Check.NotNull(service, nameof(service));
            if (cipherData == null)
                return false;

            // Try to decrypt cipher
            try
            {
                result = service.Decrypt(cipherData);
                return true;
            }
            catch
            {
                // ignore any exception
            }

            // Failed to decrypt cipher
            return false;
        }

        /// <summary>
        /// Encrypts the given byte array. A return value indicates whether the
        /// encryption succeeded or failed.
        /// </summary>
        /// <param name="service">The <see cref="IEncryptionService"/>.</param>
        /// <param name="clearData">The byte array to encrypt.</param>
        /// <param name="result">When this method returns, contains the
        /// encrypted representation of <paramref name="clearData"/>, or null
        /// if the encryption failed.</param>
        /// <returns>
        /// true if the <paramref name="clearData"/> was encrypted
        /// successfully; otherwise, false.
        /// </returns>
        public static bool TryEncrypt([NotNull] this IEncryptionService service,
            byte[] clearData, out byte[] result)
        {
            result = null;
            Check.NotNull(service, nameof(service));
            if (clearData == null)
                return false;

            // Try to encrypt data
            try
            {
                result = service.Encrypt(clearData);
                return true;
            }
            catch
            {
                // ignore any exception
            }

            // Failed to encrypt data
            return false;
        }

        /// <summary>
        /// Decrypts the given string. A return value indicates whether the
        /// decryption succeeded or failed.
        /// </summary>
        /// <param name="service">The <see cref="IEncryptionService"/>.</param>
        /// <param name="encryptedData">The data to decrypt.</param>
        /// <param name="plaintext">When this method returns, contains the
        /// decrypted representation of <paramref name="encryptedData"/>, or
        /// null if the encryption failed.</param>
        /// <returns>
        /// true if the <paramref name="encryptedData"/> was decrypted
        /// successfully; otherwise, false.
        /// </returns>
        public static bool TryDecrypt([NotNull] this IEncryptionService service,
            string encryptedData, out string plaintext)
        {
            plaintext = null;
            Check.NotNull(service, nameof(service));
            if (encryptedData == null)
                return false;

            // Try to decrypt the data
            try
            {
                plaintext = Decrypt(service, encryptedData);
                return true;
            }
            catch
            {
                // ignore any exception
            }

            // Failed to decrypt the data
            return false;
        }

        /// <summary>
        /// Encrypts the given string. A return value indicates whether the
        /// encryption succeeded or failed.
        /// </summary>
        /// <param name="service">The <see cref="IEncryptionService"/>.</param>
        /// <param name="plaintext">The data to encrypt.</param>
        /// <param name="encryptedData">When this method returns, contains the
        /// encrypted representation of <paramref name="plaintext"/>, or null
        /// if the encryption failed.</param>
        /// <returns>
        /// true if the <paramref name="plaintext"/> was encrypted
        /// successfully; otherwise, false.
        /// </returns>
        public static bool TryEncrypt([NotNull] this IEncryptionService service,
            string plaintext, out string encryptedData)
        {
            encryptedData = null;
            Check.NotNull(service, nameof(service));
            if (plaintext == null)
                return false;

            // Try to encryp the data
            try
            {
                encryptedData = Encrypt(service, plaintext);
                return true;
            }
            catch
            {
                // ignore any exception
            }

            // Failed to encrypt the data
            return false;
        }

    }
}