﻿#region # using statements #

using System;

#endregion

namespace Scalider.Security.Password
{

    /// <summary>
    /// Implements a BCrypt password hashing.
    /// </summary>
    public class BCryptPasswordHasher : IPasswordHasher
    {
        #region # Constants #

        /// <summary>
        /// Represents the default log2 of the number of rounds of hashing to
        /// apply.
        /// </summary>
        public const int DefaultWorkFactor = 10;

        #endregion

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="BCryptPasswordHasher"/> class.
        /// </summary>
        public BCryptPasswordHasher()
            : this(DefaultWorkFactor)
        {
        }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="BCryptPasswordHasher"/> class.
        /// </summary>
        /// <param name="workFactor"></param>
        public BCryptPasswordHasher(int workFactor)
        {
            if (workFactor <= 0)
                throw new ArgumentOutOfRangeException(nameof(workFactor));

            WorkFactor = workFactor;
        }

        #region # Properties #

        #region == Public ==

        /// <summary>
        /// Gets the log2 of the number of rounds of hashing to apply - the
        /// work factor therefore increases as 2^WorkFactor.
        /// </summary>
        public int WorkFactor { get; }

        #endregion

        #endregion

        #region # IPasswordHasher #

        /// <summary>
        /// Returns a hashed representation of the supplied
        /// <paramref name="password"/>.
        /// </summary>
        /// <param name="password">The password to hash.</param>
        /// <returns>
        /// A hashed representation of the supplied
        /// <paramref name="password"/>.
        /// </returns>
        public virtual string HashPassword(string password)
            => BCrypt.Net.BCrypt.HashPassword(password ?? string.Empty, WorkFactor);

        /// <summary>
        /// Returns a <see cref="PasswordVerificationResult"/> indicating the
        /// result of a password hash comparison.
        /// </summary>
        /// <param name="hashedPassword">The hash value for a user's stored
        /// password.</param>
        /// <param name="providedPassword">The password supplied for
        /// comparison.</param>
        /// <returns>
        /// A <see cref="PasswordVerificationResult"/> indicating the result of
        /// a password hash comparison.
        /// </returns>
        /// <remarks>
        /// Implementations of this method should be time consistent.
        /// </remarks>
        public virtual PasswordVerificationResult VerifyPasswordHash(
            string hashedPassword, string providedPassword)
        {
            Check.NotNullOrEmpty(hashedPassword, nameof(hashedPassword));
            Check.NotNullOrEmpty(providedPassword, nameof(providedPassword));

            return BCrypt.Net.BCrypt.Verify(providedPassword, hashedPassword)
                ? PasswordVerificationResult.Success
                : PasswordVerificationResult.Failed;
        }

        #endregion
    }
}