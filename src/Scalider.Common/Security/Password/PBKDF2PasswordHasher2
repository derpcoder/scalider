﻿#region # using statements #

using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Security.Cryptography;
using JetBrains.Annotations;

#endregion

namespace Scalider.Security.Password
{
    [DebuggerStepThrough, SuppressMessage("ReSharper", "InconsistentNaming")]
    public sealed class PBKDF2PasswordHasher : IPasswordHasher
    {
        #region # Constants #

        private const int DefaultSaltBytes = 24;
        private const int DefaultHashBytes = 18;
        private const int DefaultIterationCount = 64000;

        private const int HashSections = 5;
        private const int HashAlgorithmIndex = 0;
        private const int IterationIndex = 1;
        private const int HashSizeIndex = 2;
        private const int SaltIndex = 3;
        private const int PBKDFIndex = 4;

        #endregion

        #region # Variables #

        private readonly IRngService _rngService;

        #endregion

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="PBKDF2PasswordHasher"/> class.
        /// </summary>
        /// <param name="rngService">The <see cref="IRngService"/> to use for
        /// generating random salt bytes.</param>
        public PBKDF2PasswordHasher([NotNull] IRngService rngService)
            : this(rngService, DefaultSaltBytes)
        {
        }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="PBKDF2PasswordHasher"/> class.
        /// </summary>
        /// <param name="rngService">The <see cref="IRngService"/> to use for
        /// generating random salt bytes.</param>
        /// <param name="saltBytes">The number of random bytes to use as salt.</param>
        public PBKDF2PasswordHasher([NotNull] IRngService rngService, int saltBytes)
            : this(rngService, saltBytes, DefaultHashBytes)
        {
        }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="PBKDF2PasswordHasher"/> class.
        /// </summary>
        /// <param name="rngService">The <see cref="IRngService"/> to use for
        /// generating random salt bytes.</param>
        /// <param name="saltBytes">The number of random bytes to use as salt.</param>
        /// <param name="hashBytes"></param>
        public PBKDF2PasswordHasher([NotNull] IRngService rngService, int saltBytes,
            int hashBytes)
            : this(rngService, saltBytes, hashBytes, DefaultIterationCount)
        {
        }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="PBKDF2PasswordHasher"/> class.
        /// </summary>
        /// <param name="rngService">The <see cref="IRngService"/> to use for
        /// generating random salt bytes.</param>
        /// <param name="saltBytes">The number of random bytes to use as salt.</param>
        /// <param name="hashBytes"></param>
        /// <param name="iterationCount">The number of iterations.</param>
        public PBKDF2PasswordHasher([NotNull] IRngService rngService, int saltBytes,
            int hashBytes, int iterationCount)
        {
            Check.NotNull(rngService, nameof(rngService));

            _rngService = rngService;
            SaltBytes = saltBytes;
            HashBytes = hashBytes;
            IterationCount = iterationCount;
        }

        #region # Properties #

        #region == Public ==

        public int SaltBytes { get; }

        public int HashBytes { get; }

        public int IterationCount { get; }

        #endregion

        #endregion

        /// <summary>
        /// Returns a hashed representation of the supplied
        /// <paramref name="password"/>.
        /// </summary>
        /// <param name="password">The password to hash.</param>
        /// <returns>
        /// A hashed representation of the supplied
        /// <paramref name="password"/>.
        /// </returns>
        public string HashPassword(string password)
        {
            var salt = _rngService.GetBytes(SaltBytes);
            var hash = PBKDF2(password, salt, IterationCount, HashBytes);

            return "sha1:" + IterationCount + ":" + hash.Length + ":" +
                   Convert.ToBase64String(salt) + ":" + Convert.ToBase64String(hash);
        }

        /// <summary>
        /// Returns a <see cref="PasswordVerificationResult"/> indicating the
        /// result of a password hash comparison.
        /// </summary>
        /// <param name="hashedPassword">The hash value for a user's stored
        /// password.</param>
        /// <param name="providedPassword">The password supplied for
        /// comparison.</param>
        /// <returns>
        /// A <see cref="PasswordVerificationResult"/> indicating the result of
        /// a password hash comparison.
        /// </returns>
        /// <remarks>
        /// Implementations of this method should be time consistent.
        /// </remarks>
        public PasswordVerificationResult VerifyPasswordHash(string hashedPassword,
            string providedPassword)
        {
            Check.NotNullOrEmpty(hashedPassword, nameof(hashedPassword));
            Check.NotNullOrEmpty(providedPassword, nameof(providedPassword));

            // Determine if the correct number of sections was found
            var split = hashedPassword.Split(':');
            if (split.Length != HashSections)
                return PasswordVerificationResult.Failed;

            // Determine if the hash algorithm is valid
            if (
                !string.Equals(split[HashAlgorithmIndex], "sha1",
                    StringComparison.OrdinalIgnoreCase))
                return PasswordVerificationResult.Failed;

            // Retrieve iterations
            var iterations = 0;
            try
            {
                iterations = int.Parse(split[IterationIndex]);
            }
            catch
            {
                // ignore
            }

            if (iterations < 1)
                return PasswordVerificationResult.Failed;
        }

        private static byte[] PBKDF2(string password, byte[] salt, int iterationCount,
            int outputByteCount)
        {
            using (var pbkdf2 = new Rfc2898DeriveBytes(password, salt))
            {
                pbkdf2.IterationCount = iterationCount;
                return pbkdf2.GetBytes(outputByteCount);
            }
        }

    }
}