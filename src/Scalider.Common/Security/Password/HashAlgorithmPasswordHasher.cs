﻿#region # using statements #

using System;
using System.Security.Cryptography;
using System.Text;

#endregion

namespace Scalider.Security.Password
{

    /// <summary>
    /// Provides an implementation of the <see cref="IPasswordHasher"/>
    /// interface that uses <see cref="HashAlgorithm"/>s for hashing and
    /// verifying the passwords.
    /// </summary>
    public class HashAlgorithmPasswordHasher : IPasswordHasher
    {
        #region # Variables #

        private readonly HashAlgorithmName _algorithmName;

        #endregion

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="HashAlgorithmPasswordHasher"/> class.
        /// </summary>
        /// <param name="algorithmName"></param>
        public HashAlgorithmPasswordHasher(HashAlgorithmName algorithmName)
        {
            _algorithmName = algorithmName;
        }

        #region # Methods #

        #region == Protected ==

        /// <summary>
        /// Creates the <see cref="HashAlgorithm"/> to use for hashing and
        /// verifying passwords.
        /// </summary>
        /// <returns>
        /// A <see cref="HashAlgorithm"/>.
        /// </returns>
        protected virtual HashAlgorithm CreateAlgorithm()
        {
            HashAlgorithm hasher;
            if (_algorithmName == HashAlgorithmName.MD5)
                hasher = MD5.Create();
            else if (_algorithmName == HashAlgorithmName.SHA1)
                hasher = SHA1.Create();
            else if (_algorithmName == HashAlgorithmName.SHA256)
                hasher = SHA256.Create();
            else if (_algorithmName == HashAlgorithmName.SHA384)
                hasher = SHA384.Create();
            else if (_algorithmName == HashAlgorithmName.SHA512)
                hasher = SHA512.Create();
            else
            {
                throw new CryptographicException(
                    "'{0}' is not a known hash algorithm.", _algorithmName.Name);
            }

            // Done
            return hasher;
        }

        /// <summary>
        /// Returns the hexadecimal hash representation of the given
        /// <paramref name="input"/>.
        /// </summary>
        /// <param name="input">The bytes to hash.</param>
        /// <returns>
        /// The hexadecimal hash representation of <paramref name="input"/>.
        /// </returns>
        protected virtual string GetHash(byte[] input)
        {
            Check.NotNull(input, nameof(input));

            byte[] result;
            using (var algo = CreateAlgorithm())
                result = algo.ComputeHash(input);

            return result.ToHexString();
        }

#endregion

#endregion

        #region # IPasswordHasher #

        /// <summary>
        /// Returns a hashed representation of the supplied
        /// <paramref name="password"/>.
        /// </summary>
        /// <param name="password">The password to hash.</param>
        /// <returns>
        /// A hashed representation of the supplied
        /// <paramref name="password"/>.
        /// </returns>
        public virtual string HashPassword(string password)
            => GetHash(Encoding.UTF8.GetBytes(password ?? string.Empty));

        /// <summary>
        /// Returns a <see cref="PasswordVerificationResult"/> indicating the
        /// result of a password hash comparison.
        /// </summary>
        /// <param name="hashedPassword">The hash value for a user's stored
        /// password.</param>
        /// <param name="providedPassword">The password supplied for
        /// comparison.</param>
        /// <returns>
        /// A <see cref="PasswordVerificationResult"/> indicating the result of
        /// a password hash comparison.
        /// </returns>
        /// <remarks>
        /// Implementations of this method should be time consistent.
        /// </remarks>
        public virtual PasswordVerificationResult VerifyPasswordHash(
            string hashedPassword, string providedPassword)
        {
            Check.NotNullOrEmpty(hashedPassword, nameof(hashedPassword));
            Check.NotNullOrEmpty(providedPassword, nameof(providedPassword));

            var password = GetHash(Encoding.UTF8.GetBytes(providedPassword));
            return string.Equals(password, hashedPassword,
                StringComparison.OrdinalIgnoreCase)
                ? PasswordVerificationResult.Success
                : PasswordVerificationResult.Failed;
        }

        #endregion
    }
}