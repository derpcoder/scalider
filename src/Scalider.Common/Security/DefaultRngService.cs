﻿#region # using statements #

using System.Security.Cryptography;
using System.Threading.Tasks;

#endregion

namespace Scalider.Security
{
    /// <summary>
    /// Provides a default random number generator that uses the
    /// <see cref="RandomNumberGenerator"/> API.
    /// </summary>
    public class DefaultRngService : Disposable, IRngService
    {
        #region # Variables #

        private static readonly byte[] EmptyArray = new byte[0];
        private readonly RandomNumberGenerator _rng = RandomNumberGenerator.Create();

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultRngService"/> class.
        /// </summary>
        public DefaultRngService()
        {
        }

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">true to release both managed and unmanaged
        /// resources; false to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (!IsDisposed && disposing)
                _rng.Dispose();
        }

        #endregion

        #endregion

        #region # IRngService #

        /// <summary>
        /// Fills an array of bytes with a cryptographically strong random
        /// sequence of values.
        /// </summary>
        /// <param name="count">The number of bytes to fill.</param>
        /// <returns>
        /// The array containing the cryptographically strong random sequence
        /// of values.
        /// </returns>
        public virtual byte[] GetBytes(int count)
        {
            ThrowIfDisposed();
            if (count <= 0)
                return EmptyArray;

            var result = new byte[count];
            _rng.GetBytes(result);
            return result;
        }

        /// <summary>
        /// Asynchronously fills an array of bytes with a cryptographically
        /// strong random sequence of values.
        /// </summary>
        /// <param name="count">The number of bytes to fill.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        public virtual Task<byte[]> GetBytesAsync(int count)
            => Task.FromResult(GetBytes(count));

        #endregion
    }
}