﻿#region # using statements #

using System;
using System.Threading.Tasks;
using JetBrains.Annotations;

#endregion

namespace Scalider.Security
{

    /// <summary>
    /// Defines the basic functionality of a random number generator.
    /// </summary>
    public interface IRngService : IDisposable
    {

        /// <summary>
        /// Fills an array of bytes with a cryptographically strong random
        /// sequence of values.
        /// </summary>
        /// <param name="count">The number of bytes to fill.</param>
        /// <returns>
        /// The array containing the cryptographically strong random sequence
        /// of values.
        /// </returns>
        [MustUseReturnValue]
        byte[] GetBytes(int count);

        /// <summary>
        /// Asynchronously fills an array of bytes with a cryptographically
        /// strong random sequence of values.
        /// </summary>
        /// <param name="count">The number of bytes to fill.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        [MustUseReturnValue]
        Task<byte[]> GetBytesAsync(int count);

    }
}