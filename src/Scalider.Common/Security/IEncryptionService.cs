﻿#region # using statements #

using JetBrains.Annotations;

#endregion

namespace Scalider.Security
{

    /// <summary>
    /// Defines the basic functionality of an encryption service capable of
    /// encrypting and decrypting.
    /// </summary>
    public interface IEncryptionService
    {

        /// <summary>
        /// Decrypts the given byte array.
        /// </summary>
        /// <param name="cipherData">The byte array to decrypt.</param>
        /// <returns>
        /// The decrypted byte array.
        /// </returns>
        byte[] Decrypt([NotNull] byte[] cipherData);

        /// <summary>
        /// Encrypts the given byte array.
        /// </summary>
        /// <param name="clearData">The byte array to encrypt.</param>
        /// <returns>
        /// The encrypted byte array.
        /// </returns>
        byte[] Encrypt([NotNull] byte[] clearData);

    }
}