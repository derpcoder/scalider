﻿#region # using statements #

using System;
using System.Security.Cryptography;
using System.Text;

#endregion

namespace Scalider.Security
{

    /// <summary>
    /// Extension methods for hashing strings and array of bytes.
    /// </summary>
    public static class HashExtensions
    {

        /// <summary>
        /// Creates an MD5 hash of the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>
        /// A hash.
        /// </returns>
        public static string Md5(this string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return string.Empty;

            using (var alg = MD5.Create())
            {
                var bytes = Encoding.UTF8.GetBytes(input);
                var hash = alg.ComputeHash(bytes);

                return Convert.ToBase64String(hash);
            }
        }

        /// <summary>
        /// Creates an MD5 hash of the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>
        /// A hash.
        /// </returns>
        public static byte[] Md5(this byte[] input)
        {
            if (input == null)
                return null;

            using (var alg = MD5.Create())
                return alg.ComputeHash(input);
        }

        /// <summary>
        /// Creates a SHA1 hash of the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>
        /// A hash.
        /// </returns>
        public static string Sha1(this string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return string.Empty;

            using (var alg = SHA1.Create())
            {
                var bytes = Encoding.UTF8.GetBytes(input);
                var hash = alg.ComputeHash(bytes);

                return Convert.ToBase64String(hash);
            }
        }

        /// <summary>
        /// Creates an MD5 hash of the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>
        /// A hash.
        /// </returns>
        public static byte[] Sha1(this byte[] input)
        {
            if (input == null)
                return null;

            using (var alg = SHA1.Create())
                return alg.ComputeHash(input);
        }

        /// <summary>
        /// Creates a SHA256 hash of the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>
        /// A hash.
        /// </returns>
        public static string Sha256(this string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return string.Empty;

            using (var alg = SHA256.Create())
            {
                var bytes = Encoding.UTF8.GetBytes(input);
                var hash = alg.ComputeHash(bytes);

                return Convert.ToBase64String(hash);
            }
        }

        /// <summary>
        /// Creates an MD5 hash of the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>
        /// A hash.
        /// </returns>
        public static byte[] Sha256(this byte[] input)
        {
            if (input == null)
                return null;

            using (var alg = SHA256.Create())
                return alg.ComputeHash(input);
        }

        /// <summary>
        /// Creates a SHA384 hash of the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>
        /// A hash.
        /// </returns>
        public static string Sha384(this string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return string.Empty;

            using (var alg = SHA384.Create())
            {
                var bytes = Encoding.UTF8.GetBytes(input);
                var hash = alg.ComputeHash(bytes);

                return Convert.ToBase64String(hash);
            }
        }

        /// <summary>
        /// Creates an MD5 hash of the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>
        /// A hash.
        /// </returns>
        public static byte[] Sha384(this byte[] input)
        {
            if (input == null)
                return null;

            using (var alg = SHA384.Create())
                return alg.ComputeHash(input);
        }

        /// <summary>
        /// Creates a SHA512 hash of the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>
        /// A hash.
        /// </returns>
        public static string Sha512(this string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return string.Empty;

            using (var alg = SHA512.Create())
            {
                var bytes = Encoding.UTF8.GetBytes(input);
                var hash = alg.ComputeHash(bytes);

                return Convert.ToBase64String(hash);
            }
        }

        /// <summary>
        /// Creates an MD5 hash of the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>
        /// A hash.
        /// </returns>
        public static byte[] Sha512(this byte[] input)
        {
            if (input == null)
                return null;

            using (var alg = SHA512.Create())
                return alg.ComputeHash(input);
        }

    }
}