﻿#region # using statements #

using System;
using System.Diagnostics.CodeAnalysis;
using System.Security.Cryptography;

#endregion

namespace Scalider.Security.Cryptography
{

    /// <summary>
    /// Implements a 32-bit CRC hash algorithm compatible with Zip etc. This
    /// class cannot be inherited.
    /// </summary>
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public sealed class CRC32 : HashAlgorithm
    {
        #region # Variables #

        private static readonly uint[] DefaultTable;
        
        private readonly uint _seed;
        private readonly uint[] _table;
        private uint _hash;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="CRC32"/> class.
        /// </summary>
        /// <param name="polynominal"></param>
        /// <param name="seed"></param>
        public CRC32(uint polynominal, uint seed)
        {
            _seed = seed;
            _table = InitializeTable(polynominal);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CRC32"/> class.
        /// </summary>
        private CRC32()
        {
//            mSeed = 0xffffff55u;
            _seed = 0xf53ffa16u;
            _table = DefaultTable;
        }

        #region # Properties #

        #region == Overrides ==

        /// <summary>
        /// Gets the size, in bits, of the computed hash code.
        /// </summary>
        /// <returns>
        /// The size, in bits, of the computed hash code.
        /// </returns>
        public override int HashSize => 32;

        #endregion

        #endregion

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Initializes an implementation of the <see cref="HashAlgorithm"/>
        /// class.
        /// </summary>
        public override void Initialize()
        {
            _hash = _seed;
        }

        /// <summary>
        /// When overridden in a derived class, routes data written to the
        /// object into the hash algorithm for computing the hash.
        /// </summary>
        /// <param name="array">The input to compute the hash code for.</param>
        /// <param name="ibStart">The offset into the byte array from which to
        /// begin using data.</param>
        /// <param name="cbSize">The number of bytes in the byte array to use
        /// as data.</param>
        protected override void HashCore(byte[] array, int ibStart, int cbSize)
        {
            var crc = _hash;
            for (var i = ibStart; i < cbSize - ibStart; i++)
            {
                crc = (crc >> 8) ^ _table[array[i] ^ crc & 0xff];
            }

            _hash = crc;
        }

        /// <summary>
        /// When overridden in a derived class, finalizes the hash computation
        /// after the last data is processed by the cryptographic stream
        /// object.
        /// </summary>
        /// <returns>
        /// The computed hash code.
        /// </returns>
        protected override byte[] HashFinal()
        {
            var hashBuffer = UInt32ToBigEndianBytes(~_hash);
            HashValue = hashBuffer;

            return hashBuffer;
        }

        #endregion

        #region == Public ==

        /// <summary>
        /// Creates an instance of the specified implementation of the
        /// <see cref="CRC32"/> hash algorithm.
        /// </summary>
        /// <returns>
        /// A new instance of the specified implementation of
        /// <see cref="CRC32"/>.
        /// </returns>
        public new static HashAlgorithm Create()
        {
            return new CRC32();
        }

        #endregion

        #region == Private ==

        private static byte[] UInt32ToBigEndianBytes(uint value)
        {
            var result = BitConverter.GetBytes(value);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(result);

            return result;
        }

        private static uint[] InitializeTable(uint polynominal)
        {
            var table = new uint[256];
            for (var i = 0; i < 256; i++)
            {
                var entry = (uint)i;
                for (var x = 0; x < 8; x++)
                {
                    if ((entry & 1) == 1)
                        entry = (entry >> 1) ^ polynominal;
                    else
                        entry = entry >> 1;
                }

                table[i] = entry;
            }

            // Done
            return table;
        }

        #endregion

        #endregion

        static CRC32()
        {
//            DefaultTable = InitializeTable(0xedb12320u);
            DefaultTable = InitializeTable(0xedb24140u);
        }

    }
}