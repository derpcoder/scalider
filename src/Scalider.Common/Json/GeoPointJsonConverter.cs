﻿#region # using statements #

using System;
using System.Diagnostics.CodeAnalysis;
using Newtonsoft.Json;
using Scalider.Location;

#endregion

namespace Scalider.Json
{

    /// <summary>
    /// <see cref="JsonConvert"/> implementation for converting
    /// <see cref="GeoPoint"/>.
    /// </summary>
    public class GeoPointJsonConverter : JsonConverter
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="GeoPointJsonConverter"/> class.
        /// </summary>
        public GeoPointJsonConverter()
        {
        }

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Writes the JSON representation of the object.
        /// </summary>
        /// <param name="writer">The <see cref="JsonWriter" /> to write to.</param>
        /// <param name="value">The value.</param>
        /// <param name="serializer">The calling serializer.</param>
        public override void WriteJson(JsonWriter writer, object value,
            JsonSerializer serializer)
        {
            var obj = value as GeoPoint?;
            if (!obj.HasValue)
                return;

            var lat = obj.Value.Latitude.Value;
            var lng = obj.Value.Longitude.Value;

            writer.WriteValue($"{lat},{lng}");
        }

        /// <summary>
        /// Reads the JSON representation of the object.
        /// </summary>
        /// <param name="reader">The <see cref="JsonReader" /> to read from.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="existingValue">The existing value of object being
        /// read.</param>
        /// <param name="serializer">The calling serializer.</param>
        /// <returns>
        /// The object value
        /// .</returns>
        [SuppressMessage("ReSharper", "CanBeReplacedWithTryCastAndCheckForNull")]
        public override object ReadJson(JsonReader reader, Type objectType,
            object existingValue, JsonSerializer serializer)
        {
            var value = reader.Value;
            if (value == null)
                return objectType == typeof(GeoPoint) ? (object)GeoPoint.Empty : null;

            if (value is string)
                return GeoPoint.Parse((string)value);

            return objectType == typeof(GeoPoint) ? (object)GeoPoint.Empty : null;
        }

        /// <summary>
        /// Determines whether this instance can convert the specified object
        /// type.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <returns>
        /// <c>true</c> if this instance can convert the specified object type;
        /// otherwise, <c>false</c>.
        /// </returns>
        public override bool CanConvert(Type objectType)
            => objectType == typeof(GeoPoint) || objectType == typeof(GeoPoint?);

        #endregion

        #endregion
    }
}