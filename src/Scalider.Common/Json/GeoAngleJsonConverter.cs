﻿#region # using statements #

using System;
using System.Diagnostics.CodeAnalysis;
using Newtonsoft.Json;
using Scalider.Location;

#endregion

namespace Scalider.Json
{

    /// <summary>
    /// <see cref="JsonConverter"/> implementation for converting
    /// <see cref="GeoAngle"/>.
    /// </summary>
    public class GeoAngleJsonConverter : JsonConverter
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="GeoAngleJsonConverter"/> class.
        /// </summary>
        public GeoAngleJsonConverter()
        {
        }

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Writes the JSON representation of the object.
        /// </summary>
        /// <param name="writer">The <see cref="JsonWriter" /> to write to.</param>
        /// <param name="value">The value.</param>
        /// <param name="serializer">The calling serializer.</param>
        public override void WriteJson(JsonWriter writer, object value,
            JsonSerializer serializer)
        {
            var obj = value as GeoAngle?;
            if (obj.HasValue)
                writer.WriteValue(obj.Value.Value);
        }

        /// <summary>
        /// Reads the JSON representation of the object.
        /// </summary>
        /// <param name="reader">The <see cref="JsonReader" /> to read from.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="existingValue">The existing value of object being
        /// read.</param>
        /// <param name="serializer">The calling serializer.</param>
        /// <returns>
        /// The object value
        /// .</returns>
        [SuppressMessage("ReSharper", "CanBeReplacedWithTryCastAndCheckForNull")]
        public override object ReadJson(JsonReader reader, Type objectType,
            object existingValue, JsonSerializer serializer)
        {
            var value = reader.Value;
            if (value == null)
                return objectType == typeof(GeoAngle) ? (object)GeoAngle.Empty : null;

            if (value is string)
                return GeoAngle.Parse((string)value);
            if (value is double)
                return GeoAngle.FromDouble((double)value);

            return objectType == typeof(GeoAngle) ? (object)GeoAngle.Empty : null;
        }

        /// <summary>
        /// Determines whether this instance can convert the specified object
        /// type.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <returns>
        /// <c>true</c> if this instance can convert the specified object type;
        /// otherwise, <c>false</c>.
        /// </returns>
        public override bool CanConvert(Type objectType)
            => objectType == typeof(GeoAngle) || objectType == typeof(GeoAngle?);

        #endregion

        #endregion
    }
}