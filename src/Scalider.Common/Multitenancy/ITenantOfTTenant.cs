﻿namespace Scalider.Multitenancy
{

    /// <summary>
    /// Used to retrieve the <typeparamref name="TTenant"/> instances.
    /// </summary>
    /// <typeparam name="TTenant">The type encapsulating the tenant.</typeparam>
    public interface ITenant<out TTenant>
    {

        /// <summary>
        /// Gets the tenant instance.
        /// </summary>
        TTenant Value { get; }

    }
}