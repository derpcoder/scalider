﻿#region # using statements #

using JetBrains.Annotations;

#endregion

namespace Scalider.Multitenancy
{

    /// <summary>
    /// Represents a <see cref="ITenant{TTenant}"/> that returns the specified
    /// tenant instance.
    /// </summary>
    /// <typeparam name="TTenant">The type encapsulating the tenant.</typeparam>
    public class TenantWrapper<TTenant> : ITenant<TTenant>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="TenantWrapper{TTenant}"/> class.
        /// </summary>
        /// <param name="tenant">The tenant instance to return.</param>
        public TenantWrapper([NotNull] TTenant tenant)
        {
            Check.NotNull(tenant, nameof(tenant));

            Value = tenant;
        }

        #region # ITenant<TTenant> #

        /// <summary>
        /// Gets the tenant instance.
        /// </summary>
        public TTenant Value { get; }

        #endregion
    }
}