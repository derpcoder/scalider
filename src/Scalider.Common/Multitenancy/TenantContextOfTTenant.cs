﻿#region # using statements #

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using JetBrains.Annotations;

#endregion

namespace Scalider.Multitenancy
{

    /// <summary>
    /// Represents a tenant context of the givent
    /// <typeparamref name="TTenant"/>.
    /// </summary>
    /// <typeparam name="TTenant">The type encapsulating the tenant.</typeparam>
    public class TenantContext<TTenant> : Disposable
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="TenantContext{TTenant}"/> class.
        /// </summary>
        /// <param name="tenant">The tenant instance to return.</param>
        public TenantContext([NotNull] TTenant tenant)
        {
            Check.NotNull(tenant, nameof(tenant));

            Tenant = tenant;
            Properties =
                new ConcurrentDictionary<string, object>(
                    StringComparer.OrdinalIgnoreCase);
        }

        #region # Properties #

        #region == Public ==

        /// <summary>
        /// Gets a value indicating the unique identifier of this tenant
        /// context.
        /// </summary>
        public string Id { get; } = Guid.NewGuid().ToString();

        /// <summary>
        /// Gets the tenant.
        /// </summary>
        public TTenant Tenant { get; }

        /// <summary>
        /// Gets all the properties for the tenant.
        /// </summary>
        public IDictionary<string, object> Properties { get; }

        #endregion

        #endregion

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">true to release both managed and unmanaged
        /// resources; false to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (IsDisposed || !disposing)
                return;

            foreach (var prop in Properties)
                TryDisposeProperty(prop.Value);
        }

        #endregion

        #region == Private ==

        private static void TryDisposeProperty(object value)
        {
            var disposable = value as IDisposable;
            if (disposable == null)
                return;

            try
            {
                disposable.Dispose();
            }
            catch (ObjectDisposedException)
            {
            }
        }

        #endregion

        #endregion
    }
}