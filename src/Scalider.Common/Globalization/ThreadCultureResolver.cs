﻿#region # using statements #

using System.Globalization;
using System.Threading;

#endregion

namespace Scalider.Globalization
{

    /// <summary>
    /// Represents an <see cref="ICultureResolver"/> that uses the current
    /// <see cref="Thread"/> to resolve the <see cref="Culture"/> and
    /// <see cref="UiCulture"/>.
    /// </summary>
    public class ThreadCultureResolver : ICultureResolver
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="ThreadCultureResolver"/> class.
        /// </summary>
        public ThreadCultureResolver()
        {
        }

        #region # ICultureResolver #

        /// <summary>
        /// Gets the current <see cref="CultureInfo"/>.
        /// </summary>
        public CultureInfo Culture => Thread.CurrentThread.CurrentCulture;

        /// <summary>
        /// Gets the current UI <see cref="CultureInfo"/>.
        /// </summary>
        public CultureInfo UiCulture => Thread.CurrentThread.CurrentUICulture;

        #endregion
    }
}