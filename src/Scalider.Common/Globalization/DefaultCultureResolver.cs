﻿#region # using statements #

using System.Globalization;
#if !NETSTANDARD
using System.Threading;
#endif

#endregion

namespace Scalider.Globalization
{

    /// <summary>
    /// Represents the default <see cref="ICultureResolver"/>.
    /// </summary>
    public class DefaultCultureResolver : ICultureResolver
    {
        #region # Variables #

        private CultureInfo _culture;
        private CultureInfo _uiCulture;

        #endregion

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="DefaultCultureResolver"/> class.
        /// </summary>
        public DefaultCultureResolver()
        {
        }

        #region # ICultureResolver #

        /// <summary>
        /// Gets or sets the current <see cref="CultureInfo"/>.
        /// </summary>
        public CultureInfo Culture
        {
            get
            {
#if NETSTANDARD
                return _culture ?? CultureInfo.CurrentCulture;
#else
                return _culture ?? Thread.CurrentThread.CurrentCulture;
#endif
            }
            set
            {
                _culture = value;
            }
        }

        /// <summary>
        /// Gets or sets the current UI <see cref="CultureInfo"/>.
        /// </summary>
        public CultureInfo UiCulture
        {
            get
            {
#if NETSTANDARD
                return _uiCulture ?? CultureInfo.CurrentUICulture;
#else
                return _uiCulture ?? Thread.CurrentThread.CurrentUICulture;
#endif
            }
            set
            {
                _uiCulture = value;
            }
        }

        #endregion
    }
}