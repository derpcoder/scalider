﻿#region # using statements #

using System.Globalization;

#endregion

namespace Scalider.Globalization
{

    /// <summary>
    /// Defines the basic functionality of a culture resolver.
    /// </summary>
    public interface ICultureResolver
    {

        /// <summary>
        /// Gets the current <see cref="CultureInfo"/>.
        /// </summary>
        CultureInfo Culture { get; }

        /// <summary>
        /// Gets the current UI <see cref="CultureInfo"/>.
        /// </summary>
        CultureInfo UiCulture { get; }

    }
}