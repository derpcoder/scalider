﻿#region # using statements #

using System;
using System.Threading;

#endregion

namespace Scalider
{

    /// <summary>
    /// Provides a base implementation for disposable objects.
    /// </summary>
    public abstract class Disposable : IDisposable
    {
        #region # Variables #

        private int _isDisposed;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="Disposable"/> class.
        /// </summary>
        protected Disposable()
        {
        }

        /// <summary>
        /// Destructor.
        /// </summary>
        ~Disposable()
        {
            Dispose(false);
        }

        #region # Properties #

        #region == Protected ==

        /// <summary>
        /// Returns true if the current instance has been disposed; otherwise
        /// false;
        /// </summary>
        protected bool IsDisposed
        {
            get
            {
                Interlocked.MemoryBarrier();
                return _isDisposed == 1;
            }
        }

        #endregion

        #endregion

        #region # Methods #

        #region == Protected ==

        /// <summary>
        /// Throws an <see cref="ObjectDisposedException"/> if the object have
        /// been disposed.
        /// </summary>
        protected void ThrowIfDisposed()
        {
            if (IsDisposed)
                throw new ObjectDisposedException(GetType().Name);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">true to release both managed and unmanaged
        /// resources; false to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
        }

        #endregion

        #endregion

        #region # IDisposable #

        /// <summary>
        /// Performs application-defined tasks associated with freeing,
        /// releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (IsDisposed)
                return;
            
            Interlocked.Exchange(ref _isDisposed, 1);
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}