﻿#region # using statements #

using System.IO;

#endregion

namespace Scalider.IO
{

    /// <summary>
    /// Provides helper methods for working with path.
    /// </summary>
    public static class PathHelper
    {

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        public static string ResolvePath(string path, string basePath)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
        {
            Check.NotNullOrEmpty(basePath, nameof(basePath));

            if (string.IsNullOrWhiteSpace(path))
                return basePath;
            if (Path.IsPathRooted(path))
                return path;

            // Replace the alternative directory separator with the
            // platform -specific directory separator
            basePath = basePath.Replace(Path.AltDirectorySeparatorChar,
                Path.DirectorySeparatorChar);
            path = path.Replace(Path.AltDirectorySeparatorChar,
                Path.DirectorySeparatorChar);

            // Done
            return Path.GetFullPath(Path.Combine(Path.GetFullPath(basePath), path));
        }

    }
}