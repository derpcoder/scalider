﻿#region # using statements #

using System.IO;

#endregion

namespace Scalider.IO
{

    /// <summary>
    /// Provides helper methods for working with directories.
    /// </summary>
    public static class DirectoryHelper
    {

        /// <summary>
        /// Creates a new directory if it does not exists.
        /// </summary>
        /// <param name="path">The path to the directory to create.</param>
        public static void CreateIfNoExists(string path)
        {
            path = Check.NotNullOrEmpty(path, nameof(path));
            if (!Directory.Exists(path) && !File.Exists(path))
                Directory.CreateDirectory(path);
        }

    }
}