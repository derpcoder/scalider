﻿#region # using statements #

using System.IO;

#endregion

namespace Scalider.IO
{

    /// <summary>
    /// Represents an <see cref="IPathResolver"/> that uses the current working
    /// directory as content root.
    /// </summary>
    public class DefaultPathResolver : IPathResolver
    {
        #region # Variables #

        private IPathResolver _parent;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultPathResolver"/>
        /// class.
        /// </summary>
        public DefaultPathResolver()
        {
        }

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        public override string ToString() => ContentRootPath;

        #endregion

        #region == Protected ==

        /// <summary>
        /// Removes the squiggle character (~) from the beginning of the path.
        /// </summary>
        /// <param name="path">The path to remove squiggle from.</param>
        /// <returns>
        /// The path without the squiggle.
        /// </returns>
        protected virtual string RemoveSquiggle(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                return string.Empty;

            if (path.StartsWith("~"))
            {
                while (path.StartsWith("~"))
                    path = path.Substring(1);

                path = "./" + path;
            }

            return path;
        }

        #endregion

        #endregion

        #region # IPathResolver #

        /// <summary>
        /// Gets the absolute path to the directory that contains the
        /// application content files.
        /// </summary>
        public virtual string ContentRootPath => Directory.GetCurrentDirectory();

        /// <summary>
        /// Gets the parent <see cref="IPathResolver"/>.
        /// </summary>
        public virtual IPathResolver Parent
        {
            get
            {
                if (_parent != null)
                    return _parent;

                try
                {
                    _parent =
                        new FixedPathResolved(Path.GetDirectoryName(ContentRootPath));
                }
                catch
                {
                    // ignore
                }

                return _parent;
            }
        }

        /// <summary>
        /// Resolves the absolute path to the given <paramref name="path"/>.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns>
        /// The absolute path to <paramref name="path"/>.
        /// </returns>
        public virtual string Resolve(string path)
        {
            path = RemoveSquiggle(path);
            return string.IsNullOrWhiteSpace(path)
                ? string.Empty
                : Path.GetFullPath(Path.Combine(ContentRootPath, path));
        }

        #endregion
    }
}