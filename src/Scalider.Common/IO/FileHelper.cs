﻿#region # using statements #

using System.IO;

#endregion

namespace Scalider.IO
{

    /// <summary>
    /// Provides helper methods for working with files.
    /// </summary>
    public static class FileHelper
    {

        /// <summary>
        /// Checks and deletes given file if it does exists.
        /// </summary>
        /// <param name="path">The name of the file to be deleted. Wildcard
        /// characters are not supported</param>
        public static void DeleteIfExists(string path)
        {
            path = Check.NotNullOrEmpty(path, nameof(path));
            if (File.Exists(path))
                File.Delete(path);
        }
        
    }
}