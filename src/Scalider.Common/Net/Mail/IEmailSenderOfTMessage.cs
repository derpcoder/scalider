﻿#region # using statements #

using System;
using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;

#endregion

namespace Scalider.Net.Mail
{

    /// <summary>
    /// Defines the basic functionality of a service used for sending email
    /// messages.
    /// </summary>
    /// <typeparam name="TMessage"></typeparam>
    public interface IEmailSender<in TMessage> : IDisposable
        where TMessage : class
    {

        /// <summary>
        /// Sends the given <paramref name="message"/>.
        /// </summary>
        /// <param name="message">The email message to send.</param>
        void Send([NotNull] TMessage message);

        /// <summary>
        /// Asynchronously sends the given <paramref name="message"/>.
        /// </summary>
        /// <param name="message">The email message to send.</param>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/>
        /// used to propagate notifications that the operation should be
        /// canceled.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        Task SendAsync([NotNull] TMessage message,
            CancellationToken cancellationToken);

    }
}