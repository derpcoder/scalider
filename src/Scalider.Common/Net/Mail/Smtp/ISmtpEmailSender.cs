﻿#region # using statements #

using MimeKit;

#endregion

namespace Scalider.Net.Mail.Smtp
{

    /// <summary>
    /// Represents an <see cref="IEmailSender{TMessage}"/> that uses SMTP to
    /// deliver messages.
    /// </summary>
    public interface ISmtpEmailSender : IEmailSender<MimeMessage>
    {
    }
}