﻿#region # using statements #

using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;
using MailKit;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MimeKit;

#endregion

namespace Scalider.Net.Mail.Smtp
{

    /// <summary>
    /// Represents a <see cref="IEmailSender{TMessage}"/> that uses SMTP for
    /// delivering email messages.
    /// </summary>
    public class SmtpEmailSender : Disposable, ISmtpEmailSender
    {
        #region # Variables #

        private static readonly IProtocolLogger NullLogger = new NullProtocolLogger();

        private readonly SmtpOptions _options;
        private readonly ILoggerFactory _loggerFactory;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SmtpEmailSender"/>
        /// class.
        /// </summary>
        /// <param name="options"></param>
        public SmtpEmailSender([NotNull] SmtpOptions options)
        {
            Check.NotNull(options, nameof(options));
            _options = options;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SmtpEmailSender"/>
        /// class.
        /// </summary>
        /// <param name="optionsAccessor"></param>
        /// <param name="loggerFactory"></param>
        public SmtpEmailSender([NotNull] IOptions<SmtpOptions> optionsAccessor,
            [CanBeNull] ILoggerFactory loggerFactory = null)
        {
            Check.NotNull(optionsAccessor, nameof(optionsAccessor));
            Check.PropertyNotNull(optionsAccessor.Value,
                nameof(IOptions<SmtpOptions>.Value), nameof(IOptions<SmtpOptions>));

            _options = optionsAccessor.Value;
            _loggerFactory = loggerFactory;
        }

        #region # Methods #

        #region == Private ==

        private IProtocolLogger CreateProtocolLogger()
        {
            var logger = _loggerFactory?.CreateLogger(typeof(SmtpEmailSender));
            return logger != null ? new MicrosoftProtocolLogger(logger) : NullLogger;
        }

        #endregion

        #endregion

        #region # IEmailSender<MimeMessage> #

        /// <summary>
        /// Sends the given <paramref name="message"/>.
        /// </summary>
        /// <param name="message">The <see cref="MimeMessage"/> to send.</param>
        public virtual void Send(MimeMessage message)
        {
            Check.NotNull(message, nameof(message));
            ThrowIfDisposed();

            using (var client = new SmtpClient(CreateProtocolLogger()))
            {
                client.Connect(_options.Host, _options.Port,
                    _options.SecureSocketOptions);

                if (_options.Credentials != null)
                    client.Authenticate(_options.Credentials);

                // Send message and disconnect
                client.Send(message);
                client.Disconnect(true);
            }
        }

        /// <summary>
        /// Asynchronously sends the given <paramref name="message"/>.
        /// </summary>
        /// <param name="message">The <see cref="MimeMessage"/> to send.</param>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/>
        /// used to propagate notifications that the operation should be
        /// canceled.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        public virtual async Task SendAsync(MimeMessage message,
            CancellationToken cancellationToken)
        {
            ThrowIfDisposed();
            Check.NotNull(message, nameof(message));
            cancellationToken.ThrowIfCancellationRequested();

            using (var client = new SmtpClient(CreateProtocolLogger()))
            {
                await
                    client.ConnectAsync(_options.Host, _options.Port,
                        _options.SecureSocketOptions, cancellationToken);

                if (_options.Credentials != null)
                {
                    // Client authentication
                    await
                        client.AuthenticateAsync(_options.Credentials,
                            cancellationToken);
                }

                // Send message and disconnect
                await client.SendAsync(message, cancellationToken);
                await client.DisconnectAsync(true, cancellationToken);
            }
        }

        #endregion
    }
}