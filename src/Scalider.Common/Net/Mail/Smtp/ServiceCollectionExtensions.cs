﻿#region # using statements #

using System;
using JetBrains.Annotations;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;

#endregion

namespace Scalider.Net.Mail.Smtp
{

    /// <summary>
    /// Extension methods for setting up sending email using SMTP in an
    /// <see cref="IServiceCollection"/>.
    /// </summary>
    public static class ServiceCollectionExtensions
    {

        /// <summary>
        /// Adds the SMTP email sender to the specified
        /// <see cref="IServiceCollection"/>.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add
        /// services to.</param>
        /// <returns>
        /// The <see cref="IServiceCollection"/> so that additional calls can
        /// be chained.
        /// </returns>
        public static IServiceCollection AddSmtp(this IServiceCollection services)
        {
            Check.NotNull(services, nameof(services));

            // Add services
            services.TryAdd(ServiceDescriptor.Scoped(typeof(ISmtpEmailSender),
                typeof(SmtpEmailSender)));

            // Done
            return services;
        }

        /// <summary>
        /// Adds the SMTP email sender to the specified
        /// <see cref="IServiceCollection"/>.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add
        /// services to.</param>
        /// <param name="callback">A callback used to configure the SMTP
        /// options.</param>
        /// <returns>
        /// The <see cref="IServiceCollection"/> so that additional calls can
        /// be chained.
        /// </returns>
        public static IServiceCollection AddSmtp(this IServiceCollection services,
            [NotNull] Action<SmtpOptions> callback)
        {
            Check.NotNull(services, nameof(services));
            Check.NotNull(callback, nameof(callback));

            var options = new SmtpOptions();
            callback.Invoke(options);

            return AddSmtp(services, options);
        }

        /// <summary>
        /// Adds the SMTP email sender to the specified
        /// <see cref="IServiceCollection"/>.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add
        /// services to.</param>
        /// <param name="options">The options to use for sending emails using
        /// SMTP.</param>
        /// <returns>
        /// The <see cref="IServiceCollection"/> so that additional calls can
        /// be chained.
        /// </returns>
        public static IServiceCollection AddSmtp(this IServiceCollection services,
            [NotNull] SmtpOptions options)
        {
            Check.NotNull(services, nameof(services));
            Check.NotNull(options, nameof(options));

            // Add services
            services.TryAdd(ServiceDescriptor.Singleton(
                typeof(IOptions<SmtpOptions>),
                new OptionsWrapper<SmtpOptions>(options)));
            services.TryAdd(ServiceDescriptor.Scoped(typeof(ISmtpEmailSender),
                typeof(SmtpEmailSender)));

            // Done
            return services;
        }

    }
}