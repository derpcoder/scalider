﻿#region # using statements #

using System;
using System.Text;
using JetBrains.Annotations;
using MailKit;
using Microsoft.Extensions.Logging;

#endregion

namespace Scalider.Net.Mail.Smtp
{

    /// <summary>
    /// Represents an <see cref="IProtocolLogger"/> that uses the Microsoft's
    /// <see cref="ILogger"/> for logging.
    /// </summary>
    internal sealed class MicrosoftProtocolLogger : Disposable, IProtocolLogger
    {
        #region # Variables #

        private readonly ILogger _logger;

        #endregion

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="MicrosoftProtocolLogger"/> class.
        /// </summary>
        /// <param name="logger"></param>
        public MicrosoftProtocolLogger([NotNull] ILogger logger)
        {
            Check.NotNull(logger, nameof(logger));
            _logger = logger;
        }

        #region # Methods #

        #region == Private ==

        private void Log(byte[] buffer, int offset, int count)
        {
            Check.NotNull(buffer, nameof(buffer));
            if (offset < 0 || offset > buffer.Length)
                throw new ArgumentOutOfRangeException(nameof(buffer));
            if (count < 0 || count > buffer.Length - offset)
                throw new ArgumentOutOfRangeException(nameof(count));

            var message = Encoding.ASCII.GetString(buffer, offset, count);
            _logger.LogDebug(message);
        }

        #endregion

        #endregion

        #region # IProtocolLogger #

        /// <summary>
        /// Logs a connection to the specified URI.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <remarks>
        /// Logs a connection to the specified URI.
        /// </remarks>
        public void LogConnect(Uri uri)
        {
            Check.NotNull(uri, nameof(uri));
            _logger.LogInformation($"Connected to {uri}");
        }

        /// <summary>
        /// Logs a sequence of bytes sent by the client.
        /// </summary>
        /// <param name="buffer">The buffer to log.</param>
        /// <param name="offset">The offset of the first byte to log.</param>
        /// <param name="count">The number of bytes to log.</param>
        /// <remarks>
        /// Logs a sequence of bytes sent by the client.
        /// </remarks>
        public void LogClient(byte[] buffer, int offset, int count)
            => Log(buffer, offset, count);

        /// <summary>
        /// Logs a sequence of bytes sent by the server.
        /// </summary>
        /// <param name="buffer">The buffer to log.</param>
        /// <param name="offset">The offset of the first byte to log.</param>
        /// <param name="count">The number of bytes to log.</param>
        /// <remarks>
        /// Logs a sequence of bytes sent by the server.
        /// </remarks>
        public void LogServer(byte[] buffer, int offset, int count)
            => Log(buffer, offset, count);

        #endregion
    }
}