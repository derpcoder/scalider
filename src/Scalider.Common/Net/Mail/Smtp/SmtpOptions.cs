﻿#region # using statements #

using System;
using System.Net;
using MailKit.Security;

#endregion

namespace Scalider.Net.Mail.Smtp
{

    /// <summary>
    /// Represents the configuration options used for sending emails using
    /// SMTP.
    /// </summary>
    public class SmtpOptions
    {
        #region # Constants #

        /// <summary>
        /// The highest value for a port.
        /// </summary>
        public const int MaxPortNumber = 65535;

        #endregion

        #region # Variables #

        private string _host;
        private int _port = 587;
        private string _userName;
        private string _password;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SmtpOptions"/> class.
        /// </summary>
        public SmtpOptions()
        {
        }

        #region # Properties #

        #region == Public ==

        /// <summary>
        /// Gets or sets the host name to connect to.
        /// </summary>
        public virtual string Host
        {
            get
            {
                return _host;
            }
            set
            {
                Check.NotNullOrEmpty(value, nameof(value));
                _host = value;
            }
        }

        /// <summary>
        /// Gets or sets the port to connect to. If the specified port is
        /// <c>0</c>, then the default port will be used.
        /// </summary>
        public virtual int Port
        {
            get
            {
                return _port;
            }
            set
            {
                if (value < 0 || value > MaxPortNumber)
                    throw new ArgumentOutOfRangeException(nameof(value));

                _port = value;
            }
        }

        /// <summary>
        /// Gets or sets the user name used to authenticate on the SMTP server.
        /// </summary>
        public virtual string UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                _userName = string.IsNullOrWhiteSpace(value) ? string.Empty : value;
            }
        }

        /// <summary>
        /// Gets or sets the password to use to authenticate on the SMTP
        /// server.
        /// </summary>
        public virtual string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = string.IsNullOrWhiteSpace(value) ? string.Empty : value;
            }
        }

        /// <summary>
        /// Gets or sets the secure socket options to when connecting.
        /// </summary>
        public virtual SecureSocketOptions SecureSocketOptions { get; set; } =
            SecureSocketOptions.Auto;

        /// <summary>
        /// Gets the user's credential on the SMTP server.
        /// </summary>
        public virtual ICredentials Credentials
            =>
            string.IsNullOrWhiteSpace(UserName) &&
            string.IsNullOrWhiteSpace(Password)
                ? null
                : new NetworkCredential(UserName, Password);

        #endregion

        #endregion
    }
}