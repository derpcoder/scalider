﻿#region # using statements #

using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using Scalider.Internal;

#endregion

namespace Scalider.Net
{

    /// <summary>
    /// Provides an <see cref="IClock"/> that connects to an NTP server to
    /// retrieve the current date and time.
    /// </summary>
    public class NtpClock : IClock
    {
        #region # Variables #

        /// <summary>
        /// Represents a <see cref="NtpClock"/> that uses the Pool NTP server
        /// to retrieve the current date and time.
        /// </summary>
        public static readonly NtpClock Pool = new NtpClock("pool.ntp.org");

        /// <summary>
        /// Represents a <see cref="NtpClock"/> that uses the Windows NTP
        /// server to retrieve the current date and time.
        /// </summary>
        public static readonly NtpClock Windows = new NtpClock("time.windows.com");

        /// <summary>
        /// Represents a <see cref="NtpClock"/> that uses the NIST NTP server
        /// to retrieve the current date and time.
        /// </summary>
        public static readonly NtpClock Nist = new NtpClock("wwv.nist.gov");

        private static readonly Random PseudoRandom = new Random();
        private static readonly DateTime UnixDateTime = new DateTime(
            1900, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        private readonly string _ntpServer;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="NtpClock"/> class.
        /// </summary>
        /// <param name="ntpServer"></param>
        private NtpClock(string ntpServer)
        {
            Check.NotNullOrEmpty(ntpServer, nameof(ntpServer));
            _ntpServer = ntpServer;
        }

        #region # Methods #

        #region == Public ==

        /// <summary>
        /// Returns a <see cref="NtpClock"/> pointing to one of the Google's
        /// NTP servers.
        /// </summary>
        /// <returns>
        /// The <see cref="NtpClock"/> instance.
        /// </returns>
        public static NtpClock Google()
        {
            var rand = PseudoRandom.Next(1, 4);
            return new NtpClock($"time{rand}.google.com");
        }

        #endregion

        #region == Private ==

        // https://mschwarztoolkit.svn.codeplex.com/svn/NTP/NtpClient.cs
        private DateTimeOffset GetNtpTime()
        {
            var ntpData = new byte[48];

            // Setting the Leap Indicator (LI), Version Number (VN) and Mode values
            // LI = 0 (no warning) VN = 3 (IPv4 only) Mode = 3 (Client mode)
            ntpData[0] = 0x1B;
            for (var i = 1; i < 48; i++)
                ntpData[i] = 0;

#if !NETSTANDARD
            var addresses = Dns.GetHostEntry(_ntpServer).AddressList;
#else
            var addresses =
                AsyncHelper.RunSync(() => Dns.GetHostEntryAsync(_ntpServer))
                           .AddressList;
#endif

            // NTP uses UDP
            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram,
                ProtocolType.Udp);

            // The UDP port number assigned to NTP is 123
            socket.Connect(new IPEndPoint(addresses[0], 123));

            // Stops code hang if NTP is blocked
            //socket.ReceiveTimeout = 3000;

            socket.Send(ntpData);
            socket.Receive(ntpData);

            // Offset to get to the "Transmit Timestamp" field (time at which
            // the reply departed the server for the client, in 64-bit timestamp
            // format"
            const byte serverReplyTime = 40;
            ulong intPart = BitConverter.ToUInt32(ntpData, serverReplyTime);
            ulong fractPart = BitConverter.ToUInt32(ntpData, serverReplyTime + 4);

            intPart = EncodingUtil.SwapEndianness(intPart);
            fractPart = EncodingUtil.SwapEndianness(fractPart);

            //            for (var i = 0; i <= 3; i++)
            //                intPart = 256 * intPart + ntpData[serverReplyTime + i];
            //
            //            for (var i = 4; i <= 7; i++)
            //                fractPart = 256 * fractPart + ntpData[serverReplyTime + i];

            var milliseconds = (intPart * 1000 + (fractPart * 1000) / 0x100000000L);

            socket.Shutdown(SocketShutdown.Both);

#if !NETSTANDARD
            socket.Close();
#endif

            // Done
            return UnixDateTime.AddMilliseconds((long)milliseconds);
        }

        #endregion

        #endregion

        #region # IClock #

        /// <summary>
        /// Gets the current UTC date and time.
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public DateTimeOffset UtcNow => GetNtpTime();

        #endregion
    }
}