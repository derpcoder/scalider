﻿#region # using statements #

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Scalider.Security;

#endregion

namespace Scalider.Net.RandomDotOrg
{

    /// <summary>
    /// Represents an <see cref="IRandom"/> and <see cref="IRngService"/> that
    /// uses Random.org to generate random numbers and bytes.
    /// </summary>
    public sealed class RandomDotOrgService : Disposable, IRandom, IRngService
    {
        #region # Constants #

        /// <summary>
        /// Represents the maximum allowed integer value.
        /// </summary>
        public const int MaxIntegerValue = 1000000000;

        /// <summary>
        /// Represents the maximum number of bytes allowed.
        /// </summary>
        public const int MaxByteCount = 16384;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="RandomDotOrgService"/>
        /// class.
        /// </summary>
        public RandomDotOrgService()
        {
        }

        #region # Methods #

        #region == Private ==

        private static async Task<Exception> HandleWebExceptionAsync(WebException e)
        {
            using (var responseStream = e.Response.GetResponseStream())
            {
                if (responseStream == null)
                    return e;

                // Parse error response
                using (var reader = new StreamReader(responseStream))
                {
                    return new RandomDotOrgException(
                        await reader.ReadToEndAsync(), e);
                }
            }
        }

        [ContractAnnotation("value:null => halt")]
        private static void ThrowIfNullResponseStream(Stream value)
        {
            if (value != null)
                return;

            throw new RandomDotOrgException(
                "There was an unexpected error that prevented the request to " +
                "complete successfully");
        }

        #endregion

        #endregion

        #region # IRandom #

        /// <summary>
        /// Returns a non-negative random integer.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer that is greater than or equal to 0 and less
        /// than <see cref="int.MaxValue" />.
        /// </returns>
        public int Next() => Next(0, MaxIntegerValue);

        /// <summary>
        /// Asynchronously returns a non-negative random integer.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        public Task<int> NextAsync() => NextAsync(0, MaxIntegerValue);

        /// <summary>
        /// Returns a non-negative random integer that is less than the
        /// specified maximum.
        /// </summary>
        /// <param name="maxValue"></param>
        /// <returns>
        /// A 32-bit signed integer that is greater than or equal to 0, and
        /// less than <paramref name="maxValue" />; that is, the range of
        /// return values ordinarily includes 0 but not
        /// <paramref name="maxValue" />. However, if
        /// <paramref name="maxValue" /> equals 0, <paramref name="maxValue" />
        /// is returned.
        /// </returns>
        public int Next(int maxValue) => Next(0, maxValue);

        /// <summary>
        /// Asynchronously eturns a non-negative random integer that is less
        /// than the specified maximum.
        /// </summary>
        /// <param name="maxValue"></param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        public Task<int> NextAsync(int maxValue) => NextAsync(0, maxValue);

        /// <summary>
        /// Returns a random integer that is within a specified range.
        /// </summary>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <returns>
        /// A 32-bit signed integer greater than or equal to
        /// <paramref name="minValue" /> and less than
        /// <paramref name="maxValue" />; that is, the range of return values
        /// includes <paramref name="minValue" /> but not
        /// <paramref name="maxValue" />. If <paramref name="minValue" /> equals
        /// <paramref name="maxValue" />, <paramref name="minValue" /> is
        /// returned.
        /// </returns>
        public int Next(int minValue, int maxValue)
            => AsyncHelper.RunSync(() => NextAsync(minValue, maxValue));

        /// <summary>
        /// Asynchronously eturns a random integer that is within a specified range.
        /// </summary>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        public async Task<int> NextAsync(int minValue, int maxValue)
        {
            var webRequest =
                WebRequest.CreateHttp(
                    $"http://www.random.org/integers/?num=1&min={minValue}" +
                    $"&max={Math.Min(MaxIntegerValue, maxValue)}&col=1&base=10&" +
                    "format=plain&rnd=new");
            webRequest.Method = "GET";

            // Try to retrieve value
            try
            {
                var response = await webRequest.GetResponseAsync();

                int rand;
                using (var responseStream = response.GetResponseStream())
                {
                    ThrowIfNullResponseStream(responseStream);
                    using (var reader = new StreamReader(responseStream))
                        rand = int.Parse(await reader.ReadToEndAsync());
                }

                // Done
                return rand;
            }
            catch (WebException e)
            {
                throw await HandleWebExceptionAsync(e);
            }
        }

        /// <summary>
        /// Returns a random floating-point number that is greater than or
        /// equal to 0.0, and less than 1.0.
        /// </summary>
        /// <returns>
        /// A double-precision floating point number that is greater than or
        /// equal to 0.0, and less than 1.0.
        /// </returns>
        public double NextDouble() => AsyncHelper.RunSync(NextDoubleAsync);

        /// <summary>
        /// Returns a random floating-point number that is greater than or
        /// equal to 0.0, and less than 1.0.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        public async Task<double> NextDoubleAsync()
        {
            var webRequest =
                WebRequest.CreateHttp(
                    "https://www.random.org/decimal-fractions/?num=1&dec=10&" +
                    "col=1&format=plain&rnd=new");
            webRequest.Method = "GET";

            // Try to retrieve value
            try
            {
                var response = await webRequest.GetResponseAsync();

                double rand;
                using (var responseStream = response.GetResponseStream())
                {
                    ThrowIfNullResponseStream(responseStream);
                    using (var reader = new StreamReader(responseStream))
                        rand = double.Parse(await reader.ReadToEndAsync());
                }

                // Done
                return rand;
            }
            catch (WebException e)
            {
                throw await HandleWebExceptionAsync(e);
            }
        }

        #endregion

        #region # IRngService #

        /// <summary>
        /// Fills an array of bytes with a cryptographically strong random
        /// sequence of values.
        /// </summary>
        /// <param name="count">The number of bytes to fill.</param>
        /// <returns>
        /// The array containing the cryptographically strong random sequence
        /// of values.
        /// </returns>
        public byte[] GetBytes(int count)
            => AsyncHelper.RunSync(() => GetBytesAsync(count));

        /// <summary>
        /// Asynchronously fills an array of bytes with a cryptographically
        /// strong random sequence of values.
        /// </summary>
        /// <param name="count">The number of bytes to fill.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        public async Task<byte[]> GetBytesAsync(int count)
        {
            var webRequest =
                WebRequest.CreateHttp(
                    "https://www.random.org/cgi-bin/randbyte?nbytes=" +
                    $"{Math.Min(MaxByteCount, count)}&format=d");
            webRequest.Method = "GET";

            // Try to retrieve value
            try
            {
                var response = await webRequest.GetResponseAsync();

                var rand = new List<byte>();
                using (var responseStream = response.GetResponseStream())
                {
                    ThrowIfNullResponseStream(responseStream);
                    using (var reader = new StreamReader(responseStream))
                    {
                        var content = await reader.ReadToEndAsync();
                        rand.AddRange(from num in content.Split(' ')
                                      where !string.IsNullOrWhiteSpace(num)
                                      select byte.Parse(num));
                    }
                }

                // Done
                return rand.ToArray();
            }
            catch (WebException e)
            {
                throw await HandleWebExceptionAsync(e);
            }
        }

        #endregion
    }
}