﻿#region # using statements #

using System;

#endregion

namespace Scalider.Net.RandomDotOrg
{

    /// <summary>
    /// Represents errors that occur during the consumption of the Random.org
    /// service.
    /// </summary>
    public class RandomDotOrgException : Exception
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="RandomDotOrgException"/> class.
        /// </summary>
        /// <param name="message">The error message that explains the reason
        /// for the exception.</param>
        public RandomDotOrgException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="RandomDotOrgException"/> class.
        /// </summary>
        /// <param name="message">The error message that explains the reason
        /// for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the
        /// current exception, or a null reference if no inner exception is
        /// specified.</param>
        public RandomDotOrgException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

    }
}