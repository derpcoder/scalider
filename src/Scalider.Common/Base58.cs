﻿#region # using statements #

using System;
using JetBrains.Annotations;

#endregion

namespace Scalider
{

    /// <summary>
    /// Provides methods for shortening and expanding numbers.
    /// </summary>
    public static class Base58
    {

        #region # Constants #

        private const string Alphabet = "123456789abcdefghijkmnopqrstuvwxyzABC" +
                                        "DEFGHJKLMNPQRSTUVWXYZ";

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="numberToShorten">The number to be shorten.</param>
        /// <returns>
        /// The shortened representation of the given
        /// <paramref name="numberToShorten"/>.
        /// </returns>
        public static string Shorten(short numberToShorten)
        {
            return Shorten((long)numberToShorten);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="numberToShorten">The number to be shorten.</param>
        /// <returns>
        /// The shortened representation of the given
        /// <paramref name="numberToShorten"/>.
        /// </returns>
        public static string Shorten(int numberToShorten)
        {
            return Shorten((long)numberToShorten);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="numberToShorten">The number to be shorten.</param>
        /// <returns>
        /// The shortened representation of the given
        /// <paramref name="numberToShorten"/>.
        /// </returns>
        public static string Shorten(long numberToShorten)
        {
            if (numberToShorten < 0)
            {
                throw new ArgumentException(
                    "The number to shorten must be equal or greater than zero (0)",
                    nameof(numberToShorten));
            }

            return Shorten((ulong)numberToShorten);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="numberToShorten">The number to be shorten.</param>
        /// <returns>
        /// The shortened representation of the given
        /// <paramref name="numberToShorten"/>.
        /// </returns>
        public static string Shorten(ushort numberToShorten)
        {
            return Shorten((ulong)numberToShorten);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="numberToShorten">The number to be shorten.</param>
        /// <returns>
        /// The shortened representation of the given
        /// <paramref name="numberToShorten"/>.
        /// </returns>
        public static string Shorten(uint numberToShorten)
        {
            return Shorten((ulong)numberToShorten);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="numberToShorten">The number to be shorten.</param>
        /// <returns>
        /// The shortened representation of the given
        /// <paramref name="numberToShorten"/>.
        /// </returns>
        public static string Shorten(ulong numberToShorten)
        {
            var converted = "";
            var alphabetLenth = (uint)Alphabet.Length;

            while (numberToShorten > 0)
            {
                var remainder = (numberToShorten % alphabetLenth);
                numberToShorten = Convert.ToUInt64(numberToShorten / alphabetLenth);
                converted = Alphabet[Convert.ToInt32(remainder)] + converted;
            }

            // Done
            return converted;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="base58String">The string to be expanded.</param>
        /// <returns>
        /// The expanded numeric representation of the given
        /// <paramref name="base58String"/>.
        /// </returns>
        public static ulong Expand([NotNull] string base58String)
        {
            Check.NotNullOrEmpty(base58String, nameof(base58String));

            var converted = (ulong)0;
            var tmpNumberConverter = (ulong)1;

            while (base58String.Length > 0)
            {
                var currentChr = base58String.Substring(base58String.Length - 1);
                converted = converted +
                            (tmpNumberConverter *
                             (uint)Alphabet.IndexOf(currentChr, StringComparison.Ordinal));
                tmpNumberConverter = tmpNumberConverter * (uint)Alphabet.Length;
                base58String = base58String.Substring(0, base58String.Length - 1);
            }

            // Done
            return converted;
        }

    }
}