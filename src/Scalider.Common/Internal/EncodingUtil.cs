﻿#region # using statements #

using System.Text;

#endregion

namespace Scalider.Internal
{
    internal static class EncodingUtil
    {

        /// <summary>
        /// UTF8 encoding that fails on invalid chars.
        /// </summary>
        public static readonly UTF8Encoding SecretUtf8Encoding =
            new UTF8Encoding(false, true);

        /// <summary>
        /// Converts from big-endian to little-endian.
        /// </summary>
        /// <param name="x"></param>
        /// <returns>
        /// The little-endian representation of <paramref name="x"/>.
        /// </returns>
        // stackoverflow.com/a/3294698/162671
        public static uint SwapEndianness(ulong x)
            =>
            (uint)
            (((x & 0x000000ff) << 24) + ((x & 0x0000ff00) << 8) +
             ((x & 0x00ff0000) >> 8) + ((x & 0xff000000) >> 24));

    }
}