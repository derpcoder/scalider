﻿#region # using statements #

using System;

#endregion

namespace Scalider
{

    /// <summary>
    /// Represents an <see cref="IClock"/> that uses the date and time of the
    /// local system.
    /// </summary>
    public class SystemClock : IClock
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemClock"/> class.
        /// </summary>
        public SystemClock()
        {
        }

        #region # IClock #

        /// <summary>
        /// Gets the current UTC date and time.
        /// </summary>
        public virtual DateTimeOffset UtcNow => DateTimeOffset.UtcNow;

        #endregion
    }
}