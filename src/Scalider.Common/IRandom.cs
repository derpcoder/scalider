﻿#region # using statements #

using System.Threading.Tasks;
using JetBrains.Annotations;

#endregion

namespace Scalider
{

    /// <summary>
    /// Represents a provider for generating random values.
    /// </summary>
    public interface IRandom
    {

        /// <summary>
        /// Returns a non-negative random integer.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer that is greater than or equal to 0 and less
        /// than <see cref="int.MaxValue" />.
        /// </returns>
        [MustUseReturnValue]
        int Next();

        /// <summary>
        /// Asynchronously returns a non-negative random integer.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        [MustUseReturnValue]
        Task<int> NextAsync();

        /// <summary>
        /// Returns a non-negative random integer that is less than the
        /// specified maximum.
        /// </summary>
        /// <param name="maxValue"></param>
        /// <returns>
        /// A 32-bit signed integer that is greater than or equal to 0, and
        /// less than <paramref name="maxValue" />; that is, the range of
        /// return values ordinarily includes 0 but not
        /// <paramref name="maxValue" />. However, if
        /// <paramref name="maxValue" /> equals 0, <paramref name="maxValue" />
        /// is returned.
        /// </returns>
        [MustUseReturnValue]
        int Next(int maxValue);

        /// <summary>
        /// Asynchronously eturns a non-negative random integer that is less
        /// than the specified maximum.
        /// </summary>
        /// <param name="maxValue"></param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        [MustUseReturnValue]
        Task<int> NextAsync(int maxValue);

        /// <summary>
        /// Returns a random integer that is within a specified range.
        /// </summary>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <returns>
        /// A 32-bit signed integer greater than or equal to
        /// <paramref name="minValue" /> and less than
        /// <paramref name="maxValue" />; that is, the range of return values
        /// includes <paramref name="minValue" /> but not
        /// <paramref name="maxValue" />. If <paramref name="minValue" /> equals
        /// <paramref name="maxValue" />, <paramref name="minValue" /> is
        /// returned.
        /// </returns>
        [MustUseReturnValue]
        int Next(int minValue, int maxValue);

        /// <summary>
        /// Asynchronously eturns a random integer that is within a specified range.
        /// </summary>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        [MustUseReturnValue]
        Task<int> NextAsync(int minValue, int maxValue);

        /// <summary>
        /// Returns a random floating-point number that is greater than or
        /// equal to 0.0, and less than 1.0.
        /// </summary>
        /// <returns>
        /// A double-precision floating point number that is greater than or
        /// equal to 0.0, and less than 1.0.
        /// </returns>
        [MustUseReturnValue]
        double NextDouble();

        /// <summary>
        /// Returns a random floating-point number that is greater than or
        /// equal to 0.0, and less than 1.0.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        [MustUseReturnValue]
        Task<double> NextDoubleAsync();

    }
}