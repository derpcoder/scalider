﻿#region # using statements #

using System;

#endregion

namespace Scalider
{

    /// <summary>
    /// Represents a clock which can return the current date and time as a
    /// <see cref="DateTimeOffset"/>.
    /// </summary>
    public interface IClock
    {

        /// <summary>
        /// Gets the current UTC date and time.
        /// </summary>
        DateTimeOffset UtcNow { get; }

    }
}