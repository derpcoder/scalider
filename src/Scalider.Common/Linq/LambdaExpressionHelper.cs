﻿#region # using statements #

using System;
using System.Linq.Expressions;

#endregion

namespace Scalider.Linq
{

    /// <summary>
    /// A set of helpers for dynamically creating lambda expressions.
    /// </summary>
    public static class LambdaExpressionHelper
    {

        /// <summary>
        /// Creates a selection lambda expression for the entity property or
        /// field.
        /// </summary>
        /// <typeparam name="T">The type to create lambda expression for.</typeparam>
        /// <param name="propertyOrFieldName">The name of the property or field
        /// to select.</param>
        /// <returns>
        /// A selection lambda expression for the specified entity property or
        /// field.
        /// </returns>
        public static Expression<Func<T, object>> CreateLambdaForPropertyOrField<T>(
                string propertyOrFieldName)
            => CreateLambdaForPropertyOrField<T, object>(propertyOrFieldName);

        /// <summary>
        /// Creates a selection lambda expression for the entity property or
        /// field.
        /// </summary>
        /// <typeparam name="T">The type to create lambda expression for.</typeparam>
        /// <typeparam name="TValue">The type encapsulating the return type of
        /// the lambda expression.</typeparam>
        /// <param name="propertyOrFieldName">The name of the property or field
        /// to select.</param>
        /// <returns>
        /// A selection lambda expression for the specified entity property or
        /// field.
        /// </returns>
        public static Expression<Func<T, TValue>> CreateLambdaForPropertyOrField
            <T, TValue>(string propertyOrFieldName)
        {
            var lambdaParam = Expression.Parameter(typeof(T));
            var lambdaBody = Expression.PropertyOrField(lambdaParam,
                propertyOrFieldName);
            var convert = Expression.Convert(lambdaBody, typeof(TValue));

            return Expression.Lambda<Func<T, TValue>>(convert, lambdaParam);
        }

    }
}