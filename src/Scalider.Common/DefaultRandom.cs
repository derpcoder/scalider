﻿#region # using statements #

using System;
using System.Threading.Tasks;

#endregion

namespace Scalider
{

    /// <summary>
    /// Represents the default <see cref="IRandom"/> implementation.
    /// </summary>
    public class DefaultRandom : IRandom
    {
        #region # Variables #

        private readonly Random _random;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultRandom"/>
        /// class.
        /// </summary>
        public DefaultRandom()
        {
            _random = new Random();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultRandom"/>
        /// class.
        /// </summary>
        /// <param name="seed"></param>
        public DefaultRandom(int seed)
        {
            _random = new Random(seed);
        }

        #region # IRandom #

        /// <summary>
        /// Returns a non-negative random integer.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer that is greater than or equal to 0 and less
        /// than <see cref="int.MaxValue" />.
        /// </returns>
        public int Next() => Next(0, int.MaxValue);

        /// <summary>
        /// Asynchronously returns a non-negative random integer.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        public Task<int> NextAsync() => NextAsync(0, int.MaxValue);

        /// <summary>
        /// Returns a non-negative random integer that is less than the
        /// specified maximum.
        /// </summary>
        /// <param name="maxValue"></param>
        /// <returns>
        /// A 32-bit signed integer that is greater than or equal to 0, and
        /// less than <paramref name="maxValue" />; that is, the range of
        /// return values ordinarily includes 0 but not
        /// <paramref name="maxValue" />. However, if
        /// <paramref name="maxValue" /> equals 0, <paramref name="maxValue" />
        /// is returned.
        /// </returns>
        public int Next(int maxValue) => Next(0, int.MaxValue);

        /// <summary>
        /// Asynchronously eturns a non-negative random integer that is less
        /// than the specified maximum.
        /// </summary>
        /// <param name="maxValue"></param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        public Task<int> NextAsync(int maxValue) => NextAsync(0, int.MaxValue);

        /// <summary>
        /// Returns a random integer that is within a specified range.
        /// </summary>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <returns>
        /// A 32-bit signed integer greater than or equal to
        /// <paramref name="minValue" /> and less than
        /// <paramref name="maxValue" />; that is, the range of return values
        /// includes <paramref name="minValue" /> but not
        /// <paramref name="maxValue" />. If <paramref name="minValue" /> equals
        /// <paramref name="maxValue" />, <paramref name="minValue" /> is
        /// returned.
        /// </returns>
        public virtual int Next(int minValue, int maxValue)
            => _random.Next(minValue, maxValue);

        /// <summary>
        /// Asynchronously eturns a random integer that is within a specified range.
        /// </summary>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        public virtual Task<int> NextAsync(int minValue, int maxValue)
            => Task.FromResult(Next(minValue, maxValue));

        /// <summary>
        /// Returns a random floating-point number that is greater than or
        /// equal to 0.0, and less than 1.0.
        /// </summary>
        /// <returns>
        /// A double-precision floating point number that is greater than or
        /// equal to 0.0, and less than 1.0.
        /// </returns>
        public double NextDouble() => _random.NextDouble();

        /// <summary>
        /// Returns a random floating-point number that is greater than or
        /// equal to 0.0, and less than 1.0.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        public Task<double> NextDoubleAsync() => Task.FromResult(NextDouble());

        #endregion
    }
}