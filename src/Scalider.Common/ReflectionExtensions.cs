﻿#region # using statements #

using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;

#endregion

namespace Scalider
{

    /// <summary>
    /// Extension methods for reflection.
    /// </summary>
    public static class ReflectionExtensions
    {

        /// <summary>
        /// Determines whether this type is assignable to
        /// <typeparamref name="T" />.
        /// </summary>
        /// <typeparam name="T">The type to test assignability to.</typeparam>
        /// <param name="this">The type to test.</param>
        /// <returns>
        /// true if <paramref name="this"/> is assignable to reference of type
        /// <typeparamref name="T"/>; otherwise, false.
        /// </returns>
        public static bool Is<T>(this Type @this)
        {
            Check.NotNull(@this, nameof(@this));
            return typeof(T).IsAssignableFrom(@this);
        }

        /// <summary>
        /// Determines whether this type is assignable to
        /// <paramref name="requiredType"/>.
        /// </summary>
        /// <param name="this">The type to test.</param>
        /// <param name="requiredType">The type to test assignability to.</param>
        /// <returns>
        /// true if <paramref name="this"/> is assignable to reference of type
        /// <paramref name="requiredType"/>; otherwise, false.
        /// </returns>
        public static bool Is(this Type @this, Type requiredType)
        {
            Check.NotNull(@this, nameof(@this));
            Check.NotNull(requiredType, nameof(requiredType));

            return requiredType.IsAssignableFrom(@this);
        }

        /// <summary>
        /// Determines whether the <paramref name="this"/> is an anonymous
        /// <see cref="Type"/>.
        /// </summary>
        /// <param name="this">The type to test.</param>
        /// <returns>
        /// true if the give <paramref name="this"/> is an anonymous
        /// <see cref="Type"/>; otherwise, false.
        /// </returns>
        public static bool IsAnonymousType(this Type @this)
        {
            Check.NotNull(@this, nameof(@this));

            // HACK: The only way to detect anonymous types right now.
            var typeInfo = @this.GetTypeInfo();
            return
                typeInfo.IsDefined(typeof(CompilerGeneratedAttribute), false) &&
                typeInfo.IsGenericType && @this.Name.Contains("AnonymousType") &&
                (@this.Name.StartsWith("<>") || @this.Name.StartsWith("VB$")) &&
                (typeInfo.Attributes & TypeAttributes.NotPublic) ==
                TypeAttributes.NotPublic;
        }

        /// <summary>
        /// Safely returns the set of loadable types from an assembly.
        /// </summary>
        /// <param name="assembly">The <see cref="Assembly" /> from which to 
        /// load types.</param>
        /// <returns>
        /// The set of types from the <paramref name="assembly" />,
        /// or the subset of types that could be loaded if there was any
        /// error.
        /// </returns>
        public static IEnumerable<Type> GetLoadableTypes(this Assembly assembly)
        {
            assembly = Check.NotNull(assembly, nameof(assembly));

            // Try to retrieve all the valid assembly types
            IEnumerable<Type> result;
            try
            {
                result = assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException ex)
            {
                // One or more types could not be loaded
                result = ex.Types.Where(t => t != null);
            }

            // Done
            return result;
        }

        /// <summary>
        /// Safely retrieves all the types that are assignable to the given
        /// <typeparamref name="T"/> from an assembly.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="assembly">The <see cref="Assembly" /> from which to 
        /// load types.</param>
        /// <returns>
        /// The set of types from the <paramref name="assembly" />,
        /// or the subset of types that could be loaded if there was any
        /// error.
        /// </returns>
        public static IEnumerable<Type> GetLoadableTypesOf<T>(this Assembly assembly)
            => GetLoadableTypes(assembly).Where(Is<T>);

        /// <summary>
        /// Safely retrieves all the types that are assignable to the given
        /// <typeparamref name="T"/> from a collection of assemblies.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="assemblies">The list of assemblies from which to load
        /// types.</param>
        /// <returns>
        /// The set of types from the <paramref name="assemblies" />,
        /// or the subset of types that could be loaded if there was any
        /// error.
        /// </returns>
        public static IEnumerable<Type> GetLoadableTypesOf<T>(
            this IEnumerable<Assembly> assemblies)
        {
            assemblies = Check.NotNull(assemblies, nameof(assemblies));
            return
                assemblies.Where(a => a != null)
                          .SelectMany(GetLoadableTypes)
                          .Where(Is<T>);
        }

    }
}