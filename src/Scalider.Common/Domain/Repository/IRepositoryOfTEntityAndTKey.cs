﻿#region # using statements #

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Scalider.Domain.Entities;

#endregion

namespace Scalider.Domain.Repository
{

    /// <summary>
    /// Represents a repository which can read and write entities of
    /// <typeparamref name="TEntity"/>.
    /// </summary>
    /// <typeparam name="TEntity">The type encapsulating the type of the
    /// entity.</typeparam>
    /// <typeparam name="TKey">The type encapsulating the primary key of the
    /// entity.</typeparam>
    public interface IRepository<TEntity, in TKey> : IRepository
        where TEntity : class, IEntity<TKey>
        where TKey : IEquatable<TKey>
    {

        /// <summary>
        /// Gets the count of all entities in this repository.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The number of entities in this repository.
        /// </returns>
        int Count([NotNull] Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Asynchronously gets the count of all entities in this repository.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        Task<int> CountAsync([NotNull] Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Gets the count of all entities in this repository.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The number of entities in this repository.
        /// </returns>
        /// <remarks>
        /// Use this method when the return value is expected to be greater
        /// than <see cref="int.MaxValue"/>.
        /// </remarks>
        long LongCount([NotNull] Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Asynchronously gets the count of all entities in this repository.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        /// <remarks>
        /// Use this method when the return value is expected to be greater
        /// than <see cref="int.MaxValue"/>.
        /// </remarks>
        Task<long> LongCountAsync([NotNull] Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Returns the entity with the given primary key or <c>null</c> if no
        /// entity with the given primary key is found.
        /// </summary>
        /// <param name="id">The primary key of the entity to retrieve.</param>
        /// <returns>
        /// The entity matching the primary key or <c>null</c> if no entity is
        /// found.
        /// </returns>
        TEntity Get([NotNull] TKey id);

        /// <summary>
        /// Asynchronously returns the entity with the given primary key or
        /// <c>null</c> if no entity with the given primary key is found.
        /// </summary>
        /// <param name="id">The primary key of the entity to retrieve.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        Task<TEntity> GetAsync([NotNull] TKey id);

        /// <summary>
        /// Returns all the entities for this repository.
        /// </summary>
        /// <returns>
        /// A collection containing all the entities for this repository.
        /// </returns>
        IEnumerable<TEntity> GetAll();

        /// <summary>
        /// Asynchronously returns all the entities for this repository.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        Task<IEnumerable<TEntity>> GetAllAsync();

        /// <summary>
        /// Returns collection containing all the entities that satisfies a
        /// condition.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The collection containing all the entities that satisfies the
        /// condition.
        /// </returns>
        IEnumerable<TEntity> Find([NotNull] Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Asynchronously returns collection containing all the entities that
        /// satisfies a condition.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        Task<IEnumerable<TEntity>> FindAsync(
            [NotNull] Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Returns the only entity that satisfies a condition or throws an
        /// exception if there is more than exactly one entity that satisfies
        /// the condition.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The single entity that satisfies the condition.
        /// </returns>
        TEntity Single([NotNull] Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Asynchronously returns the only entity that satisfies a condition
        /// or throws an exception if there is more than exactly one entity
        /// that satisfies the condition.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        Task<TEntity> SingleAsync([NotNull] Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Returns the first entity that satisfies a condition or <c>null</c>
        /// if no such entity is found.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The entity that satisfies the condition or <c>null</c> if no such
        /// entity is found.
        /// </returns>
        TEntity FirstOrDefault([NotNull] Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Asynchronously returns the first entity that satisfies a
        /// condition or <c>null</c> if no such entity is found.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        Task<TEntity> FirstOrDefaultAsync(
            [NotNull] Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Inserts a new entity.
        /// </summary>
        /// <param name="entity">The entity to insert.</param>
        void Insert([NotNull] TEntity entity);

        /// <summary>
        /// Asynchronously inserts a new entity.
        /// </summary>
        /// <param name="entity">The entity to insert.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        Task InsertAsync([NotNull] TEntity entity);

        /// <summary>
        /// Updates an existing identity.
        /// </summary>
        /// <param name="entity">The entity to update.</param>
        void Update([NotNull] TEntity entity);

        /// <summary>
        /// Asynchronously updates an existing identity.
        /// </summary>
        /// <param name="entity">The entity to update.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        Task UpdateAsync([NotNull] TEntity entity);

        /// <summary>
        /// Either <c>Insert()</c> or <c>Update()</c> the given entity,
        /// depending upon the value of its identifier property.
        /// </summary>
        /// <param name="entity">A transient instance containing new or updated
        /// state.</param>
        void InsertOrUpdate([NotNull] TEntity entity);

        /// <summary>
        /// Either <c>InsertAsync()</c> or <c>UpdateAsync()</c> the given
        /// entity, depending upon the value of its identifier property.
        /// </summary>
        /// <param name="entity">A transient instance containing new or updated
        /// state.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        Task InsertOrUpdateAsync([NotNull] TEntity entity);

        /// <summary>
        /// Removes an entity from the datastore.
        /// </summary>
        /// <param name="entity">The entity to remove.</param>
        void Delete([NotNull] TEntity entity);

        /// <summary>
        /// Asynchronously removes an entity from the datastore.
        /// </summary>
        /// <param name="entity">The entity to remove.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        Task DeleteAsync([NotNull] TEntity entity);

        /// <summary>
        /// Removes the entity with the given primary key.
        /// </summary>
        /// <param name="id">The primary key of the entity to remove.</param>
        void Delete([NotNull] TKey id);

        /// <summary>
        /// Asynchronoulsy removes the entity with the given primary key.
        /// </summary>
        /// <param name="id">The primary key of the entity to remove.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        Task DeleteAsync([NotNull] TKey id);

        /// <summary>
        /// Removes all the entities that satisfies a condition.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        void DeleteWhere([NotNull] Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Asynchronously removes all the entities that satisfies a condition.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        Task DeleteWhereAsync([NotNull] Expression<Func<TEntity, bool>> predicate);

    }
}