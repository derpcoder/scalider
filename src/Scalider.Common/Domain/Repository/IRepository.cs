﻿#region # using statements #

using System.Threading.Tasks;

#endregion

namespace Scalider.Domain.Repository
{
    
    /// <summary>
    /// Represents the most basic functionality of a repository.
    /// </summary>
    public interface IRepository
    {

        /// <summary>
        /// Gets the count of all entities in this repository.
        /// </summary>
        /// <returns>
        /// The number of entities in this repository.
        /// </returns>
        int Count();

        /// <summary>
        /// Asynchronously gets the count of all entities in this repository.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        Task<int> CountAsync();

        /// <summary>
        /// Gets the count of all entities in this repository.
        /// </summary>
        /// <returns>
        /// The number of entities in this repository.
        /// </returns>
        /// <remarks>
        /// Use this method when the return value is expected to be greater
        /// than <see cref="int.MaxValue"/>.
        /// </remarks>
        long LongCount();

        /// <summary>
        /// Asynchronously gets the count of all entities in this repository.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        /// <remarks>
        /// Use this method when the return value is expected to be greater
        /// than <see cref="int.MaxValue"/>.
        /// </remarks>
        Task<long> LongCountAsync();

    }
}