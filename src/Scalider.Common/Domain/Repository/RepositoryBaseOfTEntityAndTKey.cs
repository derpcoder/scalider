﻿#region # using statements #

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Scalider.Domain.Entities;

#endregion

namespace Scalider.Domain.Repository
{

    /// <summary>
    /// Provides a base implementation of the
    /// <see cref="IRepository{TEntity,TKey}"/> interface.
    /// </summary>
    /// <typeparam name="TEntity">The type encapsulating the type of the
    /// entity.</typeparam>
    /// <typeparam name="TKey">The type encapsulating the primary key of the
    /// entity.</typeparam>
    public abstract class RepositoryBase<TEntity, TKey> : IRepository<TEntity, TKey>
        where TEntity : class, IEntity<TKey>
        where TKey : IEquatable<TKey>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="RepositoryBase{TEntity,TKey}"/> class.
        /// </summary>
        protected RepositoryBase()
        {
        }

        #region # Methods #

        #region == Protected ==

        /// <summary>
        /// Creates an equality lambda expression for the entity primary key.
        /// </summary>
        /// <param name="id">The value of the primary key to create expression
        /// for.</param>
        /// <returns>
        /// An equality lambda expression for the entity primary key.
        /// </returns>
        protected virtual Expression<Func<TEntity, bool>> CreateExpressionForId(
            TKey id)
        {
            var lambdaParam = Expression.Parameter(typeof(TEntity));
            var lambdaBody =
                Expression.Equal(Expression.PropertyOrField(lambdaParam, "Id"),
                    Expression.Constant(id, typeof(TKey)));

            return Expression.Lambda<Func<TEntity, bool>>(lambdaBody, lambdaParam);
        }

        #endregion

        #endregion

        #region # IRepository #

        /// <summary>
        /// Gets the count of all entities in this repository.
        /// </summary>
        /// <returns>
        /// The number of entities in this repository.
        /// </returns>
        public abstract int Count();

        /// <summary>
        /// Asynchronously gets the count of all entities in this repository.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        public virtual Task<int> CountAsync() => Task.FromResult(Count());

        /// <summary>
        /// Gets the count of all entities in this repository.
        /// </summary>
        /// <returns>
        /// The number of entities in this repository.
        /// </returns>
        /// <remarks>
        /// Use this method when the return value is expected to be greater
        /// than <see cref="int.MaxValue"/>.
        /// </remarks>
        public abstract long LongCount();

        /// <summary>
        /// Asynchronously gets the count of all entities in this repository.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        /// <remarks>
        /// Use this method when the return value is expected to be greater
        /// than <see cref="int.MaxValue"/>.
        /// </remarks>
        public virtual Task<long> LongCountAsync() => Task.FromResult(LongCount());

        #endregion

        #region # IRepository<TEntity,TKey> #

        /// <summary>
        /// Gets the count of all entities in this repository.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The number of entities in this repository.
        /// </returns>
        public abstract int Count(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Asynchronously gets the count of all entities in this repository.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        public virtual Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate)
        {
            Check.NotNull(predicate, nameof(predicate));
            return Task.FromResult(Count(predicate));
        }

        /// <summary>
        /// Gets the count of all entities in this repository.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The number of entities in this repository.
        /// </returns>
        /// <remarks>
        /// Use this method when the return value is expected to be greater
        /// than <see cref="int.MaxValue"/>.
        /// </remarks>
        public abstract long LongCount(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Asynchronously gets the count of all entities in this repository.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        /// <remarks>
        /// Use this method when the return value is expected to be greater
        /// than <see cref="int.MaxValue"/>.
        /// </remarks>
        public virtual Task<long> LongCountAsync(
            Expression<Func<TEntity, bool>> predicate)
        {
            Check.NotNull(predicate, nameof(predicate));
            return Task.FromResult(LongCount(predicate));
        }

        /// <summary>
        /// Returns the entity with the given primary key or <c>null</c> if no
        /// entity with the given primary key is found.
        /// </summary>
        /// <param name="id">The primary key of the entity to retrieve.</param>
        /// <returns>
        /// The entity matching the primary key or <c>null</c> if no entity is
        /// found.
        /// </returns>
        public virtual TEntity Get(TKey id)
            =>
            EqualityComparer<TKey>.Default.Equals(id, default(TKey))
                ? default(TEntity)
                : FirstOrDefault(CreateExpressionForId(id));

        /// <summary>
        /// Asynchronously returns the entity with the given primary key or
        /// <c>null</c> if no entity with the given primary key is found.
        /// </summary>
        /// <param name="id">The primary key of the entity to retrieve.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        public virtual Task<TEntity> GetAsync(TKey id)
            =>
            EqualityComparer<TKey>.Default.Equals(id, default(TKey))
                ? Task.FromResult(default(TEntity))
                : FirstOrDefaultAsync(CreateExpressionForId(id));

        /// <summary>
        /// Returns all the entities for this repository.
        /// </summary>
        /// <returns>
        /// A collection containing all the entities for this repository.
        /// </returns>
        public abstract IEnumerable<TEntity> GetAll();

        /// <summary>
        /// Asynchronously returns all the entities for this repository.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        public virtual Task<IEnumerable<TEntity>> GetAllAsync()
            => Task.FromResult(GetAll());

        /// <summary>
        /// Returns collection containing all the entities that satisfies a
        /// condition.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The collection containing all the entities that satisfies the
        /// condition.
        /// </returns>
        public abstract IEnumerable<TEntity> Find(
            Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Asynchronously returns collection containing all the entities that
        /// satisfies a condition.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        public virtual Task<IEnumerable<TEntity>> FindAsync(
            Expression<Func<TEntity, bool>> predicate)
        {
            Check.NotNull(predicate, nameof(predicate));
            return Task.FromResult(Find(predicate));
        }

        /// <summary>
        /// Returns the only entity that satisfies a condition or throws an
        /// exception if there is more than exactly one entity that satisfies
        /// the condition.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The single entity that satisfies the condition.
        /// </returns>
        public abstract TEntity Single(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Asynchronously returns the only entity that satisfies a condition
        /// or throws an exception if there is more than exactly one entity
        /// that satisfies the condition.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        public virtual Task<TEntity> SingleAsync(
            Expression<Func<TEntity, bool>> predicate)
        {
            Check.NotNull(predicate, nameof(predicate));

            return Task.FromResult(Single(predicate));
        }

        /// <summary>
        /// Returns the first entity that satisfies a condition or <c>null</c>
        /// if no such entity is found.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The entity that satisfies the condition or <c>null</c> if no such
        /// entity is found.
        /// </returns>
        public abstract TEntity FirstOrDefault(
            Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Asynchronously returns the first entity that satisfies a
        /// condition or <c>null</c> if no such entity is found.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        public virtual Task<TEntity> FirstOrDefaultAsync(
            Expression<Func<TEntity, bool>> predicate)
        {
            Check.NotNull(predicate, nameof(predicate));
            return Task.FromResult(FirstOrDefault(predicate));
        }

        /// <summary>
        /// Inserts a new entity.
        /// </summary>
        /// <param name="entity">The entity to insert.</param>
        public abstract void Insert(TEntity entity);

        /// <summary>
        /// Asynchronously inserts a new entity.
        /// </summary>
        /// <param name="entity">The entity to insert.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        public virtual Task InsertAsync(TEntity entity)
        {
            Check.NotNull(entity, nameof(entity));

            Insert(entity);
            return Task.FromResult(0);
        }

        /// <summary>
        /// Updates an existing identity.
        /// </summary>
        /// <param name="entity">The entity to update.</param>
        public abstract void Update(TEntity entity);

        /// <summary>
        /// Asynchronously updates an existing identity.
        /// </summary>
        /// <param name="entity">The entity to update.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        public virtual Task UpdateAsync(TEntity entity)
        {
            Check.NotNull(entity, nameof(entity));

            Update(entity);
            return Task.FromResult(0);
        }

        /// <summary>
        /// Either <c>Insert()</c> or <c>Update()</c> the given entity,
        /// depending upon the value of its identifier property.
        /// </summary>
        /// <param name="entity">A transient instance containing new or updated
        /// state.</param>
        public virtual void InsertOrUpdate(TEntity entity)
        {
            Check.NotNull(entity, nameof(entity));

            if (entity.IsTransient())
                Insert(entity);
            else
                Update(entity);
        }

        /// <summary>
        /// Either <c>InsertAsync()</c> or <c>UpdateAsync()</c> the given
        /// entity, depending upon the value of its identifier property.
        /// </summary>
        /// <param name="entity">A transient instance containing new or updated
        /// state.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        public virtual Task InsertOrUpdateAsync(TEntity entity)
        {
            Check.NotNull(entity, nameof(entity));
            return entity.IsTransient() ? InsertAsync(entity) : UpdateAsync(entity);
        }

        /// <summary>
        /// Removes an entity from the datastore.
        /// </summary>
        /// <param name="entity">The entity to remove.</param>
        public abstract void Delete(TEntity entity);

        /// <summary>
        /// Asynchronously removes an entity from the datastore.
        /// </summary>
        /// <param name="entity">The entity to remove.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        public virtual Task DeleteAsync(TEntity entity)
        {
            Check.NotNull(entity, nameof(entity));

            Delete(entity);
            return Task.FromResult(0);
        }

        /// <summary>
        /// Removes the entity with the given primary key.
        /// </summary>
        /// <param name="id">The primary key of the entity to remove.</param>
        public virtual void Delete(TKey id)
        {
            if (EqualityComparer<TKey>.Default.Equals(id, default(TKey)))
                return;

            var entity = Get(id);
            if (entity != null)
                Delete(entity);
        }

        /// <summary>
        /// Asynchronoulsy removes the entity with the given primary key.
        /// </summary>
        /// <param name="id">The primary key of the entity to remove.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        public virtual Task DeleteAsync(TKey id)
        {
            if (EqualityComparer<TKey>.Default.Equals(id, default(TKey)))
                return Task.FromResult(0);

            Delete(id);
            return Task.FromResult(0);
        }

        /// <summary>
        /// Removes all the entities that satisfies a condition.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        public virtual void DeleteWhere(Expression<Func<TEntity, bool>> predicate)
        {
            Check.NotNull(predicate, nameof(predicate));

            var matching = Find(predicate);
            foreach (var entity in matching)
                Delete(entity);
        }

        /// <summary>
        /// Asynchronously removes all the entities that satisfies a condition.
        /// </summary>
        /// <param name="predicate">A function to test each element for a
        /// condition.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        public virtual Task DeleteWhereAsync(
            Expression<Func<TEntity, bool>> predicate)
        {
            Check.NotNull(predicate, nameof(predicate));

            DeleteWhere(predicate);
            return Task.FromResult(0);
        }

        #endregion
    }
}