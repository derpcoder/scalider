﻿#region # using statements #

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;

#endregion

namespace Scalider.Domain.Entities
{

    /// <summary>
    /// Represents a basic implementation of the <see cref="IEntity{TKey}"/>.
    /// </summary>
    /// <typeparam name="TKey">The type encapsulating the primary key of the
    /// entity.</typeparam>
    public abstract class Entity<TKey> : IEntity<TKey>
        where TKey : IEquatable<TKey>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="Entity{TKey}"/> clsas.
        /// </summary>
        protected Entity()
        {
        }

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        public override string ToString() => $"[{GetType().Name} {Id}]";

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>
        /// A hash code for the current object.
        /// </returns>
        [SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
        public override int GetHashCode() => Id.GetHashCode();

        /// <summary>
        /// Determines whether the specified object is equal to the current
        /// object.
        /// </summary>
        /// <returns>
        /// true if the specified object  is equal to the current object;
        /// otherwise, false.
        /// </returns>
        /// <param name="obj">The object to compare with the current object.</param>
        public override bool Equals(object obj)
        {
            if (!(obj is Entity<TKey>))
                return false;

            // Same instances must be  considered as equal
            if (ReferenceEquals(this, obj))
                return true;

            // Transient objects are not considered as equal
            var other = (Entity<TKey>)obj;
            if (IsTransient() && other.IsTransient())
                return false;

            // Must have a IS-A relation of type or must be the same type
            var typeOfThis = GetType().GetTypeInfo();
            var typeOfOther = other.GetType().GetTypeInfo();

            if (!typeOfThis.IsAssignableFrom(typeOfOther) &&
                !typeOfOther.IsAssignableFrom(typeOfThis))
                return false;

            // Done
            return Id.Equals(other.Id);
        }

        #endregion

        #region == Public ==

        /// <summary>
        /// Indicates whether the values of two specified
        /// <see cref="Entity{TKey}" /> objects are equal.
        /// </summary>
        /// <param name="left">The first object to compare.</param>
        /// <param name="right">The second object to compare.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="left" /> and
        /// <paramref name="right" /> are equal; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator ==(Entity<TKey> left, Entity<TKey> right)
            => Equals(left, null) ? Equals(right, null) : left.Equals(right);

        /// <summary>
        /// Indicates whether the values of two specified
        /// <see cref="Entity{TKey}" /> objects are not equal.
        /// </summary>
        /// <param name="left">The first object to compare.</param>
        /// <param name="right">The second object to compare.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="left" /> and
        /// <paramref name="right" /> are not equal; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator !=(Entity<TKey> left, Entity<TKey> right)
            => !(left == right);

        #endregion

        #endregion

        #region # IEntity<TKey> #

        /// <summary>
        /// Gets a value indicating the unique identifier for this entity.
        /// </summary>
        public virtual TKey Id { get; protected set; }

        /// <summary>
        /// Determines whether the entity is a transient entity (the value for
        /// unique identifier of the entity has not been assigned).
        /// </summary>
        /// <returns>
        /// true if the value for unique identifier of the entity has been
        /// assigned or is the default value; otherwise, false.
        /// </returns>
        public virtual bool IsTransient()
        {
            if (EqualityComparer<TKey>.Default.Equals(Id, default(TKey)))
                return true;

            // Workaround for EF Core
            if (typeof(TKey) == typeof(int))
                return Convert.ToInt32(Id) <= 0;
            if (typeof(TKey) == typeof(long))
                return Convert.ToInt64(Id) <= 0;

            // The entity is not transient
            return false;
        }

        #endregion
    }
}