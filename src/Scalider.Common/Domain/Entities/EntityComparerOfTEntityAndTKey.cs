﻿#region # using statements #

using System;
using System.Collections.Generic;

#endregion

namespace Scalider.Domain.Entities
{

    /// <summary>
    /// Represents a <see cref="IEqualityComparer{T}"/> used to compare
    /// <see cref="IEntity{TKey}"/> using the entity's
    /// <see cref="object.Equals(object)"/>.
    /// </summary>
    /// <typeparam name="TEntity">The type encapsulating the type of the
    /// entity.</typeparam>
    /// <typeparam name="TKey">The type encapsulating the primary key of the
    /// entity.</typeparam>
    public class EntityComparer<TEntity, TKey> : IEqualityComparer<TEntity>
        where TEntity : Entity<TKey>
        where TKey : IEquatable<TKey>
    {
        #region # Variables #

        private static EntityComparer<TEntity, TKey> _defaultInstance;

        #endregion

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="EntityComparer{TEntity,TKey}"/> class.
        /// </summary>
        public EntityComparer()
        {
        }

        #region # Properties #

        #region == Public ==

        /// <summary>
        /// Gets the default singleton instance for this comparer.
        /// </summary>
        public static EntityComparer<TEntity, TKey> Default
            =>
                _defaultInstance ??
                (_defaultInstance = new EntityComparer<TEntity, TKey>());

        #endregion

        #endregion

        #region # IEqualityComparer<TEntity> #

        /// <summary>
        /// Determines whether the specified objects are equal.
        /// </summary>
        /// <param name="x">The first object of type
        /// <typeparamref name="TEntity" /> to compare.</param>
        /// <param name="y">The second object of type
        /// <typeparamref name="TEntity" /> to compare.</param>
        /// <returns>
        /// <c>true</c> if the specified objects are equal; otherwise,
        /// <c>false</c>.
        /// </returns>
        public bool Equals(TEntity x, TEntity y)
            => object.Equals(x, null) ? object.Equals(y, null) : x.Equals(y);

        /// <summary>
        /// Returns a hash code for the specified object.
        /// </summary>
        /// <param name="obj">The <see cref="Object" /> for which a hash code
        /// is to be returned.</param>
        /// <returns>
        /// A hash code for the specified object.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// The type of <paramref name="obj" /> is a reference type and
        /// <paramref name="obj" /> is null.
        /// </exception>
        public virtual int GetHashCode(TEntity obj)
        {
            Check.NotNull(obj, nameof(obj));
            return obj.GetHashCode();
        }

        #endregion
    }
}