﻿#region # using statements #

using System;

#endregion

namespace Scalider.Domain.Entities
{

    /// <summary>
    /// Used to standarize auditing entities.
    /// </summary>
    public interface IAuditable
    {

        /// <summary>
        /// Gets a value indicating the date and time when the entity was
        /// created.
        /// </summary>
        DateTimeOffset CreatedAt { get; }

        /// <summary>
        /// Gets or sets a value indicating the date and time when the entity
        /// was last updated.
        /// </summary>
        DateTimeOffset? UpdatedAt { get; set; }

    }
}