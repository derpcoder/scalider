﻿namespace Scalider.Domain.Entities
{

    /// <summary>
    /// Used to standarize soft deleting entities.
    /// </summary>
    /// <remarks>
    /// Soft-deleted entities are not actually deleted, they are marked as
    /// deleted by updating <see cref="IsDeleted"/> in the data store.
    /// 
    /// An entity marked as deleted should not visible to the application.
    /// </remarks>
    public interface ISoftDelete
    {

        /// <summary>
        /// Gets or sets a value indicating whether the entity has been marked
        /// as deleted.
        /// </summary>
        bool IsDeleted { get; set; }

    }
}