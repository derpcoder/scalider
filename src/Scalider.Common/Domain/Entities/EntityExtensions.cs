﻿namespace Scalider.Domain.Entities
{

    /// <summary>
    /// Extension methods for entities.
    /// </summary>
    public static class EntityExtensions
    {

        /// <summary>
        /// Determines whether the given <paramref name="entity"/> is null or
        /// has been marked as deleted.
        /// </summary>
        /// <param name="entity">The entity to validate.</param>
        /// <returns>
        /// true if the <paramref name="entity"/> is null or has been marked as
        /// deleted; otherwise, false.
        /// </returns>
        public static bool IsNullOrDeleted(this ISoftDelete entity)
            => entity == null || entity.IsDeleted;

    }
}