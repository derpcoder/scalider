﻿#region # using statements #

using System;

#endregion

namespace Scalider.Domain.Entities
{

    /// <summary>
    /// Defines the basic functionality of an entity with identifier of
    /// <typeparamref name="TKey"/>.
    /// </summary>
    /// <typeparam name="TKey">The type encapsulating the primary key of the
    /// entity.</typeparam>
    public interface IEntity<out TKey>
        where TKey : IEquatable<TKey>
    {

        /// <summary>
        /// Gets a value indicating the unique identifier for this entity.
        /// </summary>
        TKey Id { get; }

        /// <summary>
        /// Determines whether the entity is a transient entity (the value for
        /// unique identifier of the entity has not been assigned).
        /// </summary>
        /// <returns>
        /// true if the value for unique identifier of the entity has been
        /// assigned or is the default value; otherwise, false.
        /// </returns>
        bool IsTransient();

    }
}