﻿#region # using statements #

using System;
using System.Data;

#endregion

namespace Scalider.Domain.UnitOfWork
{

    /// <summary>
    /// Defines the basic functionality of a unit of work.
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {

        /// <summary>
        /// Gets the unique identifier of the unit of work.
        /// </summary>
        string Id { get; }

        /// <summary>
        /// Begins a transaction on the underlying store connection using the
        /// default <see cref="IsolationLevel"/>.
        /// </summary>
        /// <returns>
        /// A transaction object wrapping access to the underlying store's
        /// transaction object.
        /// </returns>
        void BeginTransaction();

        /// <summary>
        /// Begins a transaction on the underlying store connection using the
        /// specified isolation level.
        /// </summary>
        /// <param name="isolationLevel">The database isolation level with
        /// which the underlying store transaction will be created.</param>
        /// <returns>
        /// A transaction object wrapping access to the underlying store's
        /// transaction object.
        /// </returns>
        void BeginTransaction(IsolationLevel isolationLevel);

        /// <summary>
        /// Flush the associated database connection and end the unit of work.
        /// </summary>
        void Commit();

        /// <summary>
        /// Force the underlying transaction to roll back.
        /// </summary>
        void Rollback();

    }
}