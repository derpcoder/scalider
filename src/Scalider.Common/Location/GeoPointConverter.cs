﻿#region # using statements #

using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;

#endregion

namespace Scalider.Location
{

    /// <summary>
    /// Provides a type converter to convert <see cref="GeoPoint" /> objects to
    /// and from various other representations.
    /// </summary>
    public class GeoPointConverter : TypeConverter
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="GeoPointConverter"/>
        /// class.
        /// </summary>
        public GeoPointConverter()
        {
        }

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Returns whether this converter can convert an object of the given
        /// type to the type of this converter, using the specified context.
        /// </summary>
        /// <param name="context">An <see cref="ITypeDescriptorContext" /> that
        /// provides a format context. </param>
        /// <param name="sourceType">A <see cref="Type" /> that represents the
        /// type you want to convert from. </param>
        /// <returns>
        /// <c>true</c> if this converter can perform the conversion;
        /// otherwise, <c>false</c>.
        /// </returns>
        public override bool CanConvertFrom(ITypeDescriptorContext context,
            Type sourceType)
            =>
                sourceType == typeof(string) ||
                base.CanConvertFrom(context, sourceType);

        /// <summary>
        /// Returns whether this converter can convert the object to the
        /// specified type, using the specified context.
        /// </summary>
        /// <param name="context">An <see cref="ITypeDescriptorContext" /> that
        /// provides a format context. </param>
        /// <param name="destinationType">A <see cref="Type" /> that represents
        /// the type you want to convert to. </param>
        /// <returns>
        /// <c>true</c> if this converter can perform the conversion;
        /// otherwise, <c>false</c>.</returns>
        public override bool CanConvertTo(ITypeDescriptorContext context,
            Type destinationType)
            =>
                destinationType == typeof(string) ||
                base.CanConvertTo(context, destinationType);

        /// <summary>
        /// Converts the given object to the type of this converter, using the
        /// specified context and culture information.
        /// </summary>
        /// <param name="context">An <see cref="ITypeDescriptorContext" /> that
        /// provides a format context. </param>
        /// <param name="culture">The <see cref="CultureInfo" /> to use as the
        /// current culture. </param>
        /// <param name="value">The <see cref="object" /> to convert.</param>
        /// <returns>
        /// An <see cref="object" /> that represents the converted value.
        /// </returns>
        public override object ConvertFrom(ITypeDescriptorContext context,
            CultureInfo culture, object value)
        {
            // ReSharper disable once CanBeReplacedWithTryCastAndCheckForNull
            if (value is string)
                return GeoPoint.Parse((string)value);

            return base.ConvertFrom(context, culture, value);
        }

        /// <summary>
        /// Converts the given value object to the specified type, using the
        /// specified context and culture information.
        /// </summary>
        /// <param name="context">An <see cref="ITypeDescriptorContext" />
        /// that provides a format context.</param>
        /// <param name="culture">A <see cref="CultureInfo" />. If null is
        /// passed, the current culture is assumed.</param>
        /// <param name="value">The <see cref="object" /> to convert.</param>
        /// <param name="destinationType">The <see cref="Type" /> to convert
        /// the <paramref name="value" /> parameter to.</param>
        /// <returns>
        /// An <see cref="object" /> that represents the converted value.
        /// </returns>
        [SuppressMessage("ReSharper", "ConvertIfStatementToReturnStatement")]
        [SuppressMessage("ReSharper", "InvertIf")]
        public override object ConvertTo(ITypeDescriptorContext context,
            CultureInfo culture, object value, Type destinationType)
        {
            Check.NotNull(destinationType, nameof(destinationType));
            if (!(value is GeoPoint))
                return base.ConvertTo(context, culture, value, destinationType);

            var angle = (GeoPoint)value;
            if (destinationType == typeof(string))
            {
                var lat = angle.Latitude.Value.ToString(culture);
                var lng = angle.Longitude.Value.ToString(culture);

                return $"{lat},{lng}";
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }

        #endregion

        #endregion
    }
}