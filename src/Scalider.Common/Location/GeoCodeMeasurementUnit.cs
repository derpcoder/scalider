﻿namespace Scalider.Location
{
    
    /// <summary>
    /// Represents a measurement unit for distance.
    /// </summary>
    public enum GeoCodeMeasurementUnit : short
    {

        /// <summary>
        /// Represents nautical miles.
        /// </summary>
        NauticalMiles,

        /// <summary>
        /// Represents miles.
        /// </summary>
        Miles,

        /// <summary>
        /// Represents kilometers.
        /// </summary>
        Kilometers,

        /// <summary>
        /// Represents meters.
        /// </summary>
        Meters,

        /// <summary>
        /// Represent feet.
        /// </summary>
        Feet,

        /// <summary>
        /// Represents yards.
        /// </summary>
        Yards

    }
}