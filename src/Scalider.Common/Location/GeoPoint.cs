﻿#region # using statements #

using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using Newtonsoft.Json;
using Scalider.Json;

#endregion

namespace Scalider.Location
{

    /// <summary>
    /// Represents a geographic container that holds a latitude and longitude.
    /// </summary>
    [TypeConverter(typeof(GeoPointConverter)),
     JsonConverter(typeof(GeoPointJsonConverter))]
    public struct GeoPoint
    {

        #region # Variables #

        /// <summary>
        /// A read-only instance of the <see cref="GeoPoint"/> structure
        /// whose value is zero.
        /// </summary>
        public static readonly GeoPoint Empty = new GeoPoint();

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="GeoPoint"/> class.
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        public GeoPoint(double latitude, double longitude)
            : this(GeoAngle.FromDouble(latitude), GeoAngle.FromDouble(longitude))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GeoPoint"/> class.
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        public GeoPoint(GeoAngle latitude, GeoAngle longitude)
        {
            if (latitude.Value < -90.0 || latitude.Value > 90.0)
            {
                throw new ArgumentOutOfRangeException(nameof(latitude),
                    "The latitude degree must be between -90 and 90");
            }

            Latitude = latitude;
            Longitude = longitude;
        }

        #region # Properties #

        #region == Public ==

        /// <summary>
        /// Gets a <see cref="GeoAngle"/> indicating the latitude of the
        /// GeoPoint.
        /// </summary>
        public GeoAngle Latitude { get; }

        /// <summary>
        /// Gets a <see cref="GeoAngle"/> indicating the longitude of the
        /// GeoPoint.
        /// </summary>
        public GeoAngle Longitude { get; }

        #endregion

        #endregion

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        public override string ToString()
            => Latitude.ToString("NS") + ", " + Longitude.ToString("WE");

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer that is the hash code for this instance.
        /// </returns>
        [SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
        public override int GetHashCode()
        {
            var hash = 26;
            hash = hash * 40 + Latitude.GetHashCode();
            hash = hash * 40 + Longitude.GetHashCode();

            return hash;
        }

        /// <summary>
        /// Indicates whether this instance and a specified object are equal.
        /// </summary>
        /// <param name="obj">The object to compare with the current instance.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="obj" /> and this instance are the
        /// same type and represent the same value; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (!(obj is GeoPoint))
                return false;

            var other = (GeoPoint)obj;
            return Latitude == other.Latitude && Longitude == other.Longitude;
        }

        #endregion

        #region == Public ==

        /// <summary>
        /// Indicates whether the values of two specified
        /// <see cref="GeoPoint" /> objects are equal.
        /// </summary>
        /// <param name="left">The first object to compare.</param>
        /// <param name="right">The second object to compare.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="left" /> and
        /// <paramref name="right" /> are equal; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator ==(GeoPoint left, GeoPoint right)
            =>
                left.Latitude == right.Latitude && left.Longitude == right.Longitude;

        /// <summary>
        /// Indicates whether the values of two specified
        /// <see cref="GeoPoint" /> objects are not equal.
        /// </summary>
        /// <param name="left">The first object to compare.</param>
        /// <param name="right">The second object to compare.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="left" /> and
        /// <paramref name="right" /> are not equal; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator !=(GeoPoint left, GeoPoint right)
            => !(left == right);

        /// <summary>
        /// Converts the string representation of a GeoPoint to the
        /// equivalent <see cref="GeoPoint" /> structure.
        /// </summary>
        /// <param name="input">The string to convert.</param>
        /// <returns>
        /// A structure that contains the value that was parsed.
        /// </returns>
        public static GeoPoint Parse(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                throw new FormatException("Unexpected GeoPoint format");

            var parts = input.Split(',');
            if (parts.Length != 2)
                throw new FormatException("Unexpected GeoPoint format");

            var latitude = GeoAngle.Parse(parts[0]);
            var longitude = GeoAngle.Parse(parts[1]);

            return new GeoPoint(latitude, longitude);
        }

        /// <summary>
        /// Converts the string representation of a GeoPoint to the
        /// equivalent <see cref="GeoPoint" /> structure.
        /// </summary>
        /// <param name="input">The string to convert.</param>
        /// <param name="result">The structure that will contain the parsed
        /// value. If the method returns <c>true</c>,
        /// <paramref name="result" /> contains a valid
        /// <see cref="GeoPoint"/>.
        /// 
        /// If the method returns <c>false</c>, <paramref name="result" />
        /// equals <see cref="Empty" />.</param>
        /// <returns></returns>
        public static bool TryParse(string input, out GeoPoint result)
        {
            result = default(GeoPoint);
            if (string.IsNullOrWhiteSpace(input))
                return false;

            try
            {
                result = Parse(input);
                return true;
            }
            catch
            {
                // ignore
            }

            return false;
        }

        /// <summary>
        /// Gets the Earth radius for the given
        /// <paramref name="measurementUnit"/>.
        /// </summary>
        /// <param name="measurementUnit">The measurement unit.</param>
        /// <returns>
        /// The Earth radius for the given <paramref name="measurementUnit"/>.
        /// </returns>
        public static double GetEarthRadius(GeoCodeMeasurementUnit measurementUnit)
        {
            switch (measurementUnit)
            {
                case GeoCodeMeasurementUnit.NauticalMiles:
                    return 3440;
                case GeoCodeMeasurementUnit.Miles:
                    return 3959;
                case GeoCodeMeasurementUnit.Kilometers:
                    return 6371;
                case GeoCodeMeasurementUnit.Meters:
                    return 6371000;
                case GeoCodeMeasurementUnit.Feet:
                    return 20900000;
                case GeoCodeMeasurementUnit.Yards:
                    return 6967410.324;
                default:
                    throw new ArgumentOutOfRangeException(nameof(measurementUnit),
                        measurementUnit, null);
            }
        }

        /// <summary>
        /// Determines the approximate distance in miles between this
        /// GeoPoint and the given latitude and longitude.
        /// </summary>
        /// <param name="latitude">The destination latitude.</param>
        /// <param name="longitude">The destination longitude.</param>
        /// <returns>
        /// The approximate distance in miles.
        /// </returns>
        public double DistanceTo(double latitude, double longitude)
            => DistanceTo(new GeoPoint(latitude, longitude));

        /// <summary>
        /// Determines the approximate distance in the specified
        /// <paramref name="measurementUnit"/> between this GeoPoint and the
        /// given latitude and longitude.
        /// </summary>
        /// <param name="latitude">The destination latitude.</param>
        /// <param name="longitude">The destination longitude.</param>
        /// <param name="measurementUnit">The measurement unit.</param>
        /// <returns>
        /// The approximate distance in the specified
        /// <paramref name="measurementUnit"/> unit.
        /// </returns>
        public double DistanceTo(double latitude, double longitude,
            GeoCodeMeasurementUnit measurementUnit)
            => DistanceTo(new GeoPoint(latitude, longitude), measurementUnit);

        /// <summary>
        /// Determines the approximate distance in miles between this
        /// GeoPoint and the given GeoPoint.
        /// </summary>
        /// <param name="dest">The destination GeoPoint.</param>
        /// <returns>
        /// The approximate distance in miles.
        /// </returns>
        public double DistanceTo(GeoPoint dest)
            => DistanceTo(dest, GeoCodeMeasurementUnit.Miles);

        /// <summary>
        /// Determines the approximate distance in the specified
        /// <paramref name="measurementUnit"/> unit between this GeoPoint and
        /// the given GeoPoint.
        /// </summary>
        /// <param name="dest">The destination GeoPoint.</param>
        /// <param name="measurementUnit">The measurement unit.</param>
        /// <returns>
        /// The approximate distance in the specified
        /// <paramref name="measurementUnit"/> unit.
        /// </returns>
        public double DistanceTo(GeoPoint dest,
            GeoCodeMeasurementUnit measurementUnit)
        {
            var centerLatitudeRadians = dest.Latitude.Radians;
            var latitudeRadians = Latitude.Radians;
            var centerLongitudeRadians = dest.Longitude.Radians;
            var longitudeRadians = Longitude.Radians;

            // Calculate distance
            var radius = GetEarthRadius(measurementUnit);
            return Math.Round(
                radius *
                Math.Acos(
                    Math.Cos(centerLatitudeRadians) *
                    Math.Cos(latitudeRadians) *
                    Math.Cos(longitudeRadians - centerLongitudeRadians) +
                    Math.Sin(centerLatitudeRadians) *
                    Math.Sin(latitudeRadians)
                ),
                6
            );
        }

        #endregion

        #endregion
    }
}