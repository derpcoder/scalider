﻿#region # using statements #

using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using Newtonsoft.Json;
using Scalider.Json;

#endregion

namespace Scalider.Location
{

    /// <summary>
    /// Represents a geographic container that holds a single coordinate point.
    /// 
    /// Based on http://stackoverflow.com/a/6862835/2411798
    /// </summary>
    [TypeConverter(typeof(GeoAngleConverter)),
     JsonConverter(typeof(GeoAngleJsonConverter))]
    public struct GeoAngle
    {
        #region # Variables #

        /// <summary>
        /// A read-only instance of the <see cref="GeoAngle"/> structure whose
        /// value is zero.
        /// </summary>
        public static readonly GeoAngle Empty = new GeoAngle();

        #endregion

        #region # Properties #

        #region == Public ==

        /// <summary>
        /// Gets a value indicating the decimal representation of this
        /// angle.
        /// </summary>
        public double Value { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the angle is positive.
        /// </summary>
        public bool IsNegative { get; private set; }

        /// <summary>
        /// Gets a value indicating the degrees for this angle.
        /// </summary>
        public int Degrees { get; private set; }

        /// <summary>
        /// Gets a value indicating the minutes for this angle.
        /// </summary>
        public int Minutes { get; private set; }

        /// <summary>
        /// Gets a value indicating the seconds for this angle.
        /// </summary>
        public int Seconds { get; private set; }

        /// <summary>
        /// Gets a value indicating the milliseconds for this angle.
        /// </summary>
        public int Milliseconds { get; private set; }

        #endregion

        #region == Internal ==

        /// <summary>
        /// Gets a value indicating the radians representation of this
        /// angle.
        /// </summary>
        internal double Radians => Math.PI * Value / 180.0;

        #endregion

        #endregion

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        public override string ToString()
        {
            var degrees = IsNegative ? -Degrees : Degrees;
            var millis = Milliseconds > 0 ? $".{Milliseconds:0}" : null;

            return $"{degrees}° {Minutes:0}' {Seconds:0}{millis}\"";
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer that is the hash code for this instance.
        /// </returns>
        [SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
        public override int GetHashCode()
        {
            var hash = 13;
            hash = hash * 20 + Degrees.GetHashCode();
            hash = hash * 20 + Minutes.GetHashCode();
            hash = hash * 20 + Seconds.GetHashCode();
            hash = hash * 20 + Milliseconds.GetHashCode();

            return hash;
        }

        /// <summary>
        /// Indicates whether this instance and a specified object are equal.
        /// </summary>
        /// <param name="obj">The object to compare with the current instance.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="obj" /> and this instance are the
        /// same type and represent the same value; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (!(obj is GeoAngle))
                return false;

            var other = (GeoAngle)obj;
            return Degrees == other.Degrees && Minutes == other.Minutes &&
                   Seconds == other.Seconds && Milliseconds == other.Milliseconds;
        }

        #endregion

        #region == Public ==

        /// <summary>
        /// Indicates whether the values of two specified
        /// <see cref="GeoAngle" /> objects are equal.
        /// </summary>
        /// <param name="left">The first object to compare.</param>
        /// <param name="right">The second object to compare.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="left" /> and
        /// <paramref name="right" /> are equal; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator ==(GeoAngle left, GeoAngle right)
            =>
                left.Degrees == right.Degrees && left.Minutes == right.Minutes &&
                left.Seconds == right.Seconds &&
                left.Milliseconds == right.Milliseconds;

        /// <summary>
        /// Indicates whether the values of two specified
        /// <see cref="GeoAngle" /> objects are not equal.
        /// </summary>
        /// <param name="left">The first object to compare.</param>
        /// <param name="right">The second object to compare.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="left" /> and
        /// <paramref name="right" /> are not equal; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator !=(GeoAngle left, GeoAngle right)
            => !(left == right);

        /// <summary>
        /// Converts the decimal representation of a GeoAngle to the equivalent
        /// <see cref="GeoAngle"/> structure.
        /// </summary>
        /// <param name="angleInDegrees">The decimal value to convert.</param>
        /// <returns>
        /// A structure that contains the value that was parsed.
        /// </returns>
        public static GeoAngle FromDouble(double angleInDegrees)
        {
            if (angleInDegrees < -180 || angleInDegrees > 180)
            {
                throw new ArgumentOutOfRangeException(nameof(angleInDegrees),
                    "The angle degree must be between -180 and 180");
            }

            var result = new GeoAngle
            {
                Value = angleInDegrees,
                IsNegative = angleInDegrees < 0
            };

            // Switch the value to positive
            angleInDegrees = Math.Round(angleInDegrees, 6);
            angleInDegrees = Math.Abs(angleInDegrees);

            // Gets the degree
            result.Degrees = (int)Math.Floor(angleInDegrees);
            var delta = angleInDegrees - result.Degrees;

            // Gets minutes and seconds
            var seconds = (int)Math.Floor(3600.0 * delta);

            result.Seconds = seconds % 60;
            result.Minutes = (int)Math.Floor(seconds / 60.0);
            delta = delta * 3600.0 - seconds;

            // Gets fractions
            result.Milliseconds = (int)(10000.0 * delta);

            // Done
            return result;
        }

        /// <summary>
        /// Converts the string representation of a GeoAngle to the equivalent
        /// <see cref="GeoAngle" /> structure.
        /// </summary>
        /// <param name="input">The string to convert.</param>
        /// <returns>
        /// A structure that contains the value that was parsed.
        /// </returns>
        public static GeoAngle Parse(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                throw new FormatException("Unrecognized GeoAngle format.");

            input = input.Trim();

            // Determine if the given input is a double
            double dbl;
            if (double.TryParse(input, out dbl))
                return FromDouble(dbl);

            // Parce geo angle
            var result = ParseExact(input);
            if (result.HasValue)
                return result.Value;

            // Unable to parse
            throw new FormatException("Unrecognized GeoAngle format.");
        }

        /// <summary>
        /// Converts the string representation of a GeoAngle to the equivalent
        /// <see cref="GeoAngle" /> structure.
        /// </summary>
        /// <param name="input">The string to convert.</param>
        /// <param name="result">The structure that will contain the parsed
        /// value. If the method returns <c>true</c>,
        /// <paramref name="result" /> contains a valid <see cref="GeoAngle"/>.
        /// 
        /// If the method returns <c>false</c>, <paramref name="result" />
        /// equals <see cref="Empty" />.</param>
        /// <returns></returns>
        public static bool TryParse(string input, out GeoAngle result)
        {
            result = default(GeoAngle);
            if (string.IsNullOrWhiteSpace(input))
                return false;

            try
            {
                result = Parse(input);
                return true;
            }
            catch
            {
                // ignore
            }

            return false;
        }

        /// <summary>
        /// Formats the value of the current instance using the specified format.
        /// </summary>
        /// <param name="format">The format to use.-or- A null reference
        /// (Nothing in Visual Basic) to use the default format.</param>
        /// <returns>
        /// The value of the current instance in the specified format.
        /// </returns>
        public string ToString(string format)
        {
            if (string.IsNullOrWhiteSpace(format))
                return ToString();

            var millis = Milliseconds > 0 ? $".{Milliseconds:0}" : null;
            switch (format)
            {
                case "NS":
                    var sn = IsNegative ? 'S' : 'N';
                    return
                        $"{Degrees}° {Minutes:0}' {Seconds:0}{millis}\" {sn}";
                case "WE":
                    var we = IsNegative ? 'W' : 'E';
                    return
                        $"{Degrees}° {Minutes:0}' {Seconds:0}{millis}\" {we}";
                default:
                    throw new FormatException();
            }
        }

        #endregion

        #region == Private ==

        [SuppressMessage("ReSharper", "InvertIf")]
        private static GeoAngle? ParseExact(string input)
        {
            input = input.Replace(" ", "");
            var isNegative = false;

            // Retrieve degrees
            var indexOfDegrees = input.IndexOf('°');
            var degrees = 0;

            if (indexOfDegrees != -1)
            {
                var degreesStr = input.Substring(0, indexOfDegrees);
                input = input.Substring(indexOfDegrees + 1);

                var hasValidDegrees = int.TryParse(degreesStr, out degrees) &&
                                      degrees >= -180 && degrees <= 180;

                if (hasValidDegrees && degrees < 0)
                {
                    isNegative = true;
                    degrees = Math.Abs(degrees);
                }
                else if (!hasValidDegrees)
                {
                    throw new FormatException(
                        "The angle degree must be between -180 and 180");
                }
            }

            // Retrieve minutes
            var indexOfMinutes = input.IndexOf('\'');
            var minutes = 0;

            if (indexOfMinutes != -1)
            {
                var minutesStr = input.Substring(0, input.IndexOf('\''));
                input = input.Substring(indexOfMinutes + 1);

                var hasValidMinutes = int.TryParse(minutesStr, out minutes) &&
                                      minutes >= 0 && minutes <= 59;

                if (!hasValidMinutes)
                {
                    throw new FormatException(
                        "The angle minutes must be between 0 and 59");
                }
            }

            // Retrieve seconds and milliseconds
            var indexOfSeconds = input.IndexOf('"', Math.Max(0, indexOfMinutes));
            var seconds = 0;
            var milliseconds = 0;

            if (indexOfSeconds != -1)
            {
                bool hasValidSeconds;
                var secondsStr = input.Substring(0, input.IndexOf('"'));
                input = input.Substring(indexOfSeconds + 1);

                var indexOfMilliseconds = secondsStr.IndexOf('.');
                if (indexOfMilliseconds != -1)
                {
                    double fullSeconds;
                    hasValidSeconds = double.TryParse(secondsStr, out fullSeconds) &&
                                      fullSeconds >= 0 && fullSeconds < 60;

                    if (hasValidSeconds)
                    {
                        seconds = (int)Math.Floor(fullSeconds);
                        var delta = Math.Round(fullSeconds - seconds, 4);

                        milliseconds = (int)(10000.0 * delta);
                    }
                }
                else
                {
                    hasValidSeconds = int.TryParse(secondsStr, out seconds) &&
                                      seconds >= 0 && seconds <= 59;
                }

                if (!hasValidSeconds)
                {
                    throw new FormatException(
                        "The angle seconds must be between 0 and 59");
                }
            }

            // Retrieve position character
            if (input.Length > 0)
            {
                var chr = input[0];
                if (input.Length > 1)
                    return null;

                // ReSharper disable once ConvertIfStatementToSwitchStatement
                if (chr == 'N' || chr == 'E')
                    isNegative = false;
                else if (chr == 'S' || chr == 'W')
                    isNegative = true;
                else
                    return null; // Invalid character
            }

            // Calculate value
            double value;
            {
                var realMinutes = minutes / 60.0;
                var realMilliseconds = milliseconds / 10000.0;
                var realSeconds = (seconds + realMilliseconds) / 3600.0;

                value = Math.Round(degrees + realMinutes + realSeconds, 6);
            }

            // Done
            return new GeoAngle
            {
                IsNegative = isNegative,
                Value = value,
                Degrees = degrees,
                Minutes = minutes,
                Seconds = seconds,
                Milliseconds = milliseconds
            };
        }

        #endregion

        #endregion
    }
}