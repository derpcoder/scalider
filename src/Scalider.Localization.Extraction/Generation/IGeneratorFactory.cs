﻿#region # using statements #

using System.IO;
using System.Text;

#endregion

namespace Scalider.Localization.Extraction.Generation
{

    public interface IGeneratorFactory
    {

        string FileExtension { get; }

        Generator Create(Stream output, Encoding encoding);

    }

}