﻿#region # using statements #

using System;
using Scalider.Localization.Extraction.Analysis.Keyword;

#endregion

namespace Scalider.Localization.Extraction.Generation
{
    public abstract class Generator : IDisposable
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="Generator"/> class.
        /// </summary>
        protected Generator()
        {
        }

        #region # Methods #

        #region == Public ==

        public virtual void WriteHeader()
        {
        }

        public abstract void WriteEntry(KeywordMatchGroup match);

        public virtual void WriteFooter()
        {
        }

        public abstract void Flush();

        #endregion

        #region == Protected ==

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }

        #endregion

        #endregion

        #region # IDisposable #

        /// <summary>
        /// Performs application-defined tasks associated with freeing,
        /// releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

    }
}