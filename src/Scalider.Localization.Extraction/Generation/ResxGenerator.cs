﻿#region # using statements #

using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using Scalider.Localization.Extraction.Analysis.Keyword;

#endregion

namespace Scalider.Localization.Extraction.Generation
{

    internal class ResxGenerator : Generator
    {

        #region # Variables #

        private readonly ConcurrentBag<string> _keysBag;

        private XmlWriter _writer;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ResxGenerator"/>
        /// class.
        /// </summary>
        /// <param name="output"></param>
        /// <param name="encoding"></param>
        public ResxGenerator(Stream output, Encoding encoding)
        {
            _writer = XmlWriter.Create(output,
                new XmlWriterSettings {Encoding = encoding, Indent = true});
            _keysBag = new ConcurrentBag<string>();
        }

        #region # Methods #

        #region == Overrides ==

        public override void WriteHeader()
        {
            _writer.WriteStartElement("root");

            WriteResHeader("resmimetype", "text/microsoft-resx");
            WriteResHeader("version", "2.0");
            WriteResHeader("reader",
                "System.Resources.ResXResourceReader, System.Windows.Forms, " +
                "Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
            WriteResHeader("writer",
                "System.Resources.ResXResourceWriter, System.Windows.Forms," +
                "Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
        }

        public override void WriteEntry(KeywordMatchGroup match)
        {
            if (_keysBag.Contains(match.Text, StringComparer.OrdinalIgnoreCase))
                return;

            _keysBag.Add(match.Text);
            _writer.WriteStartElement("data");
            _writer.WriteAttributeString("name", match.Text);
            _writer.WriteAttributeString("xml", "space", "", "preserve");

            _writer.WriteStartElement("value");
            _writer.WriteEndElement();

            _writer.WriteEndElement();
        }

        public override void WriteFooter()
        {
            _writer.WriteEndElement();
        }

        public override void Flush()
        {
            _writer.Flush();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_writer != null)
                {
                    _writer.Flush();
                    _writer.Dispose();
                    _writer = null;
                }
            }

            base.Dispose(disposing);
        }

        #endregion

        #region == Private ==

        private void WriteResHeader(string name, string value)
        {
            _writer.WriteStartElement("resheader");
            _writer.WriteAttributeString("name", name);

            _writer.WriteStartElement("value");
            _writer.WriteString(value);
            _writer.WriteEndElement();

            _writer.WriteEndElement();
        }

        #endregion

        #endregion

    }

}