﻿#region # using statements #

using System;
using System.IO;
using System.Text;
using Scalider.Localization.Extraction.Analysis.Keyword;

#endregion

namespace Scalider.Localization.Extraction.Generation
{

    internal class PotGenerator : Generator
    {

        #region # Variables #

        private TextWriter _writer;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PotGenerator"/> class.
        /// </summary>
        /// <param name="output"></param>
        /// <param name="encoding"></param>
        public PotGenerator(Stream output, Encoding encoding)
        {
            _writer = new StreamWriter(output, encoding);
        }

        #region # Methods #

        #region == Overrides ==

        public override void WriteHeader()
        {
            _writer.WriteLine(@"# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid """"
msgstr """"
""Project-Id-Version: PACKAGE VERSION\n""
""Report-Msgid-Bugs-To: MAINTAINER <EMAIL@ADRESS>\n""");

            _writer.WriteLine(@"""POT-Creation-Date: {0:yyyy-MM-dd HH:mmK}\n""",
                DateTime.Now);

            _writer.WriteLine(@"""PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n""
""Last-Translator: FULL NAME <EMAIL@ADDRESS>\n""
""Language-Team: LANGUAGE <LL@li.org>\n""
""Language: \n""
""MIME-Version: 1.0\n""
""Content-Type: text/plain; charset=UTF-8\n""
""Content-Transfer-Encoding: 8bit\n""
");
        }

        public override void WriteEntry(KeywordMatchGroup match)
        {
            foreach (var source in match.Sources)
            {
                _writer.WriteLine("#: {0}:{1}", source.FileName,
                    source.LineNumber + 1);
            }

            if (!string.IsNullOrWhiteSpace(match.Context))
                _writer.WriteLine("msgctxt \"{0}\"", match.Context);

            _writer.WriteLine("msgid \"{0}\"", match.Text);
            if (!string.IsNullOrWhiteSpace(match.PluralText))
            {
                _writer.WriteLine("msgid_plural \"{0}\"", match.PluralText);
                _writer.WriteLine("msgstr[0] \"\"");
                _writer.WriteLine("msgstr[1] \"\"");
            }
            else
                _writer.WriteLine("msgstr \"\"");

            _writer.WriteLine();
        }

        public override void Flush()
        {
            _writer.Flush();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_writer != null)
                {
                    _writer.Dispose();
                    _writer = null;
                }
            }

            base.Dispose(disposing);
        }

        #endregion

        #endregion

    }
}