﻿#region # using statements #

using System.IO;
using System.Text;

#endregion

namespace Scalider.Localization.Extraction.Generation
{

    internal class PotGeneratorFactory : IGeneratorFactory
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="PotGeneratorFactory"/>
        /// class.
        /// </summary>
        public PotGeneratorFactory()
        {
        }

        #region # IGeneratorFactory #

        public string FileExtension => "pot";

        public Generator Create(Stream output, Encoding encoding) =>
            new PotGenerator(output, encoding);

        #endregion

    }

}