﻿#region # using statements #

using System.IO;
using System.Text;

#endregion

namespace Scalider.Localization.Extraction.Generation
{

    internal class ResxGeneratorFactory : IGeneratorFactory
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="ResxGeneratorFactory"/> class.
        /// </summary>
        public ResxGeneratorFactory()
        {
        }

        #region # IGeneratorFactory #

        public string FileExtension => "resx";

        public Generator Create(Stream output, Encoding encoding) =>
            new ResxGenerator(output, encoding);

        #endregion

    }

}