﻿#region # using statements #

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Mono.Options;
using Scalider.Localization.Extraction.Generation;

#endregion

namespace Scalider.Localization.Extraction
{

    internal class ExtractorOptions
    {

        #region # Variables #

        private string _helpText;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtractorOptions"/>
        /// class.
        /// </summary>
        private ExtractorOptions()
        {
        }

        #region # Properties #

        #region == Public ==

        public string[] Input { get; private set; }

        public string OutputPath { get; private set; }

        public IGeneratorFactory GeneratorFactory { get; private set; }

        public bool IncludeValidationMessages { get; private set; }

        public bool ShowHelp { get; private set; }

        #endregion

        #endregion

        #region # Methods #

        #region == Public ==

        public static ExtractorOptions Parse(string[] args)
        {
            var result = new ExtractorOptions();
            var input = new List<string>();
            var p = new OptionSet
            {
                // Output
                {
                    "o=|output=",
                    "Specify the output path",
                    v =>
                    {
                        result.OutputPath = Path.GetFullPath(v);
                    }
                },

                // Generator factory
                {
                    "g|generator=",
                    "Specify the output generator",
                    v =>
                    {
                        if ("pot".Equals(v, StringComparison.OrdinalIgnoreCase))
                            result.GeneratorFactory = new PotGeneratorFactory();
                        else if ("resx".Equals(v, StringComparison.OrdinalIgnoreCase))
                            result.GeneratorFactory = new ResxGeneratorFactory();
                        else
                        {
                            throw new OptionException(
                                $"The value {v} is not a valid type of generator",
                                "generator"
                            );
                        }
                    }
                },

                // Include validation messages
                {
                    "m|validation-messages",
                    "Include the default validation messages.",
                    v =>
                    {
                        result.IncludeValidationMessages = v != null;
                    }
                },

                // Show help
                {
                    "h|help",
                    "Show this message and exit",
                    v => result.ShowHelp = v != null
                },

                // Default
                {
                    "<>",
                    v =>
                    {
                        var path = Path.GetFullPath(v);
                        if (!File.Exists(path) && !Directory.Exists(path))
                        {
                            throw new OptionException(
                                $"The input path '{v}' is not a valid file or directory",
                                "<>"
                            );
                        }

                        input.Add(Path.GetFullPath(v));
                    }
                }
            };

            try
            {
                p.Parse(args);
                if (input.Count == 0)
                    input.Add(Path.GetFullPath("./"));
                if (result.GeneratorFactory == null)
                    result.GeneratorFactory = new PotGeneratorFactory();

                result.Input = input.ToArray();
                using (var writer = new StringWriter())
                {
                    p.WriteOptionDescriptions(writer);
                    result._helpText = writer.ToString();
                }

                return result;
            }
            catch (OptionException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Try `--help` for more information");
            }

            return null;
        }

        public void WriteHelp()
        {
            var exeName = Assembly.GetEntryAssembly().Location;
            exeName = Path.GetFileName(exeName);

            Console.WriteLine($"Usage: {exeName} input [OPTIONS]");
            Console.WriteLine(
                "Extracts all the localization messages from a path or a project."
            );
            Console.WriteLine(
                "If no input is specified, the current path is used as input.");
            Console.WriteLine();

            Console.WriteLine("Options:");
            Console.WriteLine(_helpText);
        }

        #endregion

        #endregion

    }
}