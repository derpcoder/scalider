﻿#region # using statements #

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Razor.Language;
using Microsoft.AspNetCore.Razor.Language.Legacy;

//using Microsoft.AspNetCore.Razor.Parser.Internal;
//using Microsoft.AspNetCore.Razor.Parser.SyntaxTree;
//using Microsoft.AspNetCore.Razor.Tokenizer.Symbols;
using Scalider.Localization.Extraction.Analysis.Keyword;
using Scalider.Localization.Extraction.Analysis.Result;

#endregion

namespace Scalider.Localization.Extraction.Analysis
{

    internal class CshtmlAnalyzer : IAnalyzer
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="CshtmlAnalyzer"/>
        /// class.
        /// </summary>
        public CshtmlAnalyzer()
        {
        }

        #region # Methods #

        #region == Private ==

        private static CSharpSymbol ReadSymbol(IReadOnlyList<ISymbol> symbols,
            ref int index, bool ignoreWhitespace = true, bool ignoreComments = true)
        {
            while (symbols.Count > index)
            {
                var symbol = (CSharpSymbol)symbols[index++];
                if (ignoreWhitespace && symbol.Type == CSharpSymbolType.WhiteSpace)
                    continue;
                if (ignoreComments && symbol.Type == CSharpSymbolType.Comment)
                    continue;

                return symbol;
            }

            return null;
        }

        private static IReadOnlyList<CSharpSymbol> ReadArguments(
            IReadOnlyList<ISymbol> symbols, ref int index,
            CSharpSymbolType closeSymbolType)
        {
            var result = new List<CSharpSymbol>();

            var depth = 0;
            while (true)
            {
                var symbol = ReadSymbol(symbols, ref index);
                if (symbol == null)
                    return null;

                if (depth == 0 && (symbol.Type == CSharpSymbolType.Comma ||
                                   symbol.Type == closeSymbolType))
                {
                    index--;
                    return result.AsReadOnly();
                }

                // ReSharper disable once SwitchStatementMissingSomeCases
                switch (symbol.Type)
                {
                    case CSharpSymbolType.LeftBrace:
                    case CSharpSymbolType.LeftBracket:
                    case CSharpSymbolType.LeftParenthesis:
                        depth++;
                        break;
                    case CSharpSymbolType.RightBrace:
                    case CSharpSymbolType.RightBracket:
                    case CSharpSymbolType.RightParenthesis:
                        depth--;
                        break;
                }

                result.Add(symbol);
            }
        }

        private static Invocation CreateInvocation(string name,
            IReadOnlyList<Argument> arguments, SourceInfo? source,
            CSharpSymbolType closeSymbolType)
        {
            if (closeSymbolType != CSharpSymbolType.RightBracket)
                return new Invocation(name, arguments, source);

            if (arguments.Count <= 1 ||
                string.IsNullOrWhiteSpace(arguments[0].Value))
                return null;

            return new MatchedInvocation(name, arguments,
                new KeywordMatch(arguments[0].Value, null, null, source),
                source);
        }

        private static Invocation PeekCode(IReadOnlyList<ISymbol> symbols, int index,
            string fileName, int lineIndex, int characterIndex)
        {
            var nameSymbol = ReadSymbol(symbols, ref index, false, false);
            if (nameSymbol == null || nameSymbol.Type != CSharpSymbolType.Identifier)
                return null;

            // Determine the closing symbol type
            CSharpSymbolType closeSymbolType;
            var openSymbol = ReadSymbol(symbols, ref index);

            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (openSymbol?.Type)
            {
                case CSharpSymbolType.LeftParenthesis:
                    closeSymbolType = CSharpSymbolType.RightParenthesis;
                    break;
                case CSharpSymbolType.LeftBracket:
                    closeSymbolType = CSharpSymbolType.RightBracket;
                    break;
                default:
                    return null;
            }

            //
            lineIndex += nameSymbol.Start.LineIndex;
            characterIndex = nameSymbol.Start.LineIndex == 0
                ? characterIndex + nameSymbol.Start.CharacterIndex
                : nameSymbol.Start.CharacterIndex;

            var sourceInfo = new SourceInfo(fileName, lineIndex, characterIndex);

            // Read all the arguments
            var arguments = new List<Argument>();
            while (true)
            {
                var result = ReadArguments(symbols, ref index, closeSymbolType);
                if (result == null)
                    continue;

                if (result.Count <= 0)
                {
                    var closeSymbol = ReadSymbol(symbols, ref index);
                    if (closeSymbol == null)
                        return null;

                    return CreateInvocation(nameSymbol.Content, arguments,
                        sourceInfo, closeSymbolType);
                }

                var argValue = new StringBuilder();
                var isValidArgument = true;

                foreach (var symbol in result)
                {
                    // ReSharper disable once SwitchStatementMissingSomeCases
                    switch (symbol.Type)
                    {
                        case CSharpSymbolType.Plus:
                        case CSharpSymbolType.NewLine:
                            continue;
                        case CSharpSymbolType.StringLiteral:
                            var str = symbol.Content;
                            if (str.StartsWith("@\""))
                                str = str.Substring(2, str.Length - 3);
                            else if (str.StartsWith("\""))
                                str = str.Substring(1, str.Length - 2);
                            else
                                return null;

                            argValue.Append(str);
                            break;
                        default:
                            isValidArgument = false;
                            break;
                    }
                }

                arguments.Add(isValidArgument
                    ? new Argument(null, argValue.ToString())
                    : new Argument());

                // Determine if the call was closed
                var commaOrCloseSymbol = ReadSymbol(symbols, ref index);
                if (commaOrCloseSymbol == null)
                    return null;

                if (commaOrCloseSymbol.Type == closeSymbolType)
                {
                    return CreateInvocation(nameSymbol.Content, arguments,
                        sourceInfo, closeSymbolType);
                }
                if (commaOrCloseSymbol.Type == CSharpSymbolType.Comma)
                    continue;

                return null;
            }
        }

        #endregion

        #endregion

        #region # IAnalyzer #

        public string SupportedExtension => ".cshtml";

        public IEnumerable<Invocation> GetInvocations(string code,
            string fileName = null)
        {
            var result = new List<Invocation>();
//            var parser = new RazorParser(new CSharpCodeParser(),
//                new HtmlMarkupParser(), null);

            var parserResult =
                RazorSyntaxTree.Parse(RazorSourceDocument.Create(code, fileName));
            parserResult.Source.Lines.

            using (var reader = new StringReader(code))
            {
//                var parserResult = RazorSyntaxTree.Parse(
//                    RazorSourceDocument.ReadFrom(
//                        reader, fileName
//                    )
//                );
                
                var parserResult = parser.Parse(reader);
                var codeSpans = parserResult.Document
                                            .Flatten()
                                            .Where(s => s.Kind == SpanKind.Code);

                foreach (var codeSpan in codeSpans)
                {
                    var symbols = codeSpan.Symbols;
                    var span = codeSpan;

                    result.AddRange(symbols
                        .Select((t, i) => PeekCode(symbols, i, fileName,
                            span.Start.LineIndex,
                            span.Start.CharacterIndex)));
                }
            }

            // Done
            return result.AsReadOnly();
        }

        #endregion

    }
}