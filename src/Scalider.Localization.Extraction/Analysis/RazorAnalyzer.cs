﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Razor.Language;
using Scalider.Localization.Extraction.Analysis.Result;

namespace Scalider.Localization.Extraction.Analysis
{
    internal class RazorAnalyzer : IAnalyzer
    {

        public string SupportedExtension => ".cshtml";

        public IEnumerable<Invocation> GetInvocations(string code, string fileName = null)
        {
            var parsedDocument = RazorSyntaxTree.Parse(
                RazorSourceDocument.Create(code, fileName),
                RazorParserOptions.CreateDefault()
            );

            throw new System.NotImplementedException();
        }

    }
}