﻿#region # using statements #

using Scalider.Localization.Extraction.Analysis.Result;

#endregion

namespace Scalider.Localization.Extraction.Analysis.Keyword
{

    internal class KeywordMatch : KeywordMatchBase
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="KeywordMatch"/> class.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="pluralText"></param>
        /// <param name="context"></param>
        /// <param name="source"></param>
        public KeywordMatch(string text, string pluralText, string context,
            SourceInfo? source)
            : base(text, pluralText, context)
        {
            Source = source;
        }

        #region # Properties #

        #region == Public ==

        public SourceInfo? Source { get; }

        #endregion

        #endregion

    }

}