﻿#region # using statements #

using System;

#endregion

namespace Scalider.Localization.Extraction.Analysis.Keyword
{

    public abstract class KeywordMatchBase
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="KeywordMatchBase"/>
        /// class.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="pluralText"></param>
        /// <param name="context"></param>
        internal KeywordMatchBase(string text, string pluralText, string context)
        {
            Text = text ?? throw new ArgumentNullException(nameof(text));
            PluralText = pluralText;
            Context = context;
        }

        #region # Properties #

        #region == Public ==

        public string Text { get; }

        public string PluralText { get; }

        public string Context { get; }

        #endregion

        #endregion

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>
        /// A hash code for the current object.
        /// </returns>
        public override int GetHashCode()
        {
            return Text.GetHashCode() ^ (PluralText?.GetHashCode() ?? 0) ^
                   (Context?.GetHashCode() ?? 0);
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current
        /// object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>
        /// true if the specified object  is equal to the current object;
        /// otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            var other = obj as KeywordMatchBase;
            if (other == null)
                return false;

            return string.Equals(Text, other.Text, StringComparison.Ordinal) &&
                   string.Equals(PluralText, other.PluralText,
                       StringComparison.Ordinal) &&
                   string.Equals(Context, other.Context, StringComparison.Ordinal);
        }

        #endregion

        #endregion

    }
}