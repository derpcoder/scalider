﻿#region # using statements #

using System.Collections.Generic;
using Scalider.Localization.Extraction.Analysis.Result;

#endregion

namespace Scalider.Localization.Extraction.Analysis.Keyword
{

    public class KeywordMatchGroup : KeywordMatchBase
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="KeywordMatchGroup"/>
        /// class.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="pluralText"></param>
        /// <param name="context"></param>
        /// <param name="sources"></param>
        internal KeywordMatchGroup(string text, string pluralText, string context,
            IEnumerable<SourceInfo> sources = null)
            : base(text, pluralText, context)
        {
            Sources = sources;
        }

        #region # Properties #

        #region == Public ==

        public IEnumerable<SourceInfo> Sources { get; }

        #endregion

        #endregion

    }
}