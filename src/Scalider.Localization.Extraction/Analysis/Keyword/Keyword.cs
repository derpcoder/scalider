﻿#region # using statements #

using System;
using Scalider.Localization.Extraction.Analysis.Result;

#endregion

namespace Scalider.Localization.Extraction.Analysis.Keyword
{

    internal class Keyword
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="Keyword"/> class.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="text"></param>
        /// <param name="pluralText"></param>
        /// <param name="context"></param>
        protected Keyword(string id, int text, int? pluralText = null,
            int? context = null)
        {
            if (text < 0)
                throw new ArgumentOutOfRangeException(nameof(text));
            if (pluralText != null && pluralText < 0)
                throw new ArgumentOutOfRangeException(nameof(pluralText));
            if (context != null && context < 0)
                throw new ArgumentOutOfRangeException(nameof(context));

            Id = id ?? throw new ArgumentNullException(nameof(id));
            Text = text;
            PluralText = pluralText;
            Context = context;

            RequiredArguments = Math.Max(text,
                Math.Max(PluralText ?? 0, Context ?? 0));
        }

        #region # Properties #

        #region == Public ==

        public string Id { get; }

        public int Text { get; }

        public int? PluralText { get; }

        public int? Context { get; }

        public int RequiredArguments { get; }

        #endregion

        #endregion

        #region # Methods #

        #region == Public ==

        public static Keyword Parse(string keyword)
        {
            if (string.IsNullOrWhiteSpace(keyword))
                throw new ArgumentNullException(nameof(keyword));

            var colonIndex = keyword.IndexOf(':');
            if (colonIndex == 1)
            {
                // Only the name of the method was provided
                return new Keyword(keyword, 1);
            }

            // Initialize variables
            var id = keyword.Substring(0, colonIndex);
            int? text = null;
            int? pluralText = null;
            int? context = null;

            // Retrieve arguments
            var args = keyword.Substring(colonIndex + 1).Split(new[] {' ', ','},
                StringSplitOptions.RemoveEmptyEntries);

            foreach (var arg in args)
            {
                if (arg.EndsWith("c", StringComparison.OrdinalIgnoreCase))
                {
                    var num = int.Parse(arg.Substring(0, arg.Length - 1));
                    if (context == null)
                        context = num;
                    else
                    {
                        throw new InvalidOperationException(
                            $"The context is already defined for keyword '{keyword}'"
                        );
                    }
                }
                else
                {
                    var num = int.Parse(arg);
                    if (text == null)
                        text = num;
                    else if (pluralText == null)
                        pluralText = num;
                    else
                    {
                        throw new InvalidOperationException(
                            "Both singular and plural forms are already defined " +
                            $"for keyword '{keyword}'"
                        );
                    }
                }
            }

            // Done
            return new Keyword(id, text ?? 1, pluralText, context);
        }

        public virtual KeywordMatch Match(Invocation call)
        {
            var matchedCall = call as MatchedInvocation;
            if (matchedCall != null)
                return matchedCall.Match;
            if (Id != call.Name || call.Arguments.Count < RequiredArguments)
                return null;

            var text = call.Arguments[Text - 1];
            var pluralText = PluralText == null
                ? new Argument()
                : call.Arguments[PluralText.Value - 1];
            var context = Context == null
                ? new Argument()
                : call.Arguments[Context.Value - 1];

            if (string.IsNullOrWhiteSpace(text.Value) ||
                PluralText != null && string.IsNullOrWhiteSpace(pluralText.Value) ||
                Context != null && string.IsNullOrWhiteSpace(context.Value))
                return null;

            return new KeywordMatch(text.Value, pluralText.Value, context.Value,
                call.SourceInfo);
        }

        #endregion

        #endregion

    }
}