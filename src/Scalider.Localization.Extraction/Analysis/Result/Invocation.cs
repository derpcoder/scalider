﻿#region # using statements #

using System;
using System.Collections.Generic;

#endregion

namespace Scalider.Localization.Extraction.Analysis.Result
{
    internal class Invocation
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="Invocation"/> class.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="arguments"></param>
        /// <param name="source"></param>
        public Invocation(string name, IReadOnlyList<Argument> arguments,
            SourceInfo? source = null)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Arguments = arguments ??
                        throw new ArgumentNullException(nameof(arguments));
            SourceInfo = source;
        }

        #region # Properties #

        #region == Public ==

        public string Name { get; }

        public IReadOnlyList<Argument> Arguments { get; }

        public SourceInfo? SourceInfo { get; }

        #endregion

        #endregion

    }
}