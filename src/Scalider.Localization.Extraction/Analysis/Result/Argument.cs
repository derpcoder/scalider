﻿#region # using statements #

using System.Text;

#endregion

namespace Scalider.Localization.Extraction.Analysis.Result
{

    internal struct Argument
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="Argument"/> class.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public Argument(string name, string value)
        {
            Name = name;
            Value = value;
        }

        #region # Properties #

        #region == Public ==

        public string Name { get; }

        public string Value { get; }

        #endregion

        #endregion

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Returns the fully qualified type name of this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="string" /> containing a fully qualified type name.
        /// </returns>
        public override string ToString()
        {
            var builder = new StringBuilder();
            if (Name != null)
            {
                builder.Append(Name);
                builder.Append(": ");
            }

            if (Value != null)
            {
                builder.Append("\"");
                builder.Append(Value);
                builder.Append("\"");
            }
            else
                builder.Append("<unsupported>");

            return builder.ToString();
        }

        #endregion

        #endregion

    }

}