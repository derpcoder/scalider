﻿#region # using statements #

using System;
using System.Collections.Generic;
using Scalider.Localization.Extraction.Analysis.Keyword;

#endregion

namespace Scalider.Localization.Extraction.Analysis.Result
{

    internal class MatchedInvocation : Invocation
    {

        /// <summary>
        /// Initializes a new instance of the <see cref=""/>
        /// </summary>
        /// <param name="name"></param>
        /// <param name="arguments"></param>
        /// <param name="match"></param>
        /// <param name="source"></param>
        public MatchedInvocation(string name, IReadOnlyList<Argument> arguments,
            KeywordMatch match, SourceInfo? source)
            : base(name, arguments, source)
        {
            Match = match ?? throw new ArgumentNullException(nameof(match));
        }

        #region # Properties #

        #region == Public ==

        public KeywordMatch Match { get; }

        #endregion

        #endregion

    }

}