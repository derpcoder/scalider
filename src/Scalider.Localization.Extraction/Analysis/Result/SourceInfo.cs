﻿#region # using statements #

using System.Text;

#endregion

namespace Scalider.Localization.Extraction.Analysis.Result
{

    public struct SourceInfo
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceInfo"/> class.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="lineNumber"></param>
        /// <param name="characterIndex"></param>
        public SourceInfo(string fileName, int? lineNumber, int? characterIndex)
        {
            FileName = fileName;
            LineNumber = lineNumber;
            CharacterIndex = characterIndex;
        }

        #region # Properties #

        #region == Public ==

        public string FileName { get; }

        public int? LineNumber { get; }

        public int? CharacterIndex { get; }

        #endregion

        #endregion

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Returns the fully qualified type name of this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="string" /> containing a fully qualified type name.
        /// </returns>
        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append(FileName);

            if (LineNumber != null)
                builder.Append(':').Append(LineNumber.Value);
            if (CharacterIndex != null)
            {
                builder.Append(LineNumber != null ? ',' : ':')
                       .Append(CharacterIndex);
            }

            return builder.ToString();
        }

        #endregion

        #endregion

    }

}