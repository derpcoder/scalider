﻿#region # using statements #

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.MSBuild;
using Scalider.Localization.Extraction.Analysis.Keyword;
using Scalider.Localization.Extraction.Analysis.Result;

#endregion

namespace Scalider.Localization.Extraction.Analysis
{
    internal class CsprojAnalyzer : IAnalyzer
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="CsprojAnalyzer"/>
        /// class.
        /// </summary>
        public CsprojAnalyzer()
        {
        }

        #region # Methods #

        #region == Private ==

        private static string GetNodeNameFromExpression(ExpressionSyntax syntax)
        {
            if (syntax == null)
                return null;

            while (syntax.Kind() == SyntaxKind.AliasQualifiedName)
                syntax = ((AliasQualifiedNameSyntax)syntax).Name;

            while (syntax.Kind() == SyntaxKind.QualifiedName)
                syntax = ((QualifiedNameSyntax)syntax).Right;

            while (syntax.Kind() == SyntaxKind.SimpleMemberAccessExpression)
                syntax = ((MemberAccessExpressionSyntax)syntax).Name;

            var simpleName = syntax as SimpleNameSyntax;
            return simpleName?.Identifier.ValueText;
        }

        private static string GetExpressionValue(ExpressionSyntax syntax)
        {
            if (syntax == null)
                return null; // Nothing to do with a null expression, is there?

            // Remove the parenthesis expression
            while (syntax.Kind() == SyntaxKind.ParenthesizedExpression)
                syntax = ((ParenthesizedExpressionSyntax)syntax).Expression;

            // Remove the cast expression
            while (syntax.Kind() == SyntaxKind.CastExpression)
                syntax = ((CastExpressionSyntax)syntax).Expression;

            // Retrieve the value of a literal expression
            var literal = syntax as LiteralExpressionSyntax;
            if (literal != null)
            {
                return literal.Token.Kind() != SyntaxKind.StringLiteralToken
                    ? null
                    : literal.Token.ValueText;
            }

            // Retrieve the value of a interpolated string expression
            var interpolated = syntax as InterpolatedStringExpressionSyntax;
            if (interpolated != null)
            {
                var builder = new StringBuilder();
                foreach (var part in interpolated.Contents)
                {
                    if (part is InterpolatedStringTextSyntax)
                    {
                        var interString = part as InterpolatedStringTextSyntax;
                        builder.Append(interString.TextToken.ValueText);
                    }
                    else if (part is InterpolationSyntax)
                    {
                        var inter = part as InterpolationSyntax;
                        builder.Append(inter.OpenBraceToken.ValueText)
                               .Append(GetExpressionValue(inter.Expression));

                        // Append format (if any)
                        if (inter.FormatClause != null)
                        {
                            var frmt = inter.FormatClause;
                            builder.Append(frmt.ColonToken.ValueText)
                                   .Append(frmt.FormatStringToken.ValueText);
                        }

                        // Close brace
                        builder.Append(inter.CloseBraceToken.ValueText);
                    }
                }

                return builder.ToString();
            }

            // Retrieve the value of a binary expression
            var binary = syntax as BinaryExpressionSyntax;

            // ReSharper disable once InvertIf
            if (binary != null)
            {
                // ReSharper disable once SwitchStatementMissingSomeCases
                switch (binary.Kind())
                {
                    case SyntaxKind.AsExpression:
                        return GetExpressionValue(binary.Left);
                    case SyntaxKind.AddExpression:
                        var leftValue = GetExpressionValue(binary.Left);
                        var rightValue = GetExpressionValue(binary.Right);

                        if (binary.OperatorToken.ValueText == "+")
                            return leftValue + rightValue;
                        break;
                }
            }

            // Unable to retrieve expression value
            return null;
        }

        private static Argument GetArgument(ArgumentSyntax syntax)
        {
            return new Argument(
                syntax.NameColon?.Name.Identifier.ValueText,
                GetExpressionValue(syntax.Expression)
            );
        }

        private static IEnumerable<Invocation> AnalyzeTree(SyntaxTree syntaxTree,
            Compilation compilation)
        {
            var root = syntaxTree.GetCompilationUnitRoot();
            var result = new List<Invocation>();

            var semanticModel = compilation.GetSemanticModel(syntaxTree);

            result.AddRange(RetrieveAllInvocations(root));
            result.AddRange(RetrieveAllElementAccesses(root));
            result.AddRange(RetrieveAllAnnotatedClassMembers(root, semanticModel));

            return result.AsReadOnly();
        }

        private static IEnumerable<Invocation> RetrieveAllElementAccesses(
            SyntaxNode treeRoot)
        {
            var elementAccessNodes = treeRoot
                .DescendantNodes()
                .OfType<ElementAccessExpressionSyntax>()
                .ToArray();

            return from node in elementAccessNodes
                   let name = GetNodeNameFromExpression(node.Expression)
                   where !string.IsNullOrWhiteSpace(name)
                   let arguments = node
                       .ArgumentList.Arguments.Select(GetArgument).ToArray()
                   let location = node.GetLocation().GetLineSpan()
                   let source = new SourceInfo(location.Path,
                       location.StartLinePosition.Line,
                       location.StartLinePosition.Character)
                   where arguments.Length >= 1 &&
                         !string.IsNullOrWhiteSpace(arguments[0].Value)
                   select new MatchedInvocation(name, arguments,
                       new KeywordMatch(arguments[0].Value, null, null, source),
                       source);
        }

        private static IEnumerable<Invocation> RetrieveAllInvocations(
            SyntaxNode treeRoot)
        {
            var invocationNodes = treeRoot
                .DescendantNodes()
                .OfType<InvocationExpressionSyntax>()
                .ToArray();

            return from node in invocationNodes
                   let name = GetNodeNameFromExpression(node.Expression)
                   where !string.IsNullOrWhiteSpace(name)
                   let arguments = node
                       .ArgumentList.Arguments
                       .Select(GetArgument).ToArray()
                   let location = node.GetLocation().GetLineSpan()
                   let source = new SourceInfo(location.Path,
                       location.StartLinePosition.Line,
                       location.StartLinePosition.Character)
                   select new Invocation(name, arguments, source);
        }

        private static IEnumerable<Invocation> RetrieveAllAnnotatedClassMembers(
            SyntaxNode treeRoot, SemanticModel semanticModel)
        {
            var result = new List<Invocation>();
            var classDeclarationNodes = treeRoot
                .DescendantNodes()
                .OfType<ClassDeclarationSyntax>()
                .ToArray();

            foreach (var classNode in classDeclarationNodes)
            {
                var propertyNodes = classNode
                    .DescendantNodes()
                    .OfType<PropertyDeclarationSyntax>()
                    .Where(p => p.AttributeLists.Count > 0)
                    .ToArray();

                foreach (var prop in propertyNodes)
                    result.AddRange(RetrieveAllForProperty(prop, semanticModel));
            }

            return result;
        }

        private static IEnumerable<Invocation> RetrieveAllForProperty(
            PropertyDeclarationSyntax syntax, SemanticModel semanticModel)
        {
            var result = new List<Invocation>();
            var attributes = syntax.AttributeLists
                                   .SelectMany(a => a.Attributes)
                                   .ToArray();

            var validationAttributeType = typeof(ValidationAttribute);
            var displayAttributeType = typeof(DisplayAttribute);
            var displayNameAttributeType = typeof(DisplayNameAttribute);

            foreach (var attr in attributes)
            {
                var name = GetNodeNameFromExpression(attr.Name);
                if (string.IsNullOrWhiteSpace(name))
                    continue;

                var type = semanticModel.GetTypeInfo(attr);
                if (type.Type == null)
                    continue;

                var typeName = GetFullTypeName(type.Type);
                if (typeName.Equals(displayAttributeType.FullName,
                    StringComparison.Ordinal))
                {
                    // Found the Display attribute
                    var nameArg = GetNamedArgumentValue(attr.ArgumentList, "Name");
                    if (string.IsNullOrWhiteSpace(nameArg))
                        continue;

                    result.Add(GetInvocation(attr, nameArg));
                }
                else if (typeName.Equals(displayNameAttributeType.FullName,
                    StringComparison.Ordinal))
                {
                    // Found the DisplayName attribute
                    if (attr.ArgumentList == null ||
                        attr.ArgumentList.Arguments.Count == 0)
                        continue;

                    result.Add(GetInvocation(attr,
                        GetExpressionValue(attr
                            .ArgumentList.Arguments[0].Expression)));
                }
                else
                {
                    var baseType = type.Type.BaseType;
                    while (baseType != null)
                    {
                        var baseTypeName = GetFullTypeName(baseType);
                        if (baseTypeName.Equals(validationAttributeType.FullName,
                            StringComparison.Ordinal))
                        {
                            // Found an attribute that inherits the Validation
                            // attribute
                            var errorMessageArg =
                                GetNamedArgumentValue(attr.ArgumentList,
                                    "ErrorMessage");

                            if (!string.IsNullOrWhiteSpace(errorMessageArg))
                                result.Add(GetInvocation(attr, errorMessageArg));

                            break;
                        }

                        baseType = baseType.BaseType;
                    }
                }
            }

            // Append property or field name
            var propName = syntax.Identifier.ValueText;
            var propElement = result
                .FirstOrDefault(i => i.Arguments[0].Value == propName);

            if (propElement != null)
                return result;

            result.Add(GetInvocation(syntax, propName));

            // Done
            return result;
        }

        private static string GetNamedArgumentValue(
            AttributeArgumentListSyntax argumentList, string name)
        {
            if (argumentList == null || argumentList.Arguments.Count == 0)
                return null;

            return (from a in argumentList.Arguments
                    let argName = GetNodeNameFromExpression(a.NameEquals?.Name)
                    where !string.IsNullOrWhiteSpace(argName) &&
                          argName.Equals(name, StringComparison.Ordinal)
                    select GetExpressionValue(a.Expression))
                .FirstOrDefault();
        }

        private static Invocation GetInvocation(SyntaxNode syntax, string value)
        {
            var lineSpan = syntax.GetLocation().GetLineSpan().StartLinePosition;

            var prop = syntax as PropertyDeclarationSyntax;
            if (prop?.Modifiers.Count > 0)
            {
                lineSpan = prop.Modifiers[0].GetLocation().GetLineSpan()
                               .StartLinePosition;
            }

            var source = new SourceInfo(syntax.SyntaxTree.FilePath, lineSpan.Line,
                lineSpan.Character);

            return new MatchedInvocation("", new[] {new Argument(null, value)},
                new KeywordMatch(value, null, null, source), source);
        }

        private static string GetFullTypeName(ISymbol typeSymbol)
        {
            var namespaces = new List<string>();

            var containingNamespace = typeSymbol.ContainingNamespace;
            while (!containingNamespace.IsGlobalNamespace)
            {
                namespaces.Insert(0, containingNamespace.Name);
                containingNamespace = containingNamespace.ContainingNamespace;
            }

            return string.Join(".", namespaces) + "." + typeSymbol.Name;
        }

        #endregion

        #endregion

        #region # IAnalyzer #

        public string SupportedExtension => ".csproj";

        public IEnumerable<Invocation> GetInvocations(string code,
            string fileName = null)
        {
            var workspace = MSBuildWorkspace.Create()
                                            .OpenProjectAsync(fileName)
                                            .Result;

            var compilation = workspace.GetCompilationAsync().Result;

            var result = new List<Invocation>();
            foreach (var syntaxTree in compilation.SyntaxTrees)
                result.AddRange(AnalyzeTree(syntaxTree, compilation));

            return result;
        }

        #endregion

    }
}