﻿#region # using statements #

using System.Collections.Generic;
using Scalider.Localization.Extraction.Analysis.Result;

#endregion

namespace Scalider.Localization.Extraction.Analysis
{
    
    internal interface IAnalyzer
    {

        string SupportedExtension { get; }

        IEnumerable<Invocation> GetInvocations(string code, string fileName = null);

    }

}