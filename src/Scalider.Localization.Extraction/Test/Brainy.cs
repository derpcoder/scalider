﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Scalider.Localization.Extraction.Test
{
    public class Brainy
    {

        public Brainy()
        {
            var localizer = new Dictionary<string, string>();

//            var helloWorld = localizer.GetString("Hello world!");
            var hello = localizer["Hello"];

            var hello2 = Localizer["Hello"];
        }

        [Display(Name = "Name"), Required(ErrorMessage = "Gravy"), DisplayName("OPSS")]
        internal string Name { get; set; }

        public IDictionary<string, string> Localizer = new Dictionary<string, string>();

    }
}