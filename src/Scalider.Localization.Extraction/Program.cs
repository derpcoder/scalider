﻿#region # using statements #

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Scalider.Localization.Extraction.Analysis.Keyword;
using Scalider.Localization.Extraction.Analysis.Result;

#endregion

namespace Scalider.Localization.Extraction
{
    internal static partial class Program
    {

        public static void Main(string[] args)
        {
            var options = ExtractorOptions.Parse(args);
            if (options == null || options.ShowHelp || options.Input == null ||
                options.Input.Length == 0)
            {
                options?.WriteHelp();
                return;
            }

            // Retrieve all the invocations
            var allFiles = new List<string>();
            foreach (var path in options.Input)
                allFiles.AddRange(GetAllFilesUnderPathRecursively(path));

            var matches = new List<KeywordMatch>();
            if (options.IncludeValidationMessages)
            {
                matches = DataAnnotationErrorMessages
                    .Select(m => new KeywordMatch(m, null, null, null))
                    .ToList();
            }

            foreach (var file in allFiles)
            {
                var extension = Path.GetExtension(file);
                if (string.IsNullOrWhiteSpace(extension))
                    continue;

                var analyzer =
                    Analyzers.FirstOrDefault(a => extension.Equals(
                        a.SupportedExtension,
                        StringComparison.InvariantCultureIgnoreCase));

                if (analyzer != null)
                {
                    matches.AddRange(analyzer
                        .GetInvocations(File.ReadAllText(file), file)
                        .Where(i => i != null)
                        .Select(i => SupportedKeywords
                            .Select(k => k.Match(i)).FirstOrDefault(m => m != null))
                        .Where(m => m != null));
                }
            }

            // Group multiple matches
            var mappedMatches = new Dictionary<KeywordMatch, List<SourceInfo>>();
            foreach (var match in matches)
            {
                if (!mappedMatches.ContainsKey(match))
                    mappedMatches[match] = new List<SourceInfo>();

                if (match.Source != null)
                    mappedMatches[match].Add(match.Source.Value);
            }

            var groupedMatches =
                mappedMatches.Select(e => new KeywordMatchGroup(e.Key.Text,
                    e.Key.PluralText, e.Key.Context, e.Value));

            // Generate file
            var outputFile =
                Path.Combine(options.OutputPath ?? Path.GetFullPath("./"),
                    "generated." + options.GeneratorFactory.FileExtension);

            using (var file = File.OpenWrite(outputFile))
            {
                using (var generator =
                    options.GeneratorFactory.Create(file, Encoding.UTF8))
                {
                    generator.WriteHeader();
                    foreach (var match in groupedMatches)
                        generator.WriteEntry(match);

                    generator.WriteFooter();
                    generator.Flush();
                }
            }
        }

        private static IEnumerable<string>
            GetAllFilesUnderPathRecursively(string path)
        {
            if (File.Exists(path))
                return new[] {path};
            if (!Directory.Exists(path))
                return new string[0];

            var result = Directory.GetFiles(path).ToList();
            foreach (var entry in Directory.GetDirectories(path))
            {
                var newPath = Path.GetFullPath(entry);
                if (path.StartsWith(newPath))
                    continue;

                result.AddRange(GetAllFilesUnderPathRecursively(entry));
            }

            return result.AsReadOnly();
        }

    }
}