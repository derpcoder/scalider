﻿#region # using statements #

using System;
using log4net;
#if NETSTANDARD
using System.Reflection;
#endif
using Serilog.Core;
using Serilog.Events;

#endregion

namespace Scalider.Serilog.Sinks
{

    /// <summary>
    /// Provides an implementation of <see cref="ILogEventSink"/> that uses
    /// log4net to write events.
    /// </summary>
    internal class Log4NetSink : ILogEventSink
    {
        #region # Constants #

        private const string ContextMessage = "Serilog-Log4NetSink";

        #endregion

        #region # Variables #

        private readonly IFormatProvider _formatProvider;
        private readonly bool _supplyContextMessage;
        private readonly string _defaultLoggerName;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="Log4NetSink"/> class.
        /// </summary>
        /// <param name="formatProvider">The <see cref="IFormatProvider"/> to
        /// use when formatting messages.</param>
        /// <param name="supplyContextMessage">Whether to supply the context
        /// message to the logger.</param>
        /// <param name="defaultLoggerName">The default logger name.</param>
        public Log4NetSink(IFormatProvider formatProvider = null,
            bool supplyContextMessage = false,
            string defaultLoggerName = "Scalider")
        {
            Check.NotNullOrEmpty(defaultLoggerName, nameof(defaultLoggerName));

            _formatProvider = formatProvider;
            _supplyContextMessage = supplyContextMessage;
            _defaultLoggerName = defaultLoggerName;
        }

        #region # ILogEventSink #

        /// <summary>
        /// Emit the provided log event to the sink.
        /// </summary>
        /// <param name="logEvent">The log event to write.</param>
        public void Emit(LogEvent logEvent)
        {
            var loggerName = _defaultLoggerName;

            // Retrieve logger name
            LogEventPropertyValue sourceContext;
            if (logEvent.Properties.TryGetValue(Constants.SourceContextPropertyName,
                out sourceContext))
            {
                var sv = sourceContext as ScalarValue;
                if (sv?.Value is string)
                    loggerName = (string)sv.Value;
            }

            // Retrieve the message and exception to log
            var message = logEvent.RenderMessage(_formatProvider);
            var exception = logEvent.Exception;

            // Write log
#if NETSTANDARD
            var logger = LogManager.GetLogger(Assembly.GetEntryAssembly(), loggerName);
#else
            var logger = LogManager.GetLogger(loggerName);
#endif
            using (
                _supplyContextMessage
                    ? ThreadContext.Stacks["NDC"].Push(ContextMessage)
                    : new NullDisposable())
            {
                switch (logEvent.Level)
                {
                    case LogEventLevel.Verbose:
                    case LogEventLevel.Debug:
                        logger.Debug(message, exception);
                        break;
                    case LogEventLevel.Information:
                        logger.Info(message, exception);
                        break;
                    case LogEventLevel.Warning:
                        logger.Warn(message, exception);
                        break;
                    case LogEventLevel.Error:
                        logger.Error(message, exception);
                        break;
                    case LogEventLevel.Fatal:
                        logger.Fatal(message, exception);
                        break;
                }
            }
        }

        #endregion

        private class NullDisposable : IDisposable
        {

            /// <summary>
            /// Initializes a new instance of the <see cref="NullDisposable"/>
            /// class.
            /// </summary>
            public NullDisposable()
            {
            }

            #region # IDisposable #

            public void Dispose()
            {
            }

            #endregion
        }

    }
}