﻿#region # using statements #

using System;
using Scalider.Serilog.Sinks;
using Serilog;
using Serilog.Configuration;
using Serilog.Events;

#endregion

namespace Scalider.Serilog
{

    /// <summary>
    /// Extension methods for <see cref="LoggerSinkConfiguration"/>.
    /// </summary>
    public static class LoggerSinkConfigurationExtensions
    {

        /// <summary>
        /// Adds the log4net sink to the given
        /// <paramref name="loggerConfiguration"/>.
        /// </summary>
        /// <param name="loggerConfiguration">The
        /// <see cref="LoggerSinkConfiguration"/>.</param>
        /// <param name="restrictedMinimumLevel"></param>
        /// <param name="formatProvider">The <see cref="IFormatProvider"/> to
        /// use when formatting messages.</param>
        /// <param name="supplyContextMessage">Whether to supply the context
        /// message to the logger.</param>
        /// <param name="defaultLoggerName">The default logger name.</param>
        /// <returns>
        /// The <see cref="LoggerSinkConfiguration"/>.
        /// </returns>
        public static LoggerConfiguration Log4Net(
            this LoggerSinkConfiguration loggerConfiguration,
            LogEventLevel restrictedMinimumLevel = LevelAlias.Minimum,
            IFormatProvider formatProvider = null, bool supplyContextMessage = false,
            string defaultLoggerName = "Scalider")
        {
            Check.NotNull(loggerConfiguration, nameof(loggerConfiguration));
            Check.NotNullOrEmpty(defaultLoggerName, nameof(defaultLoggerName));

            return
                loggerConfiguration.Sink(
                    new Log4NetSink(formatProvider, supplyContextMessage,
                        defaultLoggerName), restrictedMinimumLevel);
        }

    }
}