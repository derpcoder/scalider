﻿#region # using statements #

using System;
using System.Data;
using MySql.Data.Types;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;

#endregion

namespace Scalider.NHibernate.Types
{

    /// <summary>
    /// Maps a <see cref="DateTimeOffset"/> property to a
    /// <see cref="MySqlDateTime"/>.
    /// </summary>
    public class DateTimeOffsetType : IUserType
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="DateTimeOffsetType"/> class.
        /// </summary>
        public DateTimeOffsetType()
        {
        }

        #region # IUserType #

        /// <summary>
        /// Compare two instances of the class mapped by this type for persistent "equality"
        /// ie. equality of persistent state
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public new bool Equals(object x, object y) => x?.Equals(y) ?? false;

        /// <summary>
        /// Get a hashcode for the instance, consistent with persistence "equality"
        /// </summary>
        public int GetHashCode(object x) => x.GetHashCode();

        /// <summary>
        /// Retrieve an instance of the mapped class from a JDBC resultset.
        /// Implementors should handle possibility of null values.
        /// </summary>
        /// <param name="rs">a IDataReader</param>
        /// <param name="names">column names</param>
        /// <param name="owner">the containing entity</param>
        /// <returns></returns>
        /// <exception cref="T:NHibernate.HibernateException">HibernateException</exception>
        public object NullSafeGet(IDataReader rs, string[] names, object owner)
        {
            var ordinal = rs.GetOrdinal(names[0]);
            if (rs.IsDBNull(ordinal))
                return null;

            // Determine if the value is null
            var rawValue = rs.GetValue(ordinal);
            if (rawValue == null)
                return null;

            // Convert to the corresponding type
            var value = (MySqlDateTime)rawValue;
            if (value.IsNull)
                return null;

            // Done
            var min = DateTimeOffset.MinValue;
            return new DateTimeOffset(Math.Max(value.Year, min.Year),
                Math.Max(value.Month, min.Month), Math.Max(value.Day, min.Day),
                value.Hour, value.Minute, value.Second, value.Millisecond,
                TimeSpan.FromHours(value.TimezoneOffset));
        }

        /// <summary>
        /// Write an instance of the mapped class to a prepared statement.
        /// Implementors should handle possibility of null values.
        /// A multi-column type should be written to parameters starting from index.
        /// </summary>
        /// <param name="cmd">a IDbCommand</param>
        /// <param name="value">the object to write</param>
        /// <param name="index">command parameter index</param>
        /// <exception cref="T:NHibernate.HibernateException">HibernateException</exception>
        public void NullSafeSet(IDbCommand cmd, object value, int index)
        {
            var param = cmd.Parameters[index] as IDataParameter;
            if (param == null)
            {
                throw new InvalidOperationException(
                    "The command parameter is not an IDataParameter");
            }

            if (value == null)
            {
                param.Value = DBNull.Value;
                return;
            }

            var dtOffset = (DateTimeOffset)value;
            dtOffset = dtOffset.ToUniversalTime();

            param.Value = $"{dtOffset:yyyy-MM-dd HH:mm:ss.ffffff}";
        }

        /// <summary>
        /// Return a deep copy of the persistent state, stopping at entities and at collections.
        /// </summary>
        /// <param name="value">generally a collection element or entity field</param>
        /// <returns>a copy</returns>
        public object DeepCopy(object value)
        {
            if (value == null)
                return null;

            var dtOffset = (DateTimeOffset)value;
            return new DateTimeOffset(dtOffset.Ticks, dtOffset.Offset);
        }

        /// <summary>
        /// During merge, replace the existing (<paramref name="target" />) value in the entity
        /// we are merging to with a new (<paramref name="original" />) value from the detached
        /// entity we are merging. For immutable objects, or null values, it is safe to simply
        /// return the first parameter. For mutable objects, it is safe to return a copy of the
        /// first parameter. For objects with component values, it might make sense to
        /// recursively replace component values.
        /// </summary>
        /// <param name="original">the value from the detached entity being merged</param>
        /// <param name="target">the value in the managed entity</param>
        /// <param name="owner">the managed entity</param>
        /// <returns>the value to be merged</returns>
        public object Replace(object original, object target, object owner)
            => original;

        /// <summary>
        /// Reconstruct an object from the cacheable representation. At the very least this
        /// method should perform a deep copy if the type is mutable. (optional operation)
        /// </summary>
        /// <param name="cached">the object to be cached</param>
        /// <param name="owner">the owner of the cached object</param>
        /// <returns>a reconstructed object from the cachable representation</returns>
        public object Assemble(object cached, object owner) => cached;

        /// <summary>
        /// Transform the object into its cacheable representation. At the very least this
        /// method should perform a deep copy if the type is mutable. That may not be enough
        /// for some implementations, however; for example, associations must be cached as
        /// identifier values. (optional operation)
        /// </summary>
        /// <param name="value">the object to be cached</param>
        /// <returns>
        /// a cacheable representation of the object.
        /// </returns>
        public object Disassemble(object value) => value;

        /// <summary>The SQL types for the columns mapped by this type.</summary>
        public SqlType[] SqlTypes => new[] {new SqlType(DbType.DateTimeOffset)};

        /// <summary>
        /// The type returned by <c>NullSafeGet()</c>.
        /// </summary>
        public Type ReturnedType => typeof(DateTimeOffset);

        /// <summary>
        /// Are objects of this type mutable?
        /// </summary>
        public bool IsMutable => false;

        #endregion
    }
}