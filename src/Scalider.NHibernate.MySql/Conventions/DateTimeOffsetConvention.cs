﻿#region # using statements #

using System;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using FluentNHibernate.Conventions.Inspections;
using FluentNHibernate.Conventions.Instances;
using Scalider.NHibernate.Types;

#endregion

namespace Scalider.NHibernate.Conventions
{

    /// <summary>
    /// <see cref="IPropertyConvention"/> implementation for
    /// <see cref="DateTimeOffset"/>.
    /// </summary>
    public class DateTimeOffsetConvention : IPropertyConvention,
                                            IPropertyConventionAcceptance
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="DateTimeOffsetConvention"/> class.
        /// </summary>
        public DateTimeOffsetConvention()
        {
        }

        #region # IConvention<IPropertyInspector,IPropertyInstance> #

        /// <summary>
        /// Apply changes to the target.
        /// </summary>
        public void Apply(IPropertyInstance instance)
            => instance.CustomType<DateTimeOffsetType>();

        #endregion

        #region # IConventionAcceptance<IPropertyInspector> #

        /// <summary>
        /// Whether this convention will be applied to the target.
        /// </summary>
        /// <param name="criteria">Instace that could be supplied</param>
        /// <returns>
        /// Apply on this target?
        /// </returns>
        public void Accept(IAcceptanceCriteria<IPropertyInspector> criteria)
            =>
            criteria.Expect(
                t =>
                    t.Type == typeof(DateTimeOffset) ||
                    t.Type == typeof(DateTimeOffset?));

        #endregion
    }
}