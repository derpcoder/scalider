﻿#region # using statements #

using System;
using System.ComponentModel.DataAnnotations;

#endregion

namespace Scalider.ComponentModel.DataAnnotations.Specialized
{

    /// <summary>
    /// Provides a validator for the Dominican Tax Payer Identifier (RNC).
    /// This class cannot be inherited.
    /// </summary>
    [AttributeUsage(
        AttributeTargets.Property | AttributeTargets.Field |
        AttributeTargets.Parameter)]
    public sealed class DominicanTaxPayerIdAttribute : ValidationAttribute
    {
        #region # Variables #

        private static readonly int[] Weights = {7, 9, 8, 6, 5, 4, 3, 2};

        #endregion

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="DominicanTaxPayerIdAttribute"/> class.
        /// </summary>
        public DominicanTaxPayerIdAttribute()
            : base("The {0} is not a valid tax payer identifier.")
        {
        }

        #region # Methods #

        #region == Overrides ==

        /// <summary>Determines whether the specified value of the object is valid. </summary>
        /// <returns>true if the specified value is valid; otherwise, false.</returns>
        /// <param name="value">The value of the object to validate. </param>
        public override bool IsValid(object value)
        {
            if (value == null)
                return true;

            var text = value as string;
            return text != null && IsValidValue(text);
        }

        #endregion

        #region == Public ==

        /// <summary>
        /// Determines whether the given <paramref name="value"/> is a valid
        /// tax payer id.
        /// </summary>
        /// <param name="value">The value to validate.</param>
        /// <returns>
        /// true if the given <paramref name="value"/> is valid; otherwise,
        /// false.
        /// </returns>
        public static bool IsValidValue(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return false;

            var text = value.Replace("-", "").Replace(" ", "");
            text = string.IsNullOrWhiteSpace(text) ? null : text;

            return text != null && text.Length == 9 && IsValidLuhn(text);
        }

        #endregion

        #region == Internal ==

        /// <summary>
        /// Determines if the given value is valid using the Luhn algorithm
        /// validator.
        /// </summary>
        /// <param name="value">The value to validate.</param>
        /// <returns>
        /// true if the given value is valid; otherwise, false.
        /// </returns>
        internal static bool IsValidLuhn(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return false;

            var checksum = 0;
            for (var i = value.Length - 2; i >= 0; i--)
                checksum += (value[i] - '0') * Weights[i];

            checksum %= 11;
            var validator = checksum == 0 ? 2 : checksum == 1 ? 1 : 11 - checksum;

            return validator == int.Parse(value.Substring(value.Length - 1));
        }

        #endregion

        #endregion
    }
}