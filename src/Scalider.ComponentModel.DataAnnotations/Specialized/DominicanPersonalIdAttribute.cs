﻿#region # using statements #

using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

#endregion

namespace Scalider.ComponentModel.DataAnnotations.Specialized
{

    /// <summary>
    /// Provides a validator for the Dominican Identification Document.
    /// This class cannot be inherited.
    /// </summary>
    [AttributeUsage(
        AttributeTargets.Property | AttributeTargets.Field |
        AttributeTargets.Parameter)]
    public sealed class DominicanPersonalIdAttribute : ValidationAttribute
    {
        #region # Variables #

        private static readonly string[] Exceptions =
        {
            "00000000018", "11111111123", "00100759932", "00105606543", "00114272360",
            "00200123640", "00200409772", "00800106971", "01200004166", "01400074875",
            "01400000282", "03103749672", "03200066940", "03800032522", "03900192284",
            "04900026260", "05900072869", "07700009346", "00114532330", "40200700675",
            "40200639953", "00121581750", "00119161853", "22321581834", "00121581800",
            "22721581818", "90001200901", "00301200901", "40200452735", "40200401324",
            "10621581792"
        };

        #endregion

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="DominicanPersonalIdAttribute"/> class.
        /// </summary>
        public DominicanPersonalIdAttribute()
            : base("The {0} field is not a valid personal identification number.")
        {
        }

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Determines whether the specified value of the object is valid. 
        /// </summary>
        /// <returns>
        /// true if the specified value is valid; otherwise, false.
        /// </returns>
        /// <param name="value">The value of the object to validate. </param>
        public override bool IsValid(object value)
        {
            if (value == null)
                return true;

            var text = value as string;
            return text != null && IsValidValue(text);
        }

        #endregion

        #region == Public ==

        /// <summary>
        /// Determines whether the given <paramref name="value"/> is a valid
        /// dominican personal id.
        /// </summary>
        /// <param name="value">The value to validate.</param>
        /// <returns>
        /// true if the given <paramref name="value"/> is valid; otherwise,
        /// false.
        /// </returns>
        public static bool IsValidValue(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return false;

            var text = value.Replace("-", "").Replace(" ", "");
            text = string.IsNullOrWhiteSpace(text) ? null : text;

            return text != null && text.Length == 11 &&
                   (IsValidLuhn(text) || Exceptions.Contains(text));
        }

        /*// <summary>
        /// Calculates the luhn checksum for a given number.
        /// </summary>
        /// <param name="number">The number to calculate the checksum for.</param>
        /// <returns>
        /// The checksum of <paramref name="number"/>.
        /// </returns>
        public static int GetLuhnCheckDigit(string number)
        {
            if (number == null)
                throw new ArgumentNullException(nameof(number));

            var sum = 0;
            var alt = true;
            var digits = number.ToCharArray();
            for (var i = digits.Length - 1; i >= 0; i--)
            {
                var curDigit = (digits[i] - 48);
                if (alt)
                {
                    curDigit *= 2;
                    if (curDigit > 9)
                        curDigit -= 9;
                }

                sum += curDigit;
                alt = !alt;
            }

            if ((sum % 10) == 0)
                return 0;
            return (10 - (sum % 10));
        }*/

        #endregion

        #region == Internal ==

        /// <summary>
        /// Determines if the given value is valid using the Luhn algorithm
        /// validator.
        /// </summary>
        /// <param name="value">The value to validate.</param>
        /// <returns>
        /// true if the given value is valid; otherwise, false.
        /// </returns>
        internal static bool IsValidLuhn(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return false;

            var checksum = 0;
            var evenDigit = false;

            foreach (var digit in value.Reverse())
            {
                if (digit < '0' || digit > '9')
                    return false;

                var digitValue = (digit - '0') * ((evenDigit) ? 2 : 1);
                evenDigit = !evenDigit;

                while (digitValue > 0)
                {
                    checksum += digitValue % 10;
                    digitValue /= 10;
                }
            }

            return (checksum % 10) == 0;
        }

        #endregion

        #endregion
    }
}