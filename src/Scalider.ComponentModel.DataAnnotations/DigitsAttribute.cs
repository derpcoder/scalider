﻿#region # using statements #

using System;
using System.ComponentModel.DataAnnotations;

#endregion

namespace Scalider.ComponentModel.DataAnnotations
{

    /// <summary>
    /// Specifies that a data field value should only contain digits. This
    /// class cannot be inherited.
    /// </summary>
    [AttributeUsage(
        AttributeTargets.Property | AttributeTargets.Field |
        AttributeTargets.Parameter)]
    public sealed class DigitsAttribute : ValidationAttribute
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="DigitsAttribute"/>
        /// class.
        /// </summary>
        public DigitsAttribute()
            : base("The {0} field should only contain digits.")
        {
        }

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Determines whether the specified value only contains digits.
        /// </summary>
        /// <param name="value">The value to validate.</param>
        /// <returns>
        /// true if the specified value is valid or null; otherwise, false.
        /// </returns>
        public override bool IsValid(object value)
        {
            if (value == null)
                return true;
            var text = value as string;
            long ret;

            return text != null && long.TryParse(text, out ret);
        }

        #endregion

        #endregion
    }

}