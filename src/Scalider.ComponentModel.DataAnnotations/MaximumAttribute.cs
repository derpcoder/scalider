﻿#region # using statements #

using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

#endregion

namespace Scalider.ComponentModel.DataAnnotations
{

    /// <summary>
    /// Specifies the maximum value allowed in a property. This class cannot be
    /// inherited.
    /// </summary>
    [AttributeUsage(
        AttributeTargets.Property | AttributeTargets.Field |
        AttributeTargets.Parameter)]
    public sealed class MaximumAttribute : ValidationAttribute
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="MaximumAttribute"/>
        /// class.
        /// </summary>
        /// <param name="maximum">The maximum allowed value.</param>
        public MaximumAttribute(int maximum)
            : base("The field {0} must be less than or equal to {1}.")
        {
            Maximum = maximum;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MaximumAttribute"/>
        /// class.
        /// </summary>
        /// <param name="maximum">The maximum allowed value.</param>
        public MaximumAttribute(double maximum)
            : base("The field {0} must be less than or equal to {1}.")
        {
            Maximum = maximum;
        }

        #region # Properties #

        #region == Public ==

        /// <summary>
        /// Gets the maximum allowed value.
        /// </summary>
        public object Maximum { get; }

        #endregion

        #region == Private ==

        private Func<object, object> Conversion { get; set; }

        #endregion

        #endregion

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Checks that the value of the data field is valid.
        /// </summary>
        /// <param name="value">The value to validate.</param>
        /// <returns>
        /// true if the specified value is valid or null; otherwise, false.
        /// </returns>
        public override bool IsValid(object value)
        {
            SetupConversion();
            if (value == null)
                return true;

            var text = value as string;
            if (text != null && string.IsNullOrEmpty(text))
                return true;

            // Convert value to the expected type
            object convertedValue;
            try
            {
                convertedValue = Conversion(value);
            }
            catch
            {
                return false;
            }

            // Validate
            var max = (IComparable)Maximum;
            return max.CompareTo(convertedValue) >= 0;
        }

        /// <summary>
        /// Applies formatting to an error message, based on the data field
        /// where the error occurred.
        /// </summary>
        /// <param name="name">The name to include in the formatted
        /// message.</param>
        /// <returns>
        /// An instance of the formatted error message.
        /// </returns>
        public override string FormatErrorMessage(string name)
        {
            return string.Format(ErrorMessageString, name, Maximum);
        }

        #endregion

        #region == Private ==

        private void SetupConversion()
        {
            var operandType = Maximum.GetType();
            if (operandType == typeof(int))
            {
                Conversion =
                    v => Convert.ToInt32(v, CultureInfo.InvariantCulture);
            }
            else if (operandType == typeof(double))
            {
                Conversion =
                    v => Convert.ToDouble(v, CultureInfo.InvariantCulture);
            }
        }

        #endregion

        #endregion
    }

}