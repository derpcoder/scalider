﻿#region # using statements #

using System;
using System.ComponentModel.DataAnnotations;

#endregion

namespace Scalider.ComponentModel.DataAnnotations
{

    /// <summary>
    /// Specifies that a data field value should be numeric.
    /// </summary>
    [AttributeUsage(
        AttributeTargets.Property | AttributeTargets.Field |
        AttributeTargets.Parameter)]
    public sealed class NumericAttribute : ValidationAttribute
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="NumericAttribute"/>
        /// class.
        /// </summary>
        public NumericAttribute()
            : base("The {0} field is not a valid number.")
        {
        }

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Determines whether the specified value only contains digits.
        /// </summary>
        /// <param name="value">The value to validate.</param>
        /// <returns>
        /// true if the specified value is valid or null; otherwise, false.
        /// </returns>
        public override bool IsValid(object value)
        {
            if (value == null)
                return true;

            var text = value as string;
            double ret;

            return text != null && double.TryParse(text, out ret);
        }

        #endregion

        #endregion
    }

}