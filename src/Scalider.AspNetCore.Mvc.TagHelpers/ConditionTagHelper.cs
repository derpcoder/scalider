﻿#region # using statements #

using Microsoft.AspNetCore.Razor.TagHelpers;

#endregion

namespace Scalider.AspNetCore.Mvc.TagHelpers
{

    /// <summary>
    /// Sets a condition used to determine if the element should be rendered.
    /// </summary>
    [HtmlTargetElement(Attributes = ConditionAttributeName)]
    public class ConditionTagHelper : TagHelper
    {
        #region # Constants #

        /// <summary>
        /// Represents the name of the attribute.
        /// </summary>
        public const string ConditionAttributeName = "asp-condition";

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ConditionTagHelper" /> class.
        /// </summary>
        public ConditionTagHelper()
        {
        }

        #region # Properties #

        #region == Public ==

        /// <summary>
        /// Gets or sets a flag indicating whether the element should be
        /// supressed.
        /// </summary>
        [HtmlAttributeName(ConditionAttributeName)]
        public bool Condition { get; set; }

        #endregion

        #endregion

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Synchronously executes the <see cref="TagHelper" /> with the given
        /// <paramref name="context" /> and <paramref name="output" />.
        /// </summary>
        /// <param name="context">
        /// Contains information associated with the
        /// current HTML tag.
        /// </param>
        /// <param name="output">
        /// A stateful HTML element used to generate an
        /// HTML tag.
        /// </param>
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            Check.NotNull(context, nameof(context));
            Check.NotNull(output, nameof(output));

            if (!Condition)
                output.SuppressOutput();
        }

        #endregion

        #endregion
    }
}