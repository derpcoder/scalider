﻿#region # using statements #

using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

#endregion

namespace Scalider.AspNetCore.Mvc.TagHelpers
{

    /// <summary>
    /// Renders a partial view.
    /// 
    /// Based on https://github.com/aspnet/live.asp.net/blob/dev/src/live.asp.net/TagHelpers/PartialTagHelper.cs
    /// </summary>
    [HtmlTargetElement("partial", Attributes = ViewNameAttributeName)]
    public class PartialTagHelper : TagHelper
    {

        #region # Constants #

        /// <summary>
        /// The name of the view name attribute.
        /// </summary>
        public const string ViewNameAttributeName = "view-name";

        #endregion

        #region # Variables #
        
        private readonly IHtmlHelper _htmlHelper;
        private string _viewName;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PartialTagHelper"/>
        /// class.
        /// </summary>
        /// <param name="htmlHelper"></param>
        public PartialTagHelper(IHtmlHelper htmlHelper)
        {
            Check.NotNull(htmlHelper, nameof(htmlHelper));
            _htmlHelper = htmlHelper;
        }

        #region # Properties #

        #region == Public ==

        /// <summary>
        /// Gets or sets the name of the partial.
        /// </summary>
        [HtmlAttributeName(ViewNameAttributeName)]
        public string Name
        {
            [AspMvcPartialView]
            get
            {
                return _viewName;
            }
            [AspMvcPartialView]
            set
            {
                Check.NotNullOrEmpty(value, nameof(value));
                _viewName = value;
            }
        }

        /// <summary>
        /// Gets or sets the model to pass to the partial.
        /// </summary>
        public object Model { get; set; }

        /// <summary>
        /// Gets the view context for the tag helper.
        /// </summary>
        [ViewContext]
        public ViewContext ViewContext { get; set; }

        #endregion

        #endregion

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Asynchronously executes the <see cref="TagHelper"/> with the given
        /// <paramref name="context"/> and <paramref name="output"/>.
        /// </summary>
        /// <param name="context">Contains information associated with the
        /// current HTML tag.</param>
        /// <param name="output">A stateful HTML element used to generate an
        /// HTML tag.</param>
        /// <returns>
        /// A <see cref="System.Threading.Tasks.Task{TResult}"/> that on completion updates the
        /// <paramref name="output"/>.
        /// </returns>
        public override async Task ProcessAsync(TagHelperContext context,
            TagHelperOutput output)
        {
            Check.NotNull(context, nameof(context));

            ((IViewContextAware)_htmlHelper).Contextualize(ViewContext);
            output.TagName = null;

            var content = Model == null
                ? await _htmlHelper.PartialAsync(Name)
                : await _htmlHelper.PartialAsync(Name, Model);

            // ReSharper disable once MustUseReturnValue
            output.Content.SetHtmlContent(content);
        }

        #endregion

        #endregion
    }
}