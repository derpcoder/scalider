﻿#region # using statements #

using Scalider.IO;

#endregion

namespace Scalider
{

    /// <summary>
    /// Extension methods for <see cref="IApplication"/>.
    /// </summary>
    public static class ApplicationExtensions
    {

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        public static string ResolvePath(this IApplication app, string path)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
        {
            Check.NotNull(app, nameof(app));

            return PathHelper.ResolvePath(path, app.ContentRootPath);
        }

    }
}