﻿#region # using statements #

using System;
using System.Reflection;
using System.Runtime.ExceptionServices;
using Microsoft.Extensions.DependencyInjection;

#endregion

namespace Scalider.Internal
{

    /// <summary>
    /// <see cref="IStartup"/> implementation that uses a delegate to
    /// configure the application services.
    /// </summary>
    public class ConventionBasedStartup : IStartup
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="ConventionBasedStartup"/> class.
        /// </summary>
        /// <param name="configureServicesDelegate"></param>
        public ConventionBasedStartup(
            Func<IServiceCollection, IServiceProvider> configureServicesDelegate)
        {
            ConfigureServicesDelegate = configureServicesDelegate;
        }

        #region # Properties #

        #region == Public ==

        /// <summary>
        /// Gets the delegate used to configure the application services.
        /// </summary>
        public Func<IServiceCollection, IServiceProvider> ConfigureServicesDelegate {
            get; }

        #endregion

        #endregion

        #region # IStartup #

        /// <summary>
        /// Configures the application services.
        /// </summary>
        /// <param name="serviceCollection">The
        /// <see cref="IServiceCollection"/>.</param>
        /// <returns>
        /// The <see cref="IServiceProvider"/>.
        /// </returns>
        public IServiceProvider ConfigureServices(
            IServiceCollection serviceCollection)
        {
            try
            {
                return ConfigureServicesDelegate(serviceCollection);
            }
            catch (Exception ex)
            {
                if (ex is TargetInvocationException)
                    ExceptionDispatchInfo.Capture(ex.InnerException).Throw();

                throw;
            }
        }

        #endregion
    }
}