﻿namespace Scalider.Internal
{

    /// <summary>
    /// Repreents a JSON configuration file that will be added to the
    /// application configuration.
    /// </summary>
    public class ConfigurationFile
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationFile"/>
        /// class.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="required"></param>
        /// <param name="reloadOnChange"></param>
        public ConfigurationFile(string path, bool required, bool reloadOnChange)
        {
            Check.NotNullOrEmpty(path, nameof(path));

            Path = path;
            Required = required;
            ReloadOnChange = reloadOnChange;
        }

        #region # Properties #

        #region == Public ==

        /// <summary>
        /// The path to the file.
        /// </summary>
        public string Path { get; }

        /// <summary>
        /// A value indicating whether the file is required.
        /// </summary>
        public bool Required { get; }

        /// <summary>
        /// A value indicating whether the configuration should be reloaded
        /// when the file is changed.
        /// </summary>
        public bool ReloadOnChange { get; }

        #endregion

        #endregion
    }
}