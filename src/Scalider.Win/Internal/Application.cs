﻿#region # using statements #

using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

#endregion

namespace Scalider.Internal
{

    /// <summary>
    /// Default implementation for <see cref="IApplication"/>.
    /// </summary>
    public class Application : Disposable, IApplication
    {
        #region # Variables #

        private readonly IServiceProvider _applicationHostProvider;
        private readonly IServiceCollection _applicationServiceCollection;
        private IStartup _startup;
        private IServiceProvider _applicationServices;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="Application"/> class.
        /// </summary>
        /// <param name="applicationHostProvider"></param>
        /// <param name="serviceCollection"></param>
        /// <param name="config"></param>
        /// <param name="contentRootPath"></param>
        public Application(IServiceProvider applicationHostProvider,
            IServiceCollection serviceCollection, IConfiguration config,
            string contentRootPath)
        {
            Check.NotNull(applicationHostProvider, nameof(applicationHostProvider));
            Check.NotNull(serviceCollection, nameof(serviceCollection));

            _applicationHostProvider = applicationHostProvider;
            _applicationServiceCollection = serviceCollection;
            ContentRootPath = contentRootPath;
            Configuration = config;
        }

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">true to release both managed and unmanaged
        /// resources; false to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            (_applicationServices as IDisposable)?.Dispose();
        }

        #endregion

        #region == Private ==

        private void EnsureApplicationServices()
        {
            if (_applicationServices != null)
                return;

            EnsureStartup();
            _applicationServices =
                _startup.ConfigureServices(_applicationServiceCollection);
        }

        private void EnsureStartup()
        {
            if (_startup != null)
                return;

            _startup = _applicationHostProvider.GetRequiredService<IStartup>();
        }

        #endregion

        #endregion

        #region # IApplication #

        /// <summary>
        /// Gets the root path for the application content.
        /// </summary>
        public string ContentRootPath { get; }

        /// <summary>
        /// Gets the <see cref="IConfiguration"/> for the application.
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// Gets the <see cref="IServiceProvider"/> for the application.
        /// </summary>
        public IServiceProvider ApplicationServices
        {
            get
            {
                EnsureApplicationServices();
                return _applicationServices;
            }
        }

        #endregion
    }
}