﻿#region # using statements #

using System;
using JetBrains.Annotations;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

#endregion

namespace Scalider
{

    /// <summary>
    /// A builder for <see cref="IApplication"/>.
    /// </summary>
    public interface IApplicationBuilder
    {

        /// <summary>
        /// Defines the root path for the application content.
        /// </summary>
        /// <param name="contentRootPath">The application root path.</param>
        /// <returns>
        /// The <see cref="IApplicationBuilder" />.
        /// </returns>
        IApplicationBuilder UseContentRootPath(string contentRootPath);

        /// <summary>
        /// Adds a JSON configuration file to the application configuration.
        /// </summary>
        /// <param name="path">The path to the configuration file.</param>
        /// <param name="required">Whether the configuration file is required.</param>
        /// <param name="reloadOnChange">Whether the configuration file should
        /// be reloaded when the file is changed.</param>
        /// <returns>
        /// The <see cref="IApplicationBuilder" />.
        /// </returns>
        IApplicationBuilder AddJsonFile([NotNull] string path, bool required,
            bool reloadOnChange);

        /// <summary>
        /// Adds a delegate for configuring additional services for the
        /// application. This may be called multiple times.
        /// </summary>
        /// <param name="configureServices">A delegate for configuring the
        /// <see cref="IServiceCollection" />.</param>
        /// <returns>
        /// The <see cref="IApplicationBuilder" />.
        /// </returns>
        IApplicationBuilder ConfigureServices(
            [NotNull] Action<IServiceCollection> configureServices);

        /// <summary>
        /// Adds a delegate for configuring the provided
        /// <see cref="ILoggerFactory" />. This may be called multiple times.
        /// </summary>
        /// <param name="configureLogging">The delegate that configures the
        /// <see cref="ILoggerFactory" />.</param>
        /// <returns>
        /// The <see cref="IApplicationBuilder" />.
        /// </returns>
        IApplicationBuilder ConfigureLogging(
            [NotNull] Action<ILoggerFactory> configureLogging);

        /// <summary>
        /// Specify the <see cref="ILoggerFactory" /> to be used by the
        /// application.
        /// </summary>
        /// <param name="loggerFactory">The <see cref="ILoggerFactory" /> to be
        /// used.</param>
        /// <returns>
        /// The <see cref="IApplicationBuilder" />.
        /// </returns>
        IApplicationBuilder UseLoggerFactory([NotNull] ILoggerFactory loggerFactory);

        /// <summary>
        /// Builds the required services and an <see cref="IApplication" />.
        /// </summary>
        /// <returns>
        /// The <see cref="IApplication"/>.
        /// </returns>
        IApplication Build();

    }
}