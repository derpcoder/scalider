﻿#region # using statements #

using System;
using System.Linq;
using System.Reflection;
using JetBrains.Annotations;
using Microsoft.Extensions.DependencyInjection;
using Scalider.Internal;

#endregion

namespace Scalider
{

    /// <summary>
    /// Extension methods for <see cref="IApplicationBuilder"/>.
    /// </summary>
    public static class ApplicationBuilderExtensions
    {

        /// <summary>
        /// Adds a JSON configuration file to the application configuration.
        /// </summary>
        /// <param name="appBuilder">The <see cref="IApplicationBuilder"/>.</param>
        /// <param name="path">The path to the configuration file.</param>
        /// <param name="required">Whether the configuration file is required.</param>
        /// <param name="reloadOnChange">Whether the configuration file should
        /// be reloaded when the file is changed.</param>
        /// <returns>
        /// The <see cref="IApplicationBuilder" />.
        /// </returns>
        public static IApplicationBuilder AddJsonFile(
            this IApplicationBuilder appBuilder, [NotNull] string path,
            bool required = true, bool reloadOnChange = false)
        {
            Check.NotNull(appBuilder, nameof(appBuilder));
            return appBuilder.AddJsonFile(path, required, reloadOnChange);
        }

        /// <summary>
        /// Specify the startup type to be used by the application.
        /// </summary>
        /// <typeparam name="T">The type containing the startup methods for
        /// the application.</typeparam>
        /// <param name="appBuilder">The <see cref="IApplicationBuilder"/>.</param>
        /// <returns>
        /// The <see cref="IApplicationBuilder" />.
        /// </returns>
        public static IApplicationBuilder UseStartup<T>(
            this IApplicationBuilder appBuilder) where T : class
            => appBuilder.UseStartup(typeof(T));

        /// <summary>
        /// Specify the startup type to be used by the application.
        /// </summary>
        /// <param name="appBuilder">The <see cref="IApplicationBuilder"/>.</param>
        /// <param name="startupType">The <see cref="Type" /> to be used.</param>
        /// <returns>
        /// The <see cref="IApplicationBuilder" />.
        /// </returns>
        public static IApplicationBuilder UseStartup(
            this IApplicationBuilder appBuilder, [NotNull] Type startupType)
        {
            Check.NotNull(appBuilder, nameof(appBuilder));
            Check.NotNull(startupType, nameof(startupType));

            appBuilder.ConfigureServices(s =>
            {
                if (typeof(IStartup).GetTypeInfo().IsAssignableFrom(startupType))
                    s.AddSingleton(typeof(IStartup), startupType);
                else
                {
                    s.AddSingleton(typeof(IStartup),
                        p =>
                            new ConventionBasedStartup(
                                FindConfigureServicesDelegate(p, startupType)));
                }
            });

            return appBuilder;
        }

        private static Func<IServiceCollection, IServiceProvider>
            FindConfigureServicesDelegate(IServiceProvider serviceProvider,
                Type startupType)
        {
            var configureServicesMethod =
                FindMethod(startupType, "ConfigureServices", typeof(IServiceProvider)) ??
                FindMethod(startupType, "ConfigureServices", typeof(void));

            // Retrieve the startup class instance
            object instance = null;
            if (configureServicesMethod != null && !configureServicesMethod.IsStatic)
            {
                instance =
                    ActivatorUtilities.GetServiceOrCreateInstance(serviceProvider,
                        startupType);
            }

            // Create callback
            Func<IServiceCollection, IServiceProvider> configureServicesCallback =
                services =>
                    InvokeConfigureServicesDelegate(configureServicesMethod, instance,
                        services);

            // Done
            return services =>
            {
                var applicationServicesProvider =
                    configureServicesCallback.Invoke(services);

                return applicationServicesProvider ?? services.BuildServiceProvider();
            };
        }

        private static MethodInfo FindMethod(IReflect startupType, string methodName,
            Type returnType = null)
        {
            var methods =
                startupType.GetMethods(BindingFlags.Public | BindingFlags.Instance |
                                       BindingFlags.Static);

            var selectedMethods =
                methods.Where(m => m.Name.Equals(methodName)).ToList();

            if (selectedMethods.Count > 1)
            {
                // Multiple methods found
                throw new InvalidOperationException(
                    $"Having multiple overloads of method '{methodName}' " +
                    "is not supported.");
            }

            // Validate found method
            var methodInfo = selectedMethods.FirstOrDefault();
            if (methodInfo == null)
                return null;

            if (returnType != null && methodInfo.ReturnType != returnType)
                return null;

            // Done
            return methodInfo;
        }

        private static IServiceProvider InvokeConfigureServicesDelegate(
            MethodInfo methodInfo, object instance, IServiceCollection services)
        {
            if (methodInfo == null)
                return null;

            // Only support IServiceCollection parameters
            var parameters = methodInfo.GetParameters();
            if (parameters.Length > 1 ||
                parameters.Any(p => p.ParameterType != typeof(IServiceCollection)))
            {
                throw new InvalidOperationException(
                    "The ConfigureServices method must either be parameterless " +
                    "or take only one parameter of type IServiceCollection.");
            }

            // Invoke method
            var arguments = new object[parameters.Length];
            if (parameters.Length > 0)
                arguments[0] = services;

            return methodInfo.Invoke(instance, arguments) as IServiceProvider;
        }

    }
}