﻿#region # using statements #

using System;
using Microsoft.Extensions.Configuration;

#endregion

namespace Scalider
{

    /// <summary>
    /// Represents a configured application.
    /// </summary>
    public interface IApplication
    {

        /// <summary>
        /// Gets the root path for the application content.
        /// </summary>
        string ContentRootPath { get; }

        /// <summary>
        /// Gets the <see cref="IConfiguration"/> for the application.
        /// </summary>
        IConfiguration Configuration { get; }

        /// <summary>
        /// Gets the <see cref="IServiceProvider"/> for the application.
        /// </summary>
        IServiceProvider ApplicationServices { get; }

    }
}