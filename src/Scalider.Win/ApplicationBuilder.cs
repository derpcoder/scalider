﻿#region # using statements #

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.ObjectPool;
using Scalider.Internal;
using Scalider.IO;

#endregion

namespace Scalider
{

    /// <summary>
    /// A builder for <see cref="IApplication"/>.
    /// </summary>
    public class ApplicationBuilder : IApplicationBuilder
    {
        #region # Variables #

        private readonly List<ConfigurationFile> _configFiles;
        private readonly List<Action<IServiceCollection>> _configureServicesDelegates;
        private readonly List<Action<ILoggerFactory>> _configureLoggingDelegates;

        private bool _applicationBuilt;
        private string _contentRootPath;
        private ILoggerFactory _loggerFactory;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationBuilder"/>
        /// class.
        /// </summary>
        public ApplicationBuilder()
        {
            _configFiles = new List<ConfigurationFile>();
            _configureServicesDelegates = new List<Action<IServiceCollection>>();
            _configureLoggingDelegates = new List<Action<ILoggerFactory>>();

            AddJsonFile("./appconfig.json", false, true);
        }

        #region # Methods #

        #region == Private ==

        private static IServiceCollection CloneServices(
            IServiceCollection serviceCollection)
        {
            var newServices = new ServiceCollection();
            foreach (var serviceDescriptor in serviceCollection)
                newServices.Add(serviceDescriptor);

            return newServices;
        }

        private static void AddApplicationServices(IServiceCollection services,
            IServiceProvider provider)
        {
            var loggerFactory = provider.GetService<ILoggerFactory>();
            services.Replace(ServiceDescriptor.Singleton(typeof(ILoggerFactory),
                loggerFactory));
        }

        private IServiceCollection BuildCommonServices()
        {
            var services = new ServiceCollection();

            // Logging
            if (_loggerFactory == null)
            {
                // Create the default logger factory
                _loggerFactory = new LoggerFactory();
            }

            services.AddSingleton(p => _loggerFactory);
            foreach (var configureDelegate in _configureLoggingDelegates)
                configureDelegate(_loggerFactory);

            services.AddLogging();

            // Other services
            services
                .AddTransient
                <IServiceProviderFactory<IServiceCollection>,
                    DefaultServiceProviderFactory>()
                .AddSingleton<ObjectPoolProvider, DefaultObjectPoolProvider>();

            // Invoke services delegates
            foreach (var configureDelegate in _configureServicesDelegates)
                configureDelegate(services);

            // Done
            return services;
        }

        #endregion

        #endregion

        #region # IApplicationBuilder #

        /// <summary>
        /// Defines the root path for the application content.
        /// </summary>
        /// <param name="contentRootPath">The application root path.</param>
        /// <returns>
        /// The <see cref="IApplicationBuilder" />.
        /// </returns>
        public IApplicationBuilder UseContentRootPath(string contentRootPath)
        {
            _contentRootPath = contentRootPath;
            return this;
        }

        /// <summary>
        /// Adds a JSON configuration file to the application configuration.
        /// </summary>
        /// <param name="path">The path to the configuration file.</param>
        /// <param name="required">Whether the configuration file is required.</param>
        /// <param name="reloadOnChange">Whether the configuration file should
        /// be reloaded when the file is changed.</param>
        /// <returns>
        /// The <see cref="IApplicationBuilder" />.
        /// </returns>
        public IApplicationBuilder AddJsonFile(string path, bool required,
            bool reloadOnChange)
        {
            Check.NotNullOrEmpty(path, nameof(path));

            // Determine if the configuration file already exists
            var file =
                _configFiles.Where(
                    f =>
                        string.Equals(f.Path, path, StringComparison.Ordinal) &&
                        f.Required == required);

            if (file.Any())
                return this;

            // The configuration file doesn't exists, add it
            _configFiles.Add(new ConfigurationFile(path, required, reloadOnChange));
            return this;
        }

        /// <summary>
        /// Adds a delegate for configuring additional services for the
        /// application. This may be called multiple times.
        /// </summary>
        /// <param name="configureServices">A delegate for configuring the
        /// <see cref="IServiceCollection" />.</param>
        /// <returns>
        /// The <see cref="IApplicationBuilder" />.
        /// </returns>
        public IApplicationBuilder ConfigureServices(
            Action<IServiceCollection> configureServices)
        {
            Check.NotNull(configureServices, nameof(configureServices));

            _configureServicesDelegates.Add(configureServices);
            return this;
        }

        /// <summary>
        /// Adds a delegate for configuring the provided
        /// <see cref="ILoggerFactory" />. This may be called multiple times.
        /// </summary>
        /// <param name="configureLogging">The delegate that configures the
        /// <see cref="ILoggerFactory" />.</param>
        /// <returns>
        /// The <see cref="IApplicationBuilder" />.
        /// </returns>
        public IApplicationBuilder ConfigureLogging(
            Action<ILoggerFactory> configureLogging)
        {
            Check.NotNull(configureLogging, nameof(configureLogging));

            _configureLoggingDelegates.Add(configureLogging);
            return this;
        }

        /// <summary>
        /// Specify the <see cref="ILoggerFactory" /> to be used by the
        /// application.
        /// </summary>
        /// <param name="loggerFactory">The <see cref="ILoggerFactory" /> to be
        /// used.</param>
        /// <returns>
        /// The <see cref="IApplicationBuilder" />.
        /// </returns>
        public IApplicationBuilder UseLoggerFactory(ILoggerFactory loggerFactory)
        {
            Check.NotNull(loggerFactory, nameof(loggerFactory));

            _loggerFactory = loggerFactory;
            return this;
        }

        /// <summary>
        /// Builds the required services and an <see cref="IApplication" />.
        /// </summary>
        /// <returns>
        /// The <see cref="IApplication"/>.
        /// </returns>
        public IApplication Build()
        {
            if (_applicationBuilt)
                throw new Exception();

            _applicationBuilt = true;

            // Retrieve content root path
            var entryAssembly = Assembly.GetExecutingAssembly();
            var entryAssemblyPath = Path.GetDirectoryName(entryAssembly.Location);

            var contentRootPath = PathHelper.ResolvePath(_contentRootPath,
                entryAssemblyPath);

            // Create application configuration
            var config = new ConfigurationBuilder();
            foreach (var configFile in _configFiles)
            {
                var path = PathHelper.ResolvePath(configFile.Path, contentRootPath);
                config.AddJsonFile(path, !configFile.Required,
                    configFile.ReloadOnChange);
            }

            // Build application services
            var services1 = BuildCommonServices();
            var services2 = CloneServices(services1);
            var applicationServiceProvider = services1.BuildServiceProvider();

            AddApplicationServices(services2, applicationServiceProvider);

            // Done
            return new Application(applicationServiceProvider, services2,
                config.Build(), contentRootPath);
        }

        #endregion
    }
}