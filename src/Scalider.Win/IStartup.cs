﻿#region # using statements #

using System;
using Microsoft.Extensions.DependencyInjection;

#endregion

namespace Scalider
{

    /// <summary>
    /// Defines the basic functionality of an application startup service used
    /// to configure the application services.
    /// </summary>
    public interface IStartup
    {

        /// <summary>
        /// Configures the application services.
        /// </summary>
        /// <param name="serviceCollection">The
        /// <see cref="IServiceCollection"/>.</param>
        /// <returns>
        /// The <see cref="IServiceProvider"/>.
        /// </returns>
        IServiceProvider ConfigureServices(IServiceCollection serviceCollection);

    }
}