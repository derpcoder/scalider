﻿#region # using statements #

using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

#endregion

namespace Scalider.AspNetCore.Mvc
{

    /// <summary>
    /// Represents a middleware that blocks any request that isn't lcoal. This
    /// class cannot be inherited.
    /// </summary>
    public sealed class LocalRequestOnlyMiddleware
    {
        #region # Variables #

        private readonly RequestDelegate _next;
        private readonly ILogger<LocalRequestOnlyMiddleware> _logger;

        #endregion

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="LocalRequestOnlyMiddleware"/> class.
        /// </summary>
        /// <param name="next"></param>
        /// <param name="loggerFactory"></param>
        public LocalRequestOnlyMiddleware([NotNull] RequestDelegate next,
            [NotNull] ILoggerFactory loggerFactory)
        {
            Check.NotNull(next, nameof(next));
            Check.NotNull(loggerFactory, nameof(loggerFactory));

            _next = next;
            _logger = loggerFactory.CreateLogger<LocalRequestOnlyMiddleware>();
        }

        #region # Methods #

        #region == Public ==

        /// <summary>
        /// A function that can process an HTTP request.
        /// </summary>
        /// <param name="context">The <see cref="HttpContext"/> for the
        /// request.</param>
        /// <returns>
        /// A task taht represents the completion of the request processing.
        /// </returns>
        public Task Invoke([NotNull] HttpContext context)
        {
            Check.NotNull(context, nameof(context));
            return !context.Request.IsLocal()
                ? Task.FromResult(0)
                : _next.Invoke(context);
        }

        #endregion

        #endregion
    }
}