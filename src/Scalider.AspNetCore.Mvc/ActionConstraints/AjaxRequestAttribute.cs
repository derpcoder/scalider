﻿#region # using statements #

using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.ActionConstraints;
using Microsoft.AspNetCore.Routing;

#endregion

namespace Scalider.AspNetCore.Mvc.ActionConstraints
{

    /// <summary>
    /// Represents an <see cref="IActionConstraint"/> that validates that the
    /// request is an AJAX request.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class AjaxRequestAttribute : ActionMethodSelectorAttribute
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="AjaxRequestAttribute"/> class.
        /// </summary>
        public AjaxRequestAttribute()
        {
        }

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Determines whether the action selection is valid for the specified
        /// route context.
        /// </summary>
        /// <param name="routeContext">The route context.</param>
        /// <param name="action">Information about the action.</param>
        /// <returns>
        /// <see langword="true" /> if the action  selection is valid for the
        /// specified context; otherwise, <see langword="false" />.
        /// </returns>
        public override bool IsValidForRequest(RouteContext routeContext,
            ActionDescriptor action)
        {
            Check.NotNull(routeContext, nameof(routeContext));
            Check.NotNull(action, nameof(action));

            var request = routeContext.HttpContext.Request;
            var requestedWith = GetHeaderValue(request);

            return !string.IsNullOrWhiteSpace(requestedWith) &&
                   string.Equals(requestedWith, "xmlhttprequest",
                       StringComparison.OrdinalIgnoreCase);
        }

        #endregion

        #region == Private ==

        private string GetHeaderValue(HttpRequest request)
        {
            var expectedHeaders = new[] {"x-requested-with", "http-x-requested-with"};
            foreach (var key in request.Headers.Keys)
            {
                var headerName = key?.ToLowerInvariant();
                if (string.IsNullOrWhiteSpace(headerName) ||
                    !expectedHeaders.Contains(headerName))
                    continue;

                var values = request.Headers[key];
                if (values.Count == 0)
                    continue;

                return values[0];
            }

            return null;
        }

        #endregion

        #endregion
    }
}