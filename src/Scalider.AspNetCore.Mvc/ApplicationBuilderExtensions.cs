﻿#region # using statements #

using JetBrains.Annotations;
using Microsoft.AspNetCore.Builder;

#endregion

namespace Scalider.AspNetCore.Mvc
{

    /// <summary>
    /// Extension methods for <see cref="IApplicationBuilder"/>.
    /// </summary>
    public static class ApplicationBuilderExtensions
    {

        /// <summary>
        /// Adds request blocking for remote clients to the
        /// <see cref="IApplicationBuilder"/> request executing path.
        /// </summary>
        /// <param name="appBuilder">The <see cref="IApplicationBuilder" />.</param>
        /// <returns>
        /// A reference to this instance after the operation has completed.
        /// </returns>
        public static IApplicationBuilder UseLocalOnly(
            [NotNull] this IApplicationBuilder appBuilder)
        {
            Check.NotNull(appBuilder, nameof(appBuilder));
            appBuilder.UseMiddleware<LocalRequestOnlyMiddleware>();

            // Done
            return appBuilder;
        }

    }
}