﻿#region # using statements #

using System.Threading.Tasks;
using JetBrains.Annotations;

#endregion

namespace Scalider.AspNetCore.Mvc
{

    /// <summary>
    /// Extension methods for <see cref="IViewRenderer"/>.
    /// </summary>
    public static class ViewRendererExtensions
    {

        /// <summary>
        /// Asynchronously renders a view as a <see cref="string"/>.
        /// </summary>
        /// <param name="renderer">The <see cref="IViewRenderer"/>.</param>
        /// <param name="viewName">The name of the view to render.</param>
        /// <returns>
        /// A task that represents the asynchronous render operation.
        /// </returns>
        public static Task<string> RenderAsync([NotNull] this IViewRenderer renderer,
            string viewName)
        {
            Check.NotNull(renderer, nameof(renderer));
            Check.NotNullOrEmpty(viewName, nameof(viewName));

            return renderer.RenderAsync(viewName, null);
        }

    }
}