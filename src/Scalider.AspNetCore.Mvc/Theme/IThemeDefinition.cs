﻿namespace Scalider.AspNetCore.Mvc.Theme
{

    /// <summary>
    /// Defines the basic functionality of a theme definition.
    /// </summary>
    public interface IThemeDefinition
    {
        
        /// <summary>
        /// Gets the name of the theme.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the absolute path to the theme directory.
        /// </summary>
        string AbsolutePath { get; }

        /// <summary>
        /// Gets the application relative path to the theme directory.
        /// </summary>
        string RelativePath { get; }

    }
}