﻿#region # using statements #

using JetBrains.Annotations;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

#endregion

namespace Scalider.AspNetCore.Mvc
{

    /// <summary>
    /// Extension methods for <see cref="IServiceCollection"/>.
    /// </summary>
    public static class ServiceCollectionExtensions
    {

        /// <summary>
        /// Adds the default <see cref="IViewRenderer"/> service to the
        /// specified <see cref="IServiceCollection"/>.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add
        /// services to.</param>
        /// <returns>
        /// The <see cref="IServiceCollection"/> so that additional calls can
        /// be chained.
        /// </returns>
        public static IServiceCollection AddViewRenderer(
            [NotNull] this IServiceCollection services)
        {
            Check.NotNull(services, nameof(services));
            services.TryAdd(
                ServiceDescriptor.Scoped<IViewRenderer, DefaultViewRenderer>());

            return services;
        }

    }
}