﻿#region # using statements #

using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

#endregion

namespace Scalider.AspNetCore.Mvc
{

    /// <summary>
    /// Represents an action filter that blocks any request that isn't local.
    /// This class cannot be inherited.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method,
        AllowMultiple = true)]
    public sealed class LocalRequestOnlyAttribute : ActionFilterAttribute
    {

        /// <summary>
        /// Initialize a new instance of the
        /// <see cref="LocalRequestOnlyAttribute"/> class.
        /// </summary>
        public LocalRequestOnlyAttribute()
        {
        }

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Called before the action method is invoked.
        /// </summary>
        /// <param name="context">The action executing context.</param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            Check.NotNull(context, nameof(context));

            // Determine if the request is local
            if (context.HttpContext.Request.IsLocal())
            {
                // The request is local.
                return;
            }

            // The request isn't local
            context.Result = new NotFoundResult();
        }

        #endregion

        #endregion
    }
}