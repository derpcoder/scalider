﻿#region # using statements #

using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.Extensions.Options;

#endregion

namespace Scalider.AspNetCore.Mvc.Routing
{

    /// <summary>
    /// An implementation of <see cref="IUrlHelperFactory"/> that outputs
    /// absolute URLs.
    /// </summary>
    public class AbsoluteUrlHelperFactory : IUrlHelperFactory
    {
        #region # Variables #

        private readonly AbsoluteUrlHelperOptions _options;

        #endregion

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="AbsoluteUrlHelperFactory"/> class.
        /// </summary>
        /// <param name="optionsAccessor"></param>
        public AbsoluteUrlHelperFactory(
            IOptions<AbsoluteUrlHelperOptions> optionsAccessor)
        {
            Check.NotNull(optionsAccessor, nameof(optionsAccessor));
            _options = optionsAccessor.Value ?? new AbsoluteUrlHelperOptions();

            if (string.IsNullOrWhiteSpace(_options.Host))
            {
                throw new ArgumentException("Options parameter must specify host",
                    nameof(optionsAccessor));
            }
        }

        #region # IUrlHelperFactory #

        /// <summary>
        /// Gets an <see cref="IUrlHelper" /> for the request associated with
        /// <paramref name="context" />.
        /// </summary>
        /// <param name="context">The <see cref="ActionContext" /> associated
        /// with the current request.</param>
        /// <returns>
        /// An <see cref="IUrlHelper" /> for the request associated with
        /// <paramref name="context" />.
        /// </returns>
        public IUrlHelper GetUrlHelper(ActionContext context)
        {
            Check.NotNull(context, nameof(context));
            Check.PropertyNotNull(context.HttpContext,
                nameof(ActionContext.HttpContext), nameof(ActionContext));
            Check.PropertyNotNull(context.HttpContext.Items,
                nameof(HttpContext.Items), nameof(HttpContext));

            // Perf: Create only one AbsoluteUrlHelper per context
            var httpContext = context.HttpContext;

            var urlHelper = httpContext.Items[typeof(IUrlHelper)] as IUrlHelper;
            if (urlHelper != null)
                return urlHelper;

            urlHelper = new AbsoluteUrlHelper(context, _options);
            httpContext.Items[typeof(IUrlHelper)] = urlHelper;

            // Done
            return urlHelper;
        }

        #endregion
    }
}