﻿#region # using statements #

using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;

#endregion

namespace Scalider.AspNetCore.Mvc.Routing
{

    /// <summary>
    /// An implementation of <see cref="IUrlHelper" /> that contains methods to
    /// build absolute URLs for ASP.NET MVC within an application.
    /// </summary>
    public class AbsoluteUrlHelper : IUrlHelper
    {
        #region # Variables #

        private readonly UrlHelper _urlHelper;
        private readonly AbsoluteUrlHelperOptions _options;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="AbsoluteUrlHelper"/>
        /// class.
        /// </summary>
        /// <param name="actionContext"></param>
        /// <param name="options"></param>
        public AbsoluteUrlHelper(ActionContext actionContext,
            AbsoluteUrlHelperOptions options)
        {
            Check.NotNull(actionContext, nameof(actionContext));
            Check.NotNull(options, nameof(options));

            ActionContext = actionContext;
            _urlHelper = new UrlHelper(actionContext);
            _options = options;
        }

        #region # Properties #

        #region == Private ==

        private Uri Uri
            =>
            new Uri($"{_options.Scheme}://{_options.Host}:{_options.Port}",
                UriKind.Absolute);

        #endregion

        #endregion

        #region # Methods #

        #region == Private ==

        private bool SchemeAndAuthorityMatch(Uri uri)
        {
            return
                string.Equals(Uri.Scheme, uri.Scheme,
                    StringComparison.OrdinalIgnoreCase) &&
                string.Equals(Uri.Authority, uri.Authority,
                    StringComparison.OrdinalIgnoreCase);
        }

        private string Absolutize(string path) => $"{Uri}" + (path ?? "").Trim('/');

        #endregion

        #endregion

        #region # IUrlHelper #

        /// <summary>
        /// Gets the <see cref="IUrlHelper.ActionContext" /> for the current
        /// request.
        /// </summary>
        public ActionContext ActionContext { get; }

        /// <summary>
        /// Generates a fully qualified or absolute URL specified by
        /// <see cref="UrlActionContext" /> for an action method, which
        /// contains action name, controller name, route values, protocol to
        /// use, host name, and fragment.
        /// </summary>
        /// <param name="actionContext">The context object for the generated
        /// URLs for an action method.</param>
        /// <returns>
        /// The fully qualified or absolute URL to an action method.
        /// </returns>
        public string Action(UrlActionContext actionContext)
        {
            Check.NotNull(actionContext, nameof(actionContext));
            return Absolutize(_urlHelper.Action(actionContext));
        }

        /// <summary>
        /// Converts a virtual (relative) path to an application absolute path.
        /// </summary>
        /// <remarks>
        /// If the specified content path does not start with the tilde (~) character,
        /// this method returns <paramref name="contentPath" /> unchanged.
        /// </remarks>
        /// <param name="contentPath">The virtual path of the content.</param>
        /// <returns>The application absolute path.</returns>
        public string Content(string contentPath)
        {
            return Absolutize(_urlHelper.Content(contentPath));
        }

        /// <summary>
        /// Returns a value that indicates whether the URL is local. A URL with
        /// an absolute path is considered local if it does not have a
        /// host/authority part. URLs using virtual paths ('~/') are also
        /// local.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <returns>
        /// <c>true</c> if the URL is local; otherwise, <c>false</c>.
        /// </returns>
        /// <example>
        /// <para>
        /// For example, the following URLs are considered local:
        /// /Views/Default/Index.html
        /// ~/Index.html
        /// </para>
        /// <para>
        /// The following URLs are non-local:
        /// ../Index.html
        /// http://www.contoso.com/
        /// http://localhost/Index.html
        /// </para>
        /// </example>
        public bool IsLocalUrl(string url)
        {
            Uri uri;
            if (!Uri.TryCreate(url, UriKind.RelativeOrAbsolute, out uri) &&
                !_urlHelper.IsLocalUrl(url))
                return false;
            if (!uri.IsAbsoluteUri && _urlHelper.IsLocalUrl(url))
                return true;

            return (!uri.IsAbsoluteUri && _urlHelper.IsLocalUrl(url)) ||
                   (uri.IsAbsoluteUri && SchemeAndAuthorityMatch(uri));
        }

        /// <summary>
        /// Generates a fully qualified or absolute URL specified by
        /// <see cref="UrlRouteContext" />, which contains the route name, the
        /// route values, protocol to use, host name and fragment.
        /// </summary>
        /// <param name="routeContext">The context object for the generated
        /// URLs for a route.</param>
        /// <returns>
        /// The fully qualified or absolute URL.
        /// </returns>
        public string RouteUrl(UrlRouteContext routeContext)
        {
            Check.NotNull(routeContext, nameof(routeContext));
            return Absolutize(_urlHelper.RouteUrl(routeContext));
        }

        /// <summary>
        /// Generates an absolute URL using the specified route name and
        /// values.
        /// </summary>
        /// <param name="routeName">The name of the route that is used to
        /// generate the URL.</param>
        /// <param name="values">An object that contains the route values.</param>
        /// <returns>
        /// The generated absolute URL.
        /// </returns>
        /// <remarks>
        /// The protocol and host is obtained from the current request.
        /// </remarks>
        public string Link(string routeName, object values)
        {
            return RouteUrl(new UrlRouteContext
            {
                RouteName = routeName,
                Values = values,
                Protocol = Uri.Scheme,
                Host = Uri.Authority
            });
        }

        #endregion
    }
}