﻿#region # using statements #

using System;

#endregion

namespace Scalider.AspNetCore.Mvc.Routing
{

    /// <summary>
    /// Provides the scheme, host and port options for the
    /// <see cref="AbsoluteUrlHelper"/>.
    /// </summary>
    public class AbsoluteUrlHelperOptions
    {
        #region # Variables #

        private string _scheme;
        private string _host;
        private int _port;

        #endregion

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="AbsoluteUrlHelperOptions"/> class.
        /// </summary>
        public AbsoluteUrlHelperOptions()
        {
        }

        #region # Properties #

        #region == Public ==

        /// <summary>
        /// Gets or sets the scheme.
        /// </summary>
        public string Scheme
        {
            get
            {
                return _scheme ?? "http";
            }
            set
            {
                _scheme = string.IsNullOrWhiteSpace(value) ? null : value;
            }
        }

        /// <summary>
        /// Gets or sets the host name.
        /// </summary>
        public string Host
        {
            get
            {
                return _host;
            }
            set
            {
                Check.NotNullOrEmpty(value, nameof(value));
                _host = value;
            }
        }

        /// <summary>
        /// Gets or sets the port.
        /// </summary>
        public int Port
        {
            get
            {
                if (_port != 0)
                    return _port;

                return string.Equals(Scheme, "https",
                    StringComparison.OrdinalIgnoreCase)
                    ? 443
                    : 80;
            }
            set
            {
                if (value < 0 || value > 65535)
                    throw new ArgumentOutOfRangeException(nameof(value));

                _port = value;
            }
        }

        #endregion

        #endregion
    }
}