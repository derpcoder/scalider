﻿#region # using statements #

using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Http;

#endregion

namespace Scalider.AspNetCore.Mvc
{

    /// <summary>
    /// Extension methods for the <see cref="HttpRequest"/> class.
    /// </summary>
    public static class HttpRequestExtensions
    {

        #region # Variables #

        [SuppressMessage("ReSharper", "InconsistentNaming")] private static readonly
            IPAddress[] LocalIPAddresses;

        #endregion

        static HttpRequestExtensions()
        {
            try
            {
#if NETSTANDARD
                var hostEntry =
                    AsyncHelper.RunSync(
                        () => Dns.GetHostEntryAsync(Dns.GetHostName()));
#else
                var hostEntry = Dns.GetHostEntry(Dns.GetHostName());
#endif
                LocalIPAddresses = hostEntry.AddressList;
            }
            catch
            {
                LocalIPAddresses = new[] {IPAddress.Loopback};
            }
        }

        /// <summary>
        /// Determines if the connected client is local.
        /// 
        /// See: http://www.strathweb.com/2016/04/request-islocal-in-asp-net-core/
        /// </summary>
        /// <param name="request">The request to test the client from.</param>
        /// <returns>
        /// <c>true</c> if the requesting client is local or a loopback;
        /// otherwise, <c>false</c>.
        /// </returns>
        public static bool IsLocal([NotNull] this HttpRequest request)
        {
            Check.NotNull(request, nameof(request));

            // Determine if the request is remote
            var connection = request.HttpContext.Connection;
            if (connection.RemoteIpAddress == null)
            {
                // For in memory TestServer or when dealing with default
                // connection info
                return connection.RemoteIpAddress == null ||
                       connection.LocalIpAddress == null;
            }

            // Retrieve the list of all local IP addresses
            var localAddresses = LocalIPAddresses;
            if (connection.LocalIpAddress != null &&
                !localAddresses.Contains(connection.LocalIpAddress))
            {
                // The server local address is not in the list, append it
                localAddresses =
                    localAddresses.Concat(new[] {connection.LocalIpAddress})
                                  .ToArray();
            }

            // Determine if the remote address is in the list
            return localAddresses.Contains(connection.RemoteIpAddress);
        }

    }
}