﻿#region # using statements #

using System;
using System.Data;
using JetBrains.Annotations;
using Scalider.Domain.UnitOfWork;
using Microsoft.AspNetCore.Mvc.Filters;

#endregion

namespace Scalider.AspNetCore.Mvc.UnitOfWork
{

    /// <summary>
    /// Represents an <see cref="IActionFilter"/> that disposes the
    /// <see cref="IUnitOfWork"/> at the end of the request.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method,
         AllowMultiple = true)]
    public class UnitOfWorkAttribute : ActionFilterAttribute
    {
        #region # Variables #

        private readonly Type _unitOfWorkType;
        private readonly IsolationLevel? _isolationLevel;
        private IUnitOfWork _unitOfWork;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWorkAttribute"/>
        /// class.
        /// </summary>
        /// <param name="unitOfWorkType"></param>
        public UnitOfWorkAttribute([NotNull] Type unitOfWorkType)
        {
            ValidateUnitOfWorkType(unitOfWorkType);
            _unitOfWorkType = unitOfWorkType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWorkAttribute"/>
        /// class.
        /// </summary>
        /// <param name="unitOfWorkType"></param>
        /// <param name="isolationLevel"></param>
        public UnitOfWorkAttribute([NotNull] Type unitOfWorkType,
            IsolationLevel isolationLevel)
        {
            ValidateUnitOfWorkType(unitOfWorkType);
            Check.Enum<IsolationLevel>(isolationLevel, nameof(isolationLevel));

            _unitOfWorkType = unitOfWorkType;
            _isolationLevel = isolationLevel;
        }

        #region # Methods #

        #region == Overrides ==

        /// <inheritdoc />
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            Check.NotNull(context, nameof(context));

            // Retrieve the unit of work
            _unitOfWork =
                context.HttpContext.RequestServices.GetService(_unitOfWorkType) as
                    IUnitOfWork;

            if (_unitOfWork == null)
                return;

            // Start transaction using the corresponding isolation level
            if (_isolationLevel.HasValue)
                _unitOfWork.BeginTransaction(_isolationLevel.Value);
            else
                _unitOfWork.BeginTransaction();
        }

        /// <inheritdoc />
        public override void OnResultExecuted(ResultExecutedContext context)
        {
            Check.NotNull(context, nameof(context));
            if (_unitOfWork == null)
                return;

            // Try to commit transaction
            try
            {
                if (context.Exception == null || context.ExceptionHandled)
                    _unitOfWork.Commit();
                else
                    _unitOfWork.Rollback();
            }
            catch
            {
                _unitOfWork.Rollback();
                throw;
            }
            finally
            {
                _unitOfWork.Dispose();
            }
        }

        #endregion

        #region == Private ==

        private static void ValidateUnitOfWorkType(Type unitOfWorkType)
        {
            Check.NotNull(unitOfWorkType, nameof(unitOfWorkType));
            if (!unitOfWorkType.Is<IUnitOfWork>())
            {
                throw new ArgumentException(
                    "The type of the unit of work implement the " +
                    $"'{typeof(IUnitOfWork).FullName}' interface",
                    nameof(unitOfWorkType));
            }
        }

        #endregion

        #endregion
    }
}