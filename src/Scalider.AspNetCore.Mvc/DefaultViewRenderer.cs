﻿#region # using statements #

using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Routing;

#endregion

namespace Scalider.AspNetCore.Mvc
{

    /// <summary>
    /// Represents the default implementation of the
    /// <see cref="IViewRenderer"/>.
    /// </summary>
    public class DefaultViewRenderer : IViewRenderer
    {
        #region # Variables #

        private readonly ICompositeViewEngine _viewEngine;
        private readonly ITempDataProvider _tempDataProvider;
        private readonly IServiceProvider _serviceProvider;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultViewRenderer"/>
        /// class.
        /// </summary>
        /// <param name="viewEngine"></param>
        /// <param name="tempDataProvider"></param>
        /// <param name="serviceProvider"></param>
        public DefaultViewRenderer(ICompositeViewEngine viewEngine,
            ITempDataProvider tempDataProvider, IServiceProvider serviceProvider)
        {
            _viewEngine = viewEngine;
            _tempDataProvider = tempDataProvider;
            _serviceProvider = serviceProvider;
        }

        #region # Methods #

        #region == Protected ==

        /// <summary>
        /// Creates a new instance of the <see cref="ActionContext"/> to use
        /// when resolving views.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionContext"/>.
        /// </returns>
        protected virtual ActionContext CreateActionContext()
        {
            var httpContext = new DefaultHttpContext
            {
                RequestServices = _serviceProvider
            };

            return new ActionContext(httpContext, new RouteData(),
                new ActionDescriptor());
        }

        #endregion

        #endregion

        #region # IViewRenderer #

        /// <summary>
        /// Asynchronously renders a view as a <see cref="string"/>.
        /// </summary>
        /// <param name="viewName">The name of the view to render.</param>
        /// <param name="model">The model to pass to the view.</param>
        /// <returns>
        /// A task that represents the asynchronous render operation.
        /// </returns>
        public virtual async Task<string> RenderAsync(string viewName, object model)
        {
            Check.NotNullOrEmpty(viewName, nameof(viewName));

            // Try to retrieve the view
            var actionContext = CreateActionContext();
            var engineResult = _viewEngine.FindView(actionContext, viewName, false);

            if (!engineResult.Success)
            {
                // The view could not be found
                throw new InvalidOperationException(
                    $"Couldn't find view '{viewName}'.");
            }

            // Try to render the found view
            using (var output = new StringWriter())
            {
                var viewContext = new ViewContext(actionContext, engineResult.View,
                    new ViewDataDictionary(new EmptyModelMetadataProvider(),
                        new ModelStateDictionary()) {Model = model},
                    new TempDataDictionary(actionContext.HttpContext,
                        _tempDataProvider), output, new HtmlHelperOptions());

                await engineResult.View.RenderAsync(viewContext);
                return output.ToString();
            }
        }

        #endregion
    }
}