﻿#region # using statements #

using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;

#endregion

namespace Scalider.AspNetCore.Mvc.ModelBinding.Binders
{

    // http://stackoverflow.com/questions/39276939/how-to-inject-dependencies-into-models-in-asp-net-core
    /// <summary>
    /// <see cref="IModelBinder"/> implementation for binding complex types
    /// with dependency injection.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class ComplexTypeWithDIModelBinder : ComplexTypeModelBinder
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="ComplexTypeWithDIModelBinder"/> class.
        /// </summary>
        /// <param name="propertyBinders">The
        /// <see cref="IDictionary{TKey,TValue}" /> of binders
        /// to use for binding properties.</param>
        public ComplexTypeWithDIModelBinder(
            IDictionary<ModelMetadata, IModelBinder> propertyBinders)
            : base(propertyBinders)
        {
        }

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Creates suitable <see cref="object" /> for given
        /// <paramref name="bindingContext" />.
        /// </summary>
        /// <param name="bindingContext">The <see cref="ModelBindingContext"/>.</param>
        /// <returns>
        /// An <see cref="object" /> compatible with
        /// <see cref="ModelBindingContext.ModelType" />.
        /// </returns>
        protected override object CreateModel(ModelBindingContext bindingContext)
        {
            var services = bindingContext.HttpContext.RequestServices;
            var modelTypes = bindingContext.ModelType;

            var constructors = modelTypes.GetConstructors();
            foreach (var constructor in constructors)
            {
                var paramTypes = constructor.GetParameters()
                                            .Select(p => p.ParameterType)
                                            .ToList();
                var parameters = paramTypes
                    .Select(p => services.GetService(p))
                    .ToArray();

                if (parameters.All(p => p != null))
                {
                    // All parameters were meet, create a new model instance
                    return constructor.Invoke(parameters);
                }
            }

            // Fallback to the default binder
            return base.CreateModel(bindingContext);
        }

        #endregion

        #endregion

    }
}