﻿#region # using statements #

using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;

#endregion

namespace Scalider.AspNetCore.Mvc.ModelBinding.Binders
{

    // http://stackoverflow.com/questions/39276939/how-to-inject-dependencies-into-models-in-asp-net-core
    /// <summary>
    /// An <see cref="IModelBinderProvider"/> for complex types with dependency
    /// injection.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class ComplexTypeWithDIModelBinderProvider : IModelBinderProvider
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="ComplexTypeWithDIModelBinderProvider"/> class.
        /// </summary>
        public ComplexTypeWithDIModelBinderProvider()
        {
        }

        #region # IModelBinderProvider #

        /// <summary>
        /// Creates a <see cref="IModelBinder" /> based on
        /// <see cref="ModelBinderProviderContext" />.
        /// </summary>
        /// <param name="context">The <see cref="ModelBinderProviderContext"/>.</param>
        /// <returns>
        /// An <see cref="IModelBinder" />.
        /// </returns>
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            Check.NotNull(context, nameof(context));
            if (!context.Metadata.IsComplexType || context.Metadata.IsCollectionType)
                return null;

            var propertyBinders =
                context.Metadata.Properties.ToDictionary(p => p,
                    context.CreateBinder);
            return new ComplexTypeWithDIModelBinder(
                propertyBinders);
        }

        #endregion

    }
}