﻿#region # using statements #

using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;

#endregion

namespace Scalider.AspNetCore.Mvc.Csv
{

    /// <summary>
    /// Extension methods for <see cref="Controller"/>.
    /// </summary>
    public static class ControllerExtensions
    {

        /// <summary>
        /// Creates a <see cref="CsvResult"/> object that serializes the
        /// specified <paramref name="data"/> object to CSV.
        /// </summary>
        /// <param name="controller">The <see cref="Controller"/>.</param>
        /// <param name="data">The object to serialize.</param>
        /// <returns>
        /// The created <see cref="CsvResult"/> that serializes the specified
        /// <paramref name="data"/> to CSV format for the response.
        /// </returns>
        public static CsvResult Csv([NotNull] this Controller controller,
            IEnumerable<object> data)
        {
            Check.NotNull(controller, nameof(controller));

            var disposable = data as IDisposable;
            if (disposable != null)
                controller.Response.RegisterForDispose(disposable);

            return new CsvResult(data);
        }

    }
}