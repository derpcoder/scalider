﻿#region # using statements #

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Net.Http.Headers;

#endregion

namespace Scalider.AspNetCore.Mvc.Csv
{

    /// <summary>
    /// An action result which formats the given object as CSV.
    /// </summary>
    public class CsvResult : ActionResult
    {
        #region # Constants #

        private const BindingFlags MemberFlags =
            BindingFlags.Instance | BindingFlags.Public;

        #endregion

        #region # Variables #

        private static readonly MediaTypeHeaderValue DefaultContentType =
            new MediaTypeHeaderValue("text/csv") {Encoding = Encoding.UTF8};

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="CsvResult"/> class.
        /// </summary>
        /// <param name="value"></param>
        public CsvResult(IEnumerable<object> value)
        {
            Value = value;
        }

        #region # Properties #

        #region == Public ==

        /// <summary>
        /// Gets or sets the <see cref="MediaTypeHeaderValue"/> representing
        /// the Content-Type header of the response.
        /// </summary>
        public MediaTypeHeaderValue ContentType { get; set; }

        /// <summary>
        /// Gets or sets the HTTP status code.
        /// </summary>
        public int? StatusCode { get; set; }

        /// <summary>
        /// Gets or sets the value to be formatted.
        /// </summary>
        public IEnumerable<object> Value { get; set; }

        #endregion

        #endregion

        #region # Methods #

        #region == Overrides ==

        /// <summary>
        /// Executes the result operation of the action method asynchronously.
        /// This method is called by MVC to process the result of an action
        /// method.
        /// </summary>
        /// <param name="context">The context in which the result is executed.
        /// The context information includes information about the action that
        /// was executed and request information.</param>
        /// <returns>
        /// A task that represents the asynchronous execute operation.
        /// </returns>
        public override Task ExecuteResultAsync(ActionContext context)
        {
            Check.NotNull(context, nameof(context));
            var response = context.HttpContext.Response;

            // Ensure content type
            var contentTypeHeader = ContentType;
            if (contentTypeHeader == null)
                contentTypeHeader = DefaultContentType;
            else if (contentTypeHeader.Encoding == null)
            {
                // Do not modify the user supplied content type, so copy it instead
                contentTypeHeader = contentTypeHeader.Copy();
                contentTypeHeader.Encoding = Encoding.UTF8;
            }

            response.ContentType = contentTypeHeader.ToString();

            // Status code
            if (StatusCode.HasValue)
                response.StatusCode = StatusCode.Value;

            // Write Csv to response
            using (
                var writer = new HttpResponseStreamWriter(response.Body,
                    contentTypeHeader.Encoding))
            {
                Serialize(writer);
            }

            // Done
            return Task.FromResult(true);
        }

        #endregion

        #region == Private ==

        private static IEnumerable<MemberInfo> GetAllColumns(Type type)
        {
            if (type == null)
                return Enumerable.Empty<MemberInfo>();

            return
                type.GetFields(MemberFlags)
                    .Concat<MemberInfo>(
                        type.GetProperties(MemberFlags).Where(p => p.CanRead))
                    .Concat(GetAllColumns(type.GetTypeInfo().BaseType));
        }

        private Type GetElementType()
        {
            var type = Value.GetType();
            if (type.IsArray)
                return type.GetElementType();

            return
                type.GetInterfaces()
                    .Where(
                        i =>
                            i.GetTypeInfo().IsGenericType &&
                            i.GetGenericTypeDefinition() == typeof(IEnumerable<>))
                    .Select(t => t.GetGenericArguments()[0])
                    .First();
        }

        [SuppressMessage("ReSharper", "LoopCanBePartlyConvertedToQuery")]
        private void Serialize(TextWriter streamWriter)
        {
            if (Value == null)
                return;

            // Retrieve all columns
            var elementType = GetElementType();
            var columns =
                GetAllColumns(elementType)
                    .Where(c => c.GetCustomAttribute<CsvIgnoreAttribute>() == null)
                    .ToArray();

            // Write column names and elements
            streamWriter.WriteLine(string.Join(",", columns.Select(c => c.Name)));
            foreach (var obj in Value)
            {
                var values = columns.Select(c =>
                {
                    var f = c as FieldInfo;
                    return f != null
                        ? f.GetValue(obj)
                        : (c as PropertyInfo)?.GetValue(obj);
                });

                streamWriter.WriteLine(string.Join(",", values));
            }
        }

        #endregion

        #endregion
    }
}