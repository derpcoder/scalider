﻿#region # using statements #

using System;

#endregion

namespace Scalider.AspNetCore.Mvc.Csv
{

    /// <summary>
    /// Represents an attribute that flags a field or property to be ignored by
    /// <see cref="CsvResult"/>.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public sealed class CsvIgnoreAttribute : Attribute
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="CsvIgnoreAttribute"/>
        /// class.
        /// </summary>
        public CsvIgnoreAttribute()
        {
        }

    }
}