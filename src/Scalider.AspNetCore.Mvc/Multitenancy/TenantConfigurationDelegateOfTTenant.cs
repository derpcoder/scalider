﻿#region # using statements #

using JetBrains.Annotations;

#endregion

namespace Scalider.AspNetCore.Mvc.Multitenancy
{

    /// <summary>
    /// A function that configures an individual tenant.
    /// </summary>
    /// <typeparam name="TTenant">The type encapsulating the tenant.</typeparam>
    /// <param name="context">The
    /// <see cref="TenantPipelineBuilderContext{TTenant}"/> containing the
    /// tenant to be configured.</param>
    public delegate void TenantConfigurationDelegate<TTenant>(
        [NotNull] TenantPipelineBuilderContext<TTenant> context);

}