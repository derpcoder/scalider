﻿#region # using statements #

using Scalider.Multitenancy;

#endregion

namespace Scalider.AspNetCore.Mvc.Multitenancy
{

    /// <summary>
    /// Encapsulates all tenant-specific about an individual request.
    /// </summary>
    /// <typeparam name="TTenant">The type encapsulating the tenant.</typeparam>
    public sealed class TenantPipelineBuilderContext<TTenant>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="TenantPipelineBuilderContext{TTenant}"/> class.
        /// </summary>
        internal TenantPipelineBuilderContext()
        {
        }

        #region # Properties #

        #region == Public ==

        /// <summary>
        /// Gets the <see cref="TenantContext{TTenant}"/> for the current
        /// request.
        /// </summary>
        public TenantContext<TTenant> TenantContext { get; internal set; }
        
        /// <summary>
        /// Gets the tenant for the current request.
        /// </summary>
        public TTenant Tenant { get; internal set; }

        #endregion

        #endregion
    }
}