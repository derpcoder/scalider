﻿#region # using statements #

using Scalider.Multitenancy;

#endregion

namespace Scalider.AspNetCore.Mvc.Multitenancy.InMemory
{

    /// <summary>
    /// Represents the configuration options for the
    /// <see cref="MemoryCacheTenantResolver{TTenant}"/>.
    /// </summary>
    public class MemoryCacheTenantResolverOptions
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="MemoryCacheTenantResolverOptions"/> class.
        /// </summary>
        public MemoryCacheTenantResolverOptions()
        {
        }

        #region # Properties #

        #region == Public ==

        /// <summary>
        /// Gets or sets a setting that determines whether all cache entries
        /// for a <see cref="TenantContext{TTenant}"/> instance should be
        /// evicted when any of the entries expire.
        /// <para />
        /// The default value is <c>true</c>.
        /// </summary>
        public bool EvictAllEntriesOnExpiry { get; set; } = true;

        /// <summary>
        /// Gets or sets a setting that determines whether cached tenant
        /// context instances should be disposed when upon eviction from the
        /// cache.
        /// <para />
        /// The default value is <c>true</c>
        /// </summary>
        public bool DisposeOnEviction { get; set; } = true;

        #endregion

        #endregion
    }
}