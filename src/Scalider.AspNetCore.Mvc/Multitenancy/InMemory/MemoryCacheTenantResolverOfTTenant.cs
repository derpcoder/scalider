﻿#region # using statements #

using System;
using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Scalider.Multitenancy;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;

#endregion

namespace Scalider.AspNetCore.Mvc.Multitenancy.InMemory
{

    /// <summary>
    /// <see cref="ITenantResolver{TTenant}"/> implementation that uses
    /// <see cref="IMemoryCache"/> to resolve tenants only once.
    /// </summary>
    /// <typeparam name="TTenant">The type encapsulating the tenant.</typeparam>
    public abstract class MemoryCacheTenantResolver<TTenant> :
        ITenantResolver<TTenant>
    {
        #region # Variables #

        private readonly IMemoryCache _memoryCache;
        private readonly ILogger<MemoryCacheTenantResolver<TTenant>> _logger;
        private readonly MemoryCacheTenantResolverOptions _options;

        #endregion

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="MemoryCacheTenantResolver{TTenant}"/> class.
        /// </summary>
        /// <param name="memoryCache"></param>
        /// <param name="loggerFactory"></param>
        protected MemoryCacheTenantResolver([NotNull] IMemoryCache memoryCache,
            [NotNull] ILoggerFactory loggerFactory)
            : this(memoryCache, loggerFactory,
                new MemoryCacheTenantResolverOptions())
        {
        }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="MemoryCacheTenantResolver{TTenant}"/> class.
        /// </summary>
        /// <param name="memoryCache"></param>
        /// <param name="loggerFactory"></param>
        /// <param name="options"></param>
        protected MemoryCacheTenantResolver([NotNull] IMemoryCache memoryCache,
            [NotNull] ILoggerFactory loggerFactory,
            [NotNull] MemoryCacheTenantResolverOptions options)
        {
            Check.NotNull(memoryCache, nameof(memoryCache));
            Check.NotNull(loggerFactory, nameof(loggerFactory));
            Check.NotNull(options, nameof(options));

            _memoryCache = memoryCache;
            _logger =
                loggerFactory.CreateLogger<MemoryCacheTenantResolver<TTenant>>();
            _options = options;
        }

        #region # Methods #

        #region == Protected ==

        /// <summary>
        /// Creates a new <see cref="MemoryCacheEntryOptions"/>.
        /// </summary>
        /// <returns>
        /// The new <see cref="MemoryCacheEntryOptions"/>.
        /// </returns>
        protected virtual MemoryCacheEntryOptions CreateCacheEntryOptions()
        {
            return new MemoryCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromDays(1f));
        }
        
        /// <summary>
        /// Retrieves the tenant identifier to be resolved for the current
        /// request.
        /// </summary>
        /// <param name="context">The <see cref="HttpContext"/> for the current
        /// request.</param>
        /// <returns>
        /// The tenant identifier for the current request.
        /// </returns>
        protected abstract string GetContextIdentifier([NotNull] HttpContext context);

        /// <summary>
        /// Retrieves the <typeparamref name="TTenant"/> for the current
        /// request.
        /// </summary>
        /// <param name="context">The <see cref="HttpContext"/> containing the
        /// information for the current request.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        protected abstract Task<TenantContext<TTenant>> ResolveAsync(
            [NotNull] HttpContext context);
        
        /// <summary>
        /// Callback used for disposing a tenant context when the cache
        /// entry is evicted.
        /// </summary>
        /// <param name="cacheKey">The cache key for the tenant.</param>
        /// <param name="tenantContext">The
        /// <see cref="TenantContext{TTenant}"/> to dispose.</param>
        protected virtual void DisposeTenantContext([NotNull] object cacheKey,
            TenantContext<TTenant> tenantContext)
        {
            Check.NotNull(cacheKey, nameof(cacheKey));
            if (tenantContext == null)
                return;

            _logger.LogDebug(
                $"Disposing tenant with identifier '{tenantContext.Id}' and " +
                $"instance with key '{cacheKey}'.");

            tenantContext.Dispose();
        }

        #endregion

        #region == Private ==

        private MemoryCacheEntryOptions GetCacheEntryOptions()
        {
            var cacheEntryOptions = CreateCacheEntryOptions();

            // EvictAllEntriesOnExpiry
            if (_options.EvictAllEntriesOnExpiry)
            {
                var tokenSource = new CancellationTokenSource();
                cacheEntryOptions.RegisterPostEvictionCallback((k, v, r, s) =>
                                 {
                                     tokenSource.Cancel();
                                 })
                                 .AddExpirationToken(
                                     new CancellationChangeToken(tokenSource.Token));
            }

            // DisposeOnEviction
            if (_options.DisposeOnEviction)
            {
                cacheEntryOptions.RegisterPostEvictionCallback((k, v, r, s) =>
                {
                    DisposeTenantContext(k, v as TenantContext<TTenant>);
                });
            }

            // Done
            return cacheEntryOptions;
        }

        #endregion

        #endregion

        #region # ITenantResolver<TTenant> #

        async Task<TenantContext<TTenant>> ITenantResolver<TTenant>.ResolveAsync(
            HttpContext context)
        {
            Check.NotNull(context, nameof(context));

            // Obtain the key used to identify cached tenants from the current request
            var cacheKey = GetContextIdentifier(context);
            if (string.IsNullOrWhiteSpace(cacheKey))
                return null;

            // Retrieve cached tenant context
            var tenantContext = _memoryCache.Get(cacheKey) as TenantContext<TTenant>;
            if (tenantContext == null)
            {
                // Tenant context not cached
                _logger.LogDebug(
                    $"No tenant cache match for '{cacheKey}'. Trying to resolve " +
                    "tenant");
                
                tenantContext = await ResolveAsync(context);
                if (tenantContext == null)
                {
                    _logger.LogWarning(
                        "Could not resolve any tenant using the tenant for the " +
                        "current request");

                    return null;
                }

                // Write tenant context to cache
                _logger.LogDebug(
                    $"Tenant with identifier '{tenantContext.Id}' resolved " +
                    "for the current request, writing to cache");

                _memoryCache.Set(cacheKey, tenantContext, GetCacheEntryOptions());
            }
            else
            {
                // Tenant context cached
                _logger.LogDebug(
                    "TenantContext: {id} retrieved from cache with key \"{cacheKey}\".",
                    tenantContext.Id, cacheKey);
            }

            // Done
            return tenantContext;
        }

        #endregion
    }
}