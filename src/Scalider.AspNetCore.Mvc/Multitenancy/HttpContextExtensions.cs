﻿#region # using statements #

using JetBrains.Annotations;
using Scalider.Multitenancy;
using Microsoft.AspNetCore.Http;

#endregion

namespace Scalider.AspNetCore.Mvc.Multitenancy
{

    /// <summary>
    /// Extension methods for <see cref="HttpContext"/>,
    /// </summary>
    public static class HttpContextExtensions
    {

        private const string ItemKey = "Scalider.Multitenancy.TenantContext";

        /// <summary>
        /// Retrieves the <see cref="TenantContext{TTenant}"/> for the current
        /// request or <c>null</c> if the given <paramref name="context"/> does
        /// not contains any tenant context.
        /// </summary>
        /// <typeparam name="TTenant">The type encapsulating the tenant.</typeparam>
        /// <param name="context">The <see cref="HttpContext"/> for the current
        /// request.</param>
        /// <returns>
        /// The <see cref="TenantContext{TTenant}"/> for the current request or
        /// <c>null</c>.
        /// </returns>
        public static TenantContext<TTenant> GetTenantContext<TTenant>(
            [NotNull] this HttpContext context)
        {
            Check.NotNull(context, nameof(context));

            object tenantContext;
            if (!context.Items.TryGetValue(ItemKey, out tenantContext))
                return null;

            return tenantContext as TenantContext<TTenant>;
        }

        internal static void SetTenantContext<TTenant>(
            [NotNull] this HttpContext context,
            [NotNull] TenantContext<TTenant> tenantContext)
        {
            Check.NotNull(context, nameof(context));
            Check.NotNull(tenantContext, nameof(tenantContext));

            context.Items[ItemKey] = tenantContext;
        }

        /// <summary>
        /// Retrieves the tenant for the current request or <c>null</c> if the
        /// given <paramref name="context"/> does not contain any tenant.
        /// </summary>
        /// <typeparam name="TTenant">The type encapsulating the tenant.</typeparam>
        /// <param name="context">The <see cref="HttpContext"/> for the current
        /// request.</param>
        /// <returns>
        /// The tenant for the current request or <c>null</c>.
        /// </returns>
        public static TTenant GetTenant<TTenant>([NotNull] this HttpContext context)
        {
            Check.NotNull(context, nameof(context));

            var tenantContext = GetTenantContext<TTenant>(context);
            return tenantContext == null ? default(TTenant) : tenantContext.Tenant;
        }

    }
}