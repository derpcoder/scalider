﻿#region # using statements #

using System;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

#endregion

namespace Scalider.AspNetCore.Mvc.Multitenancy.Internal
{

    /// <summary>
    /// Represents a middleware that resolves the tenant for the current
    /// request.
    /// </summary>
    /// <typeparam name="TTenant">The type encapsulating the tenant.</typeparam>
    internal class TenantResolutionMiddleware<TTenant>
    {
        #region # Variables #

        private readonly RequestDelegate _next;
        private readonly ILogger<TenantResolutionMiddleware<TTenant>> _logger;

        #endregion

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="TenantResolutionMiddleware{TTenant}"/> class.
        /// </summary>
        /// <param name="next"></param>
        /// <param name="loggerFactory"></param>
        public TenantResolutionMiddleware([NotNull] RequestDelegate next,
            [NotNull] ILoggerFactory loggerFactory)
        {
            Check.NotNull(next, nameof(next));
            Check.NotNull(loggerFactory, nameof(loggerFactory));

            _next = next;
            _logger =
                loggerFactory.CreateLogger<TenantResolutionMiddleware<TTenant>>();
        }

        #region # Methods #

        #region == Public ==

        /// <summary>
        /// A function that can process an HTTP request.
        /// </summary>
        /// <param name="context">The <see cref="HttpContext"/> for the
        /// request.</param>
        /// <param name="tenantResolver">The
        /// <see cref="ITenantResolver{TTenant}"/>.</param>
        /// <returns>
        /// A task taht represents the completion of the request processing.
        /// </returns>
        public async Task Invoke([NotNull] HttpContext context,
            [NotNull] ITenantResolver<TTenant> tenantResolver)
        {
            Check.NotNull(context, nameof(context));
            Check.NotNull(tenantResolver, nameof(tenantResolver));

            // Try to resolve the TenantContext
            try
            {
                var tenantContext = await tenantResolver.ResolveAsync(context);
                if (tenantContext != null)
                {
                    // Tenant content resolved
                    _logger.LogDebug("TenantContext resolved for the current " +
                                     "request");
                    context.SetTenantContext(tenantContext);
                }
                else
                {
                    // TenantContext not resolved
                    _logger.LogDebug(
                        "Could not resolve the TenantContext for the current " +
                        "request");
                }
            }
            catch (Exception e)
            {
                _logger.LogError(0, e,
                    "There was an unexpecting error while trying to resolve " +
                    "the tenant for the current request");
            }

            // Done
            await _next(context);
        }

        #endregion

        #endregion
    }

}