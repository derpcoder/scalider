﻿#region # using statements #

using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Scalider.Multitenancy;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

#endregion

namespace Scalider.AspNetCore.Mvc.Multitenancy.Internal
{

    internal class TenantPipelineMiddleware<TTenant>
    {
        #region # Variables #

        private readonly RequestDelegate _next;
        private readonly IApplicationBuilder _rootApplication;
        private readonly TenantConfigurationDelegate<TTenant> _cfgDelegate;

        //
        private readonly ConcurrentDictionary<TTenant, Lazy<RequestDelegate>>
            _pipelines = new ConcurrentDictionary<TTenant, Lazy<RequestDelegate>>();

        #endregion

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="TenantPipelineMiddleware{TTenant}"/> class.
        /// </summary>
        /// <param name="next"></param>
        /// <param name="rootApp"></param>
        /// <param name="cfgDelegate"></param>
        public TenantPipelineMiddleware(RequestDelegate next,
            IApplicationBuilder rootApp,
            TenantConfigurationDelegate<TTenant> cfgDelegate)
        {
            _next = next;
            _rootApplication = rootApp;
            _cfgDelegate = cfgDelegate;
        }

        #region # Methods #

        #region == Public ==

        public async Task Invoke(HttpContext context)
        {
            Check.NotNull(context, nameof(context));

            // Try to retrieve TenantContext for the current request
            var tenantContext = context.GetTenantContext<TTenant>();
            if (tenantContext != null)
            {
                // The TenantContext exists for the current request
                var pipeline = _pipelines.GetOrAdd(tenantContext.Tenant,
                    new Lazy<RequestDelegate>(
                        () => BuildTenantPipeline(tenantContext)));

                await pipeline.Value(context);
            }
        }

        #endregion

        #region == Private ==

        private RequestDelegate BuildTenantPipeline(TenantContext<TTenant> context)
        {
            var branchBuilder = _rootApplication.New();
            var builderContext = new TenantPipelineBuilderContext<TTenant>
            {
                TenantContext = context,
                Tenant = context.Tenant
            };

            _cfgDelegate(builderContext);

            // Register root pipeline at the end of the tenant branch
            branchBuilder.Run(_next);
            return branchBuilder.Build();
        }

        #endregion

        #endregion
    }
}