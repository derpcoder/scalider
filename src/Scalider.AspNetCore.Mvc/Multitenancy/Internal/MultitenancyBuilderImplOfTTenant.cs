﻿#region # using statements #

using Microsoft.AspNetCore.Builder;

#endregion

namespace Scalider.AspNetCore.Mvc.Multitenancy.Internal
{

    /// <summary>
    /// Default implementation for the
    /// <see cref="IMultitenancyBuilder{TTenant}"/>.
    /// </summary>
    /// <typeparam name="TTenant"></typeparam>
    internal class MultitenancyBuilderImpl<TTenant> : IMultitenancyBuilder<TTenant>
    {
        #region # Variables #

        private readonly IApplicationBuilder _appBuilder;

        #endregion

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="MultitenancyBuilderImpl{TTenant}"/> class.
        /// </summary>
        /// <param name="appBuilder"></param>
        public MultitenancyBuilderImpl(IApplicationBuilder appBuilder)
        {
            _appBuilder = appBuilder;
        }

        #region # IMultitenancyBuilder<TTenant> #

        /// <summary>
        /// Configures the <see cref="TenantConfigurationDelegate{TTenant}"/>
        /// to use when configuring a valid request for a tenant.
        /// </summary>
        /// <param name="cfgDelegate">The
        /// <see cref="TenantConfigurationDelegate{TTenant}"/>.</param>
        /// <returns>
        /// The <see cref="IMultitenancyBuilder{TTenant}"/> that can be used to
        /// further configure multitenancy components.
        /// </returns>
        public IMultitenancyBuilder<TTenant> UsePerTenant(
            TenantConfigurationDelegate<TTenant> cfgDelegate)
        {
            Check.NotNull(cfgDelegate, nameof(cfgDelegate));

            _appBuilder.Use(
                next =>
                    new TenantPipelineMiddleware<TTenant>(next, _appBuilder,
                        cfgDelegate).Invoke);
            return this;
        }

        /// <summary>
        /// Configures the redirection location for when a tenant cannot be
        /// resolved.
        /// </summary>
        /// <param name="redirectLocation">The redirection location.</param>
        /// <returns>
        /// The <see cref="IMultitenancyBuilder{TTenant}"/> that can be used to
        /// further configure multitenancy components.
        /// </returns>
        public IMultitenancyBuilder<TTenant> RedirectUnresolvedTenant(
            string redirectLocation)
            => RedirectUnresolvedTenant(redirectLocation, false);

        /// <summary>
        /// Configures the redirection location for when a tenant cannot be
        /// resolved.
        /// </summary>
        /// <param name="redirectLocation">The redirection location.</param>
        /// <param name="permanentRedirection">A value indicating whether the
        /// redirection is permanent.</param>
        /// <returns>
        /// The <see cref="IMultitenancyBuilder{TTenant}"/> that can be used to
        /// further configure multitenancy components.
        /// </returns>
        public IMultitenancyBuilder<TTenant> RedirectUnresolvedTenant(
            string redirectLocation, bool permanentRedirection)
        {
            Check.NotNullOrEmpty(redirectLocation, nameof(redirectLocation));

            _appBuilder.Use(
                next =>
                    new RedirectUnresolvedTenantMiddleware<TTenant>(next,
                        redirectLocation, permanentRedirection).Invoke);
            return this;
        }

        #endregion
    }
}