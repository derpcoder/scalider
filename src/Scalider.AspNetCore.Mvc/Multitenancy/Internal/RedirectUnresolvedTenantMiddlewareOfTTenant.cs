﻿#region # using statements #

using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

#endregion

namespace Scalider.AspNetCore.Mvc.Multitenancy.Internal
{

    /// <summary>
    /// Represents a middleware that redirects the current request to the
    /// specified redirect location if the tenant for the current request
    /// could not be resolved.
    /// </summary>
    /// <typeparam name="TTenant"></typeparam>
    internal class RedirectUnresolvedTenantMiddleware<TTenant>
    {
        #region # Variables #

        private readonly RequestDelegate _next;
        private readonly string _redirectLocation;
        private readonly bool _permanentRedirection;

        #endregion

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="RedirectUnresolvedTenantMiddleware{TTenant}"/> class.
        /// </summary>
        /// <param name="next"></param>
        /// <param name="redirectLocation"></param>
        /// <param name="permanent"></param>
        public RedirectUnresolvedTenantMiddleware(RequestDelegate next,
            string redirectLocation, bool permanent)
        {
            _next = next;
            _redirectLocation = redirectLocation;
            _permanentRedirection = permanent;
        }

        #region # Methods #

        #region == Public ==

        public async Task Invoke(HttpContext context)
        {
            Check.NotNull(context, nameof(context));

            // Determine if the TenantContext is available
            var tenantCotnext = context.GetTenantContext<TTenant>();
            if (tenantCotnext == null)
            {
                // TenantContext not available
                context.Response.Redirect(_redirectLocation);
                context.Response.StatusCode = _permanentRedirection
                    ? StatusCodes.Status301MovedPermanently
                    : StatusCodes.Status302Found;

                return;
            }

            // Done
            await _next(context);
        }

        #endregion

        #endregion
    }
}