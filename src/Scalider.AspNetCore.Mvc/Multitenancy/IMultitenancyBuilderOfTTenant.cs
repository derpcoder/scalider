﻿#region # using statements #

using JetBrains.Annotations;

#endregion

namespace Scalider.AspNetCore.Mvc.Multitenancy
{

    /// <summary>
    /// Provides methods for further configuring the multitenancy pipeline.
    /// </summary>
    /// <typeparam name="TTenant">The type encapsulating the tenant.</typeparam>
    public interface IMultitenancyBuilder<TTenant>
    {

        /// <summary>
        /// Configures the <see cref="TenantConfigurationDelegate{TTenant}"/>
        /// to use when configuring a valid request for a tenant.
        /// </summary>
        /// <param name="cfgDelegate">The
        /// <see cref="TenantConfigurationDelegate{TTenant}"/>.</param>
        /// <returns>
        /// The <see cref="IMultitenancyBuilder{TTenant}"/> that can be used to
        /// further configure multitenancy components.
        /// </returns>
        IMultitenancyBuilder<TTenant> UsePerTenant(
            [NotNull] TenantConfigurationDelegate<TTenant> cfgDelegate);

        /// <summary>
        /// Configures the redirection location for when a tenant cannot be
        /// resolved.
        /// </summary>
        /// <param name="redirectLocation">The redirection location.</param>
        /// <returns>
        /// The <see cref="IMultitenancyBuilder{TTenant}"/> that can be used to
        /// further configure multitenancy components.
        /// </returns>
        IMultitenancyBuilder<TTenant> RedirectUnresolvedTenant(
            [NotNull] string redirectLocation);

        /// <summary>
        /// Configures the redirection location for when a tenant cannot be
        /// resolved.
        /// </summary>
        /// <param name="redirectLocation">The redirection location.</param>
        /// <param name="permanentRedirection">A value indicating whether the
        /// redirection is permanent.</param>
        /// <returns>
        /// The <see cref="IMultitenancyBuilder{TTenant}"/> that can be used to
        /// further configure multitenancy components.
        /// </returns>
        IMultitenancyBuilder<TTenant> RedirectUnresolvedTenant(
            [NotNull] string redirectLocation, bool permanentRedirection);

    }
}