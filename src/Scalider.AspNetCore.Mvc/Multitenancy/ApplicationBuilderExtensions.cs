﻿#region # using statements #

using JetBrains.Annotations;
using Scalider.AspNetCore.Mvc.Multitenancy.Internal;
using Microsoft.AspNetCore.Builder;

#endregion

namespace Scalider.AspNetCore.Mvc.Multitenancy
{

    /// <summary>
    /// Extension methods for <see cref="IApplicationBuilder"/>.
    /// </summary>
    public static class ApplicationBuilderExtensions
    {

        /// <summary>
        /// Adds multitenancy support to the <see cref="IApplicationBuilder"/>
        /// request execution pipeline.
        /// </summary>
        /// <typeparam name="TTenant">The type encapsulating the tenant.</typeparam>
        /// <param name="builder">The <see cref="IApplicationBuilder"/>.</param>
        /// <returns>
        /// An <see cref="IMultitenancyBuilder{TTenant}"/> that can be used to
        /// further configure multitenancy components.
        /// </returns>
        public static IMultitenancyBuilder<TTenant> UseMultitenancy<TTenant>(
            [NotNull] this IApplicationBuilder builder)
        {
            Check.NotNull(builder, nameof(builder));

            builder.UseMiddleware<TenantResolutionMiddleware<TTenant>>();
            return new MultitenancyBuilderImpl<TTenant>(builder);
        }

    }
}