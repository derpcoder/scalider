﻿#region # using statements #

using System.Reflection;
using JetBrains.Annotations;
using Scalider.AspNetCore.Mvc.Multitenancy.InMemory;
using Scalider.Multitenancy;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

#endregion

namespace Scalider.AspNetCore.Mvc.Multitenancy
{

    /// <summary>
    /// Extension methods for <see cref="IServiceCollection"/>.
    /// </summary>
    public static class ServiceCollectionExtensions
    {

        /// <summary>
        /// Adds multitenancy support to the specified
        /// <see cref="IServiceCollection"/>.
        /// </summary>
        /// <typeparam name="TTenant">The type encapsulating the tenant.</typeparam>
        /// <typeparam name="TResolver">The type of the
        /// <see cref="ITenantResolver{TTenant}"/> to use.</typeparam>
        /// <param name="services">The <see cref="IServiceCollection"/> to add
        /// services to.</param>
        /// <returns>
        /// The <see cref="IServiceCollection"/> so that additional calls can
        /// be chained.
        /// </returns>
        public static IServiceCollection AddMultitenancy<TTenant, TResolver>(
            [NotNull] this IServiceCollection services)
            where TTenant : class
            where TResolver : class, ITenantResolver<TTenant>
        {
            Check.NotNull(services, nameof(services));

            services.Add(
                ServiceDescriptor.Scoped<ITenantResolver<TTenant>, TResolver>());
            services.TryAdd(
                ServiceDescriptor
                    .Singleton<IHttpContextAccessor, HttpContextAccessor>());

            // Make Tenant and TenantContext injectable
            services.TryAdd(
                ServiceDescriptor.Scoped(
                    p =>
                        p.GetService<IHttpContextAccessor>()?
                         .HttpContext?.GetTenantContext<TTenant>()));
            services.TryAdd(
                ServiceDescriptor.Scoped(
                    p => p.GetService<TenantContext<TTenant>>()?.Tenant));

            // Ensure caching is available for caching resolvers
            var resolverType = typeof(TResolver);
            var memoryResolver = typeof(MemoryCacheTenantResolver<TTenant>);
            if (memoryResolver.GetTypeInfo().IsAssignableFrom(resolverType))
                services.AddMemoryCache();

            // Done
            return services;
        }

    }
}