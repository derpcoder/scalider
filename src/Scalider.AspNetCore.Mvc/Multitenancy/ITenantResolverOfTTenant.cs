﻿#region # using statements #

using System.Threading.Tasks;
using JetBrains.Annotations;
using Scalider.Multitenancy;
using Microsoft.AspNetCore.Http;

#endregion

namespace Scalider.AspNetCore.Mvc.Multitenancy
{

    /// <summary>
    /// Defines a way for retrieving the tenant for the current request.
    /// </summary>
    /// <typeparam name="TTenant">The type encapsulating the tenant.</typeparam>
    public interface ITenantResolver<TTenant>
    {

        /// <summary>
        /// Retrieves the <typeparamref name="TTenant"/> for the current
        /// request.
        /// </summary>
        /// <param name="context">The <see cref="HttpContext"/> containing the
        /// information for the current request.</param>
        /// <returns>
        /// The <see cref="Task"/> object representing the asynchronous
        /// operation.
        /// </returns>
        Task<TenantContext<TTenant>> ResolveAsync([NotNull] HttpContext context);

    }
}