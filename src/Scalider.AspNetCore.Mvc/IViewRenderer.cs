﻿#region # using statements #

using System.Threading.Tasks;
using JetBrains.Annotations;

#endregion

namespace Scalider.AspNetCore.Mvc
{

    /// <summary>
    /// Defines the basic functionality of a view renderer.
    /// </summary>
    public interface IViewRenderer
    {

        /// <summary>
        /// Asynchronously renders a view as a <see cref="string"/>.
        /// </summary>
        /// <param name="viewName">The name of the view to render.</param>
        /// <param name="model">The model to pass to the view.</param>
        /// <returns>
        /// A task that represents the asynchronous render operation.
        /// </returns>
        Task<string> RenderAsync([NotNull] string viewName, object model);

    }
}