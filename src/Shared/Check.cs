#region # using statements #

using JetBrains.Annotations;
using System;
using System.Diagnostics;
using System.Reflection;

#endregion

namespace Scalider
{

    [DebuggerStepThrough]
    internal static class Check
    {

        [ContractAnnotation("value:null => halt")]
        public static T NotNull<T>([NoEnumeration] T value,
            [InvokerParameterName, NotNull] string parameterName)
        {
            if (value == null)
                throw new ArgumentNullException(parameterName);

            return value;
        }

        [ContractAnnotation("value:null => halt")]
        public static void NullNotAllowed<T>([NoEnumeration] object value,
            [InvokerParameterName, NotNull] string parameterName)
        {
            if (value != null || default(T) == null)
                return;

            NotNull((T)value, parameterName);
        }

        [ContractAnnotation("value:null => halt")]
        public static string NotNullOrEmpty(string value,
            [InvokerParameterName, NotNull] string parameterName)
        {
            if (value == null)
                throw new ArgumentNullException(parameterName);
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentException(
                    $"The string parameter {parameterName} cannot be null or " +
                    "contain only whitespaces.", parameterName);
            }

            return value.Trim();
        }

        [ContractAnnotation("value:null => halt")]
        public static T PropertyNotNull<T>(T value, [NotNull] string propertyName,
            [NotNull] string typeName)
        {
            if (value == null)
            {
                throw new ArgumentException(
                    $"The '{propertyName}' property of '{typeName}' must not be " +
                    "null.");
            }

            return value;
        }

        public static T Enum<T>([NotNull] object value,
            [InvokerParameterName, NotNull] string parameterName)
            where T : struct
        {
            var typeOfT = typeof(T).GetTypeInfo();
            if (!typeOfT.IsEnum)
            {
                throw new ArgumentException(
                    $"The type {typeOfT.Name} is not an enum", parameterName);
            }

            // Validate value
            var stringValue = value as string;
            if (stringValue != null)
            {
                T enumValue;
                if (!System.Enum.TryParse(stringValue, true, out enumValue))
                {
                    throw new ArgumentException(
                        $"The value '{stringValue}' is not a valid value for " +
                        $"the enum {typeOfT.Name}", parameterName);
                }

                // Done
                return enumValue;
            }

            if (!System.Enum.IsDefined(typeof(T), value))
            {
                throw new ArgumentException(
                    $"The value '{value}' is not valid for the enum " +
                    $"{typeOfT.Name}", parameterName);
            }

            // Done
            return (T)value;
        }

    }

}