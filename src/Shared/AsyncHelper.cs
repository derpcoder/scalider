﻿#region # using statements #

using System;
using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;

#endregion

namespace Scalider
{

    /// <summary>
    /// Helper methods for executing <see cref="Task"/>s synchronously.
    /// </summary>
    internal static class AsyncHelper
    {

        /// <summary>
        /// Represents the <see cref="TaskFactory"/> for executing task
        /// synchronously.
        /// </summary>
        private static readonly TaskFactory Factory =
            new TaskFactory(CancellationToken.None, TaskCreationOptions.None,
                TaskContinuationOptions.None, TaskScheduler.Default);
        
        public static void RunSync([NotNull] Func<Task> func)
        {
            Check.NotNull(func, nameof(func));
            Factory.StartNew(func).Unwrap().GetAwaiter().GetResult();
        }
        
        public static TResult RunSync<TResult>([NotNull] Func<Task<TResult>> func)
        {
            Check.NotNull(func, nameof(func));
            return Factory.StartNew(func).Unwrap().GetAwaiter().GetResult();
        }

    }
}